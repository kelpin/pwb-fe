import React from "react";
import { Text } from "../../components/foundations/Text";
import Layout from "../../components/layout";
import IMAGES from "@/images";
import CssUtils from "@/utils/Css";
import ID from "../../translations/id";
import { ButtonLink } from "../../components/Button";
import ROUTES from "../../routes";
import Link from "next/link";
import dynamic from "next/dynamic";
import { IBanner } from "@/integrations/types/IBanner";
import { ISlideShow } from "@/integrations/types/ISlideShow";
import useBanner from "@/integrations/hooks/useBanner";
import useSlideshow from "@/integrations/hooks/useSlideshow";
import SectionSlideShowVideo from "@/components/SectionSlideShowVideo";
import SectionSlideShow from "@/components/SectionSlideShow";
import { IPromo } from "@/integrations/types/IPromo";
import usePromo from "@/integrations/hooks/usePromo";
import { IPromoProduk } from "@/integrations/types/IPromoProduk";
import usePromoProduk from "@/integrations/hooks/usePromoProduk";

const Carousel = dynamic(
  () => import("../../components/carousel/Carousel.js"),
  {
    ssr: false,
  }
);

const LocalTailwind = {
  buttonContainer: "flex flex-col md:flex-row items-center justify-between",
  buttonLeft: "flex flex-col md:flex-row items-left justify-between",
  button: "mx-2 my-2",
};

const SectionSlideShowSimplicity = (props: {
  image: string;
  title: string;
  desc: string;
}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex flex-col w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  };

  return (
    <section
      className={tailwindStyles.container}
      style={{ backgroundImage: `url(${props.image})` }}
    >
      <div className="flex flex-col items-center mb-16">
        <Text
          kind="h3"
          className="font-bold leading-none text-white text-center"
        >
          {props.title}
        </Text>
        <Text className="leading-none text-white">{props.desc}</Text>
      </div>
      <div className={LocalTailwind.buttonContainer}>
        <ButtonLink
          className={LocalTailwind.button}
          to={ROUTES.home.airFryer.detail.link}
          label="Shop Now"
          kind="white"
        />
        <ButtonLink
          className={LocalTailwind.button}
          to={ROUTES.home.airFryer.link}
          label="More Info"
          kind="text-white"
        />
      </div>
    </section>
  );
};
const SectionAirFyer = (props: { image: string; title: string; id: string}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen flex flex-col w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    image: "",
    title: "",
    subtitle: "",
  };

  return (
    <section
      className={tailwindStyles.container}
      style={{ backgroundImage: `url("${props.image}")` }}
    >
      <div className="flex flex-col items-center mb-16">
        <Text
          kind="h3"
          className="font-bold leading-none text-white text-center mb-2"
        >
          {props.title}
        </Text>
      </div>
      <div className={LocalTailwind.buttonContainer}>
        <ButtonLink
          className={LocalTailwind.button}
          to={"/product/"+props.id}
          label="Shop Now"
          kind="white"
        />
        <ButtonLink
          className={LocalTailwind.button}
          to={ROUTES.home.airFryer.link}
          label="More Info"
          kind="text-white"
        />
      </div>
    </section>
  );
};

const SectionRiceCooker = (props: { image: string; title: string; id: string }) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex w-full items-center justify-center py-16",
    spacer: "w-1/12",
  };

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${props.image})` }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-center md:items-start">
        <div className="mb-10">
          <Text kind="h3" className="font-bold leading-none text-white mb-4">
            {props.title}
          </Text>
          <Text className="leading-none text-white max-w-sm">{props.desc}</Text>
        </div>

        <ButtonLink
          to={"/product/"+props.id}
          label="Shop Now"
          kind="white"
        />
         <ButtonLink
          className={LocalTailwind.button}
          to={ROUTES.home.ricecooker.link}
          label="More Info"
          kind="text-white"
        />
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  );
};

const SectionMultiCooker = (props: { image: string; title: string; id: string }) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex flex-col w-full items-center justify-between py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  };

  return (
    <section
      className={tailwindStyles.container}
      style={{ backgroundImage: `url(${props.image})` }}
    >
      <div className="flex flex-col items-center">
        {/* <Text className="leading-none">{ID["home.airPurifier.subtitle"]}</Text> */}
        <Text kind="h3" className="font-bold leading-none text-black">
          {props.title}
        </Text>
      </div>
      <div className={LocalTailwind.buttonContainer}>
        <ButtonLink
          className={LocalTailwind.button}
          to={"/product/"+props.id}
          label="Shop Now"
          kind="black"
        />
        <ButtonLink
          className="mx-2"
          to={ROUTES.home.multicooker.link}
          label="More Info"
          kind="text-black"
        />
      </div>
    </section>
  );
};

const SectionAirFryerSecond = (props: { image: string; title: string; id: string }) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex w-full items-center justify-center py-16",
    spacer: "w-1/12",
  };

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${props.image})` }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-center md:items-start">
        <div className="mb-10">
          <Text kind="h3" className="font-bold leading-none text-black mb-4">
            {props.title}
          </Text>
          <Text className="leading-none text-black max-w-sm">{props.desc}</Text>
        </div>

        <ButtonLink
          to={"/product/"+props.id}
          label="Shop Now"
          kind="black"
        />
         <ButtonLink
          className={LocalTailwind.button}
          to={ROUTES.home.airFryer52.link}
          label="More Info"
          kind="text-white"
        />
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  );
};

const SectionAirPurifier = (props: { image: string; title: string; id: string }) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex flex-col w-full items-center justify-between py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  };

  return (
    <section
      className={tailwindStyles.container}
      style={{ backgroundImage: `url(${props.image})` }}
    >
      <div className="flex flex-col items-center">
        {/* <Text className="leading-none">{ID["home.airPurifier.subtitle"]}</Text> */}
        <Text kind="h3" className="font-bold leading-none text-black">
          {props.title}
        </Text>
      </div>
      <div className={LocalTailwind.buttonContainer}>
        <ButtonLink
          className={LocalTailwind.button}
          to={"/product/"+props.id}
          label="Shop Now"
          kind="black"
        />
        <ButtonLink
          className="mx-2"
          to={ROUTES.home.airPurifier.link}
          label="More Info"
          kind="text-black"
        />
      </div>
    </section>
  );
};


const SectionEWarranty = (props: { image: string; title: string }) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex flex-col w-full items-center justify-between py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  };

  return (
    <section
      className={tailwindStyles.container}
      style={{ backgroundImage: `url(${props.image})` }}
    >
      <div className="flex flex-col items-center">
        {/* <Text className="leading-none text-white text-center"> */}
        {/*   {ID["home.eWarranty.subtitle"]} */}
        {/* </Text> */}
        <Text
          kind="h3"
          className="font-bold leading-none text-white text-center"
        >
          {props.title}
        </Text>
      </div>
      <div className="flex items-center justify-between">
        <ButtonLink
          className="mx-2"
          to={ROUTES.home.eWarranty.link}
          label={ID["home.eWarranty.ctaButton"]}
          kind="white"
        />
      </div>
    </section>
  );
};

interface ProductCardProps {
  image: string;
  desc: string;
  link: string;
}

const SectionHotPromo = (props: {
  image: string;
  desc: string;
  link: string;
}) => {
  const tailwindStyles = {
    container: "flex flex-col w-full items-center justify-between py-16",
  };
  
  const ProductCard: React.FC<ProductCardProps> = ({ image, desc, link }) => {
    return (
      <Link href={link}>
        <div className="flex flex-col justify-center p-2 rounded-lg shadow-md bg-white hover:shadow-lg mx-4 my-4 cursor-pointer">
          <div className="flex flex-col items-center justify-center">
            <img
              src={image}
              alt={image}
              className="h-72 w-72"
              style={{ width: 100, height: 100, padding: 10 }}
            />
          </div>
          <Text kind="body2" className="px-2 pb-4 text-center">
            {desc}
          </Text>
        </div>
      </Link>
    );
  };

  const [promoProduk, setPromoProduk] = React.useState<IPromoProduk[]>([]);
  const { data: promoProdukData } = usePromoProduk();
  let templateCard;

  React.useEffect(() => {
    if (promoProdukData) {
      setPromoProduk(promoProdukData.data);
    }
  }, [
    promoProdukData,
    templateCard = promoProduk.map(produk =>
      <ProductCard
        image={produk.gambar}
        desc={produk.judul}
        link={"/product/"+produk.id}
      />
      )

  ]);

  return (
    <section className={tailwindStyles.container}>
      <div className="flex flex-col items-center">
        <Text kind="h3" className="font-bold leading-none">
          {ID["home.promo.title"]}
        </Text>
        <Text kind="h6" className="leading-none">
          {ID["home.promo.subtitle"]}
        </Text>
      </div>
      <div
        className="w-full px-4 md:w-2/3 my-10 flex py-4"
        style={{
          backgroundImage: `url(${props.image})`,
          height: 300,
          backgroundSize: "contain",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
        }}
      ></div>
      <div className="flex flex-col md:flex-row justify-center items-center">
       {templateCard}
      </div>
    </section>
  );
};

const SanwooHome: React.FC<any> = () => {

  const [banner, setBanner] = React.useState<IBanner[]>([]);
  const [slideShow, setSlideShow] = React.useState<ISlideShow[]>([]);
  const [promo, setPromo] = React.useState<IPromo[]>([]);
  

  const { data: bannerData } = useBanner();
  const { data: slideShowData } = useSlideshow();
  const { data: promoData } = usePromo();
  let carouselTemplate;

  React.useEffect(() => {
    if (bannerData) {
      setBanner(bannerData.data);
    }
  }, [bannerData]);

  

  React.useEffect(() => {
    if (promoData) {
      setPromo(promoData.data);
    }
  }, [promoData]);

  React.useEffect(() => {
    if (slideShowData) {
      setSlideShow(slideShowData.data);
    }
  }, [
    slideShowData,

    carouselTemplate = slideShow.map(slide =>
      slide.kategori != "Cooler & Freezer" ?
        slide.embed == null ? <SectionSlideShow
        heading={slide.judul}
        desc={slide.keterangan}
        link={slide.link == null ? "":slide.link   }
        image={slide.gambar}
        />
        :<SectionSlideShowVideo videoUrl={slide.embed} />
        :""        
      )
  ]);

  return (
    <Layout title="Sanwoo Home">
      <Carousel arrowStyle="white">
        {carouselTemplate}
      </Carousel>

      <SectionAirFyer
        image={banner[9]?.gambar ?? ""}
        title={banner[9]?.judul ?? ""}
        id={banner[9]?.id_produk ?? "#"}
      />
      <SectionAirPurifier
        image={banner[10]?.gambar ?? ""}
        title={banner[10]?.judul ?? ""}
        id={banner[9]?.id_produk ?? "#"}
      />
      <SectionAirFryerSecond
        image={banner[18]?.gambar ?? ""}
        title={banner[18]?.judul ?? ""}
        id={banner[18]?.id_produk ?? "#"}
      />
      <SectionMultiCooker
        image={banner[22]?.gambar ?? ""}
        title={banner[22]?.judul ?? ""}
        id={banner[22]?.id_produk ?? "#"}
      />
      <SectionRiceCooker
        image={banner[26]?.gambar ?? ""}
        title={banner[26]?.judul ?? ""}
        id={banner[26]?.id_produk ?? "#"}
      />
      <SectionEWarranty
        title={banner[11]?.judul ?? ""}
        image={banner[11]?.gambar ?? ""}
      />
      <SectionHotPromo 
        image={promo[0]?.image ?? ""}
        desc={promo[0]?.judul ?? ""}
        link={promo[0]?.link ?? ""}
      />
    </Layout>
  );
};

export default SanwooHome;

import React from "react";
import { Text } from "../../components/foundations/Text";
import Layout from "../../components/layout";
import IMAGES from "@/images";
import CssUtils from "@/utils/Css";
import { ButtonLink } from "../../components/Button";
import ROUTES from "../../routes";
import useBanner from "@/integrations/hooks/useBanner";
import { IBanner } from "@/integrations/types/IBanner";
import { IProductGroup } from "@/integrations/types/IProduct";
import userProducts from "@/integrations/hooks/useProduct";

const Section__Intro = (props: {
  image: string;
  title: string;
  desc: string;
  id: number;
}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex w-full items-center justify-center py-16",
    spacer: "w-1/12",
  };

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${props.image})` }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-center md:items-start">
        <div className="mb-10">
          <Text kind="h3" className="font-bold leading-none text-white mb-4">
            {props.title}
          </Text>
          <Text className="leading-none text-white max-w-sm">{props.desc}</Text>
        </div>

        <ButtonLink
          to={"/product/"+props.id}
          label="Shop Now"
          kind="white"
        />
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  );
};

const Section__Advantage = (props: {
  leftImage: string;
  leftTitle: string;
  leftDesc: string;
  rightImage: string;
  rightTitle: string;
  rightDesc: string;
}) => {
  const tailwindStyles = {
    column:
      "h-screen md:h-screen-80 w-full md:w-1/2 bg-center bg-cover flex flex-col items-center justify-start pt-10",
    heading: "w-2/3 md:w-full text-center",
  };

  return (
    <section>
      <div className="w-full flex flex-col md:flex-row justify-between items-center">
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${props.leftImage})`,
          }}
        >
          <Text
            kind="h4"
            className={CssUtils.merge([
              tailwindStyles.heading,
              "font-bold leading-none text-white",
            ])}
          >
            {props.leftTitle}
          </Text>
          <Text className="leading-none text-white text-center w-2/3 md:w-full md:max-w-sm">
            {props.leftDesc}
          </Text>
        </div>
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${props.rightImage})`,
          }}
        >
          <Text
            kind="h4"
            className={CssUtils.merge([
              tailwindStyles.heading,
              "font-bold leading-none text-black",
            ])}
          >
            {props.rightTitle}
          </Text>
          <Text className="leading-none text-black text-center max-w-sm mb-8">
            {props.rightDesc}
          </Text>
          <img
            className="hidden md:block w-1/3"
            src={IMAGES.airFryerSub_advance_item}
            alt="airfryer-advance-items"
          />
        </div>
      </div>
    </section>
  );
};

const Section__WithoutFat = (props: {
  image: string;
  title: string;
  desc: string;
}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen flex flex-col w-full items-center justify-between py-16",
  };

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${props.image})` }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none w-2/3 md:w-full text-center">
          {props.title}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-black text-center"
        >
          {props.desc}
        </Text>
      </div>
    </section>
  );
};

interface ProductCardProps {
  image: string;
}

const Section__Products = (props: { 
    productImages: string[];
    id: number;
  }) => {
  const tailwindStyles = {
    container: "flex flex-col w-full items-center justify-between py-16",
  };
  const ProductCard: React.FC<ProductCardProps> = ({ image }) => {
    return (
      <div className="flex flex-col w-72 h-72 justify-center items-center p-2 rounded-lg shadow-sm bg-white hover:shadow-lg mx-4 my-4">
        <img src={image} alt={image} className="h-full" />
      </div>
    );
  };

  return (
    <section className={tailwindStyles.container}>
      <div className="flex flex-col w-full justify-between items-start md:items-center overflow-auto">
        <div className="flex justify-start items-center mb-10 ">
          {props.productImages.map((p) => (
            <ProductCard key={p} image={p} />
          ))}
        </div>
      </div>

      <div className="flex flex-col md:flex-row items-center justify-between mb-10">
        <ButtonLink
          className="mx-2 my-2"
          to={"/product/"+props.id}
          label="Shop Now"
          kind="black"
        />
      </div>
    </section>
  );
};

const SanwoHomeAirfryer: React.FC = () => {
  const [banner, setBanner] = React.useState<IBanner[]>([]);
  const [product, setProduct] = React.useState<IProductGroup[]>([]);

  const { data: bannerData } = useBanner();
  const { data: productData } = userProducts();

  React.useEffect(() => {
    if (bannerData) {
      setBanner(bannerData.data);
    }
  }, [bannerData]);

  React.useEffect(() => {
    if (productData) {
      setProduct(productData.data);
    }
  }, [productData]);

  return (
    <Layout title="Sanwoo Home">
      <Section__Intro
        title={banner[14]?.judul ?? ""}
        image={banner[14]?.gambar ?? ""}
        desc={banner[14]?.keterangan ?? ""}
        id={banner[14]?.id_produk ?? ""}
      />
      <Section__Advantage
        leftImage={banner[15]?.gambar ?? ""}
        leftTitle={banner[15]?.judul ?? ""}
        leftDesc={banner[15]?.keterangan ?? ""}
        rightTitle={banner[16]?.judul ?? ""}
        rightDesc={banner[16]?.keterangan ?? ""}
        rightImage={banner[16]?.gambar ?? ""}
      />
      <Section__WithoutFat
        title={banner[17]?.judul ?? ""}
        desc={banner[17]?.keterangan ?? ""}
        image={banner[17]?.gambar ?? ""}
      />
      <Section__Products
        productImages={
          // @ts-ignore
          product[0]?.["Air Fryer"]?.[0].gambar ?? []
        }
        id={banner[14]?.id_produk ?? ""}
      />
    </Layout>
  );
};

export default SanwoHomeAirfryer;

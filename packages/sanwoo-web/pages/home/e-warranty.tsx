import React from "react";
import { Text } from "../../components/foundations/Text";
import Layout from "../../components/layout";
import ID from "../../translations/id";
import { Button } from "../../components/Button";
import * as Form from "../../components/Form";
import CssUtils from "@/utils/Css";
import IMAGES from "@/images";
import { useMutation } from "react-query";
import { IWaranty } from "@/integrations/types/IEwaranty";
import postEwaranty from "@/integrations/hooks/useEwarranty";
import { IBanner } from "@/integrations/types/IBanner";
import useBanner from "@/integrations/hooks/useBanner";

const Section__EWarranty = (props: { image: string; title: string }) => {
  // TODO: Change color
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex flex-col w-full items-center justify-between py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  };

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${props.image})` }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none text-white">
          {ID["home.eWarranty.subtitle"]}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-white text-center"
        >
          {ID["home.eWarranty.title"]}
        </Text>
      </div>
    </section>
  );
};

const EWarranty: React.FC = () => {
  const [banner, setBanner] = React.useState<IBanner[]>([]);
  const { data: bannerData } = useBanner();

  React.useEffect(() => {
    if (bannerData) {
      setBanner(bannerData.data);
    }
  }, [bannerData]);

  let selectOptions: Form.SelectOption[] = [
    { value: "Topic 1", label: "Topic 1" },
    { value: "Topic 2", label: "Topic 2" },
  ];

  const mutation = useMutation((params: IWaranty) => postEwaranty(params), {
    onSuccess: (message) => {
      alert("Berhasil mengirim E-Warranty!");
      window.location.reload();  
    }
  });

  const [nama, setnama] = React.useState("");
  const [telpon, settelpon] = React.useState("");
  const [alamat1, setalamat1] = React.useState("");
  const [alamat2, setalamat2] = React.useState("");
  const [email, setemail] = React.useState("");
  const [kodepos, setkodepos] = React.useState("");
  const [provinsi, setprovinsi] = React.useState("");
  const [model, setmodel] = React.useState("");
  const [modelLain, setmodelLain] = React.useState("");
  const [serialNumber, setserialNumber] = React.useState("");
  const [tglPurchase, settglPurchase] = React.useState("");
  const [tipeUnit, settipeUnit] = React.useState("");
  
  const handleSubmit = () => {
    mutation.mutate({
      nama,
      telpon,
      alamat1,
      alamat2,
      email,
      kodepos,
      provinsi,
      model,
      modelLain,
      serialNumber,
      tglPurchase,
      tipeUnit,
    });
  };

  return (
    <Layout title="E-Warranty">
      <Section__EWarranty
         title={banner[11]?.judul ?? ""}
         image={banner[11]?.gambar ?? ""}
       />

      <section id="form-section" className="w-full flex flex-col items-center">
        <div className="flex flex-col py-5 border-t items-center justify-start w-full md:w-10/12 lg:max-w-7xl mb-10">
          <div className="w-full flex flex-col items-center pt-10">
            <Text className="leading-none">
              {ID["eWarranty.form.subtitle"]}
            </Text>
            <Text kind="h3" className="leading-none text-center mb-4 px-4">
              {ID["eWarranty.form.title"]}
            </Text>
          </div>

          <form className="w-full my-10">
            <div className="w-full grid grid-rows-2 px-4 md:px-0 md:grid-rows-none md:grid-cols-2 gap-6">
              <Form.TextField
                id={"fullname"}
                label={ID["eWarranty.form.name.label"]}
                placeholder={ID["eWarranty.form.name.label"]}
                type="text"
                onChange={(value) => setnama(value)}
              />
              <Form.TextField
                id={"email"}
                label={ID["eWarranty.form.email.label"]}
                placeholder={ID["eWarranty.form.email.label"]}
                type="email"
                onChange={(value) => setemail(value)}
              />
              <Form.TextField
                id={"phoneNumber"}
                label={ID["eWarranty.form.phoneNumber.label"]}
                placeholder={ID["eWarranty.form.phoneNumber.label"]}
                type="number"
                
                onChange={(value) => settelpon(value)}
              />
              <div></div>
              <Form.TextArea
                onChange={(value) => setalamat1(value)}
                id={"street-address-1"}
                label={ID["eWarranty.form.streetAddress1.label"]}
                placeholder={ID["eWarranty.form.streetAddress1.label"]}
                type="text"
              />
              <Form.TextArea
                onChange={(value) => setalamat2(value)}
                id={"street-address-2"}
                label={ID["eWarranty.form.streetAddress2.label"]}
                placeholder={ID["eWarranty.form.streetAddress2.label"]}
                type="text"
              />
              <Form.TextField
                onChange={(value) => setkodepos(value)}
                id={"postal-code"}
                label={ID["eWarranty.form.postalCode.label"]}
                placeholder={ID["eWarranty.form.postalCode.label"]}
                type="number"
              />
              <Form.TextField
                onChange={(value) => setprovinsi(value)}
                id={"province"}
                label={ID["eWarranty.form.province.label"]}
                placeholder={ID["eWarranty.form.province.label"]}
                type="text"
              />
            </div>
            <hr className="mx-4 md:mx-0 my-10" />

            <div className="w-full grid grid-rows-2 px-4 md:px-0 md:grid-rows-none md:grid-cols-2 gap-6">
              <Form.TextField
                onChange={(value) => setmodel(value)}
                id={"model"}
                label={ID["eWarranty.form.model.label"]}
                placeholder={ID["eWarranty.form.model.label"]}
                type="text"
              />
              <Form.TextField
                onChange={(value) => setmodelLain(value)}
                id={"other-model"}
                label={ID["eWarranty.form.otherModel.label"]}
                placeholder={ID["eWarranty.form.otherModel.label"]}
                type="number"
              />
              <Form.TextField
                onChange={(value) => setserialNumber(value)}
                id={"serial-number"}
                label={ID["eWarranty.form.serialNumber.label"]}
                placeholder={ID["eWarranty.form.serialNumber.label"]}
                type="text"
              />
              <Form.TextField
                onChange={(value) => settglPurchase(value)}
                id={"date-of-purchase"}
                label={ID["eWarranty.form.dateOfPurchase.label"]}
                placeholder={ID["eWarranty.form.dateOfPurchase.label"]}
                type="date"
              />
              <Form.TextField
                onChange={(value) => settipeUnit(value)}
                id={"type-of-unit"}
                label={ID["eWarranty.form.typeOfUnit.label"]}
                placeholder={ID["eWarranty.form.typeOfUnit.label"]}
                type="text"
              />
            </div>
            <div className="w-full flex justify-center items-center mt-10">
              <Button
                onClick={() => handleSubmit()}
                kind="primary"
                label="Submit"
                className="w-64"
              />
            </div>
          </form>
        </div>
      </section>
    </Layout>
  );
};

export default EWarranty;

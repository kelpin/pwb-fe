import React from "react";
import { Text } from "@/components/foundations/Text";
import Layout from "@/components/layout";
import IMAGES from "@/images";
import ID from "@/translations/id";
import { ButtonLink } from "@/components/Button";
import ROUTES from "@/routes";
import { IBanner } from "@/integrations/types/IBanner";
import { IProductGroup } from "@/integrations/types/IProduct";
import useBanner from "@/integrations/hooks/useBanner";
import userProducts from "@/integrations/hooks/useProduct";

const Section__Intro = (props: {
  image: string;
  title: string;
  desc: string;
  id: number;
}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    spacer: "w-1/12",
  };

  return (
    <section
      className={tailwindStyles.container}
      style={{ backgroundImage: `url(${props.image})` }}
    >
      <div className={tailwindStyles.spacer}></div>

      <div className="flex-1 flex flex-col items-center md:items-start">
        <div className="mb-10">
          <Text kind="h3" className="font-bold leading-none text-black mb-4">
            {props.title}
          </Text>
          <Text className="leading-none text-black max-w-sm">{props.desc}</Text>
        </div>

        <ButtonLink
          to={"/product/"+props.id}
          label="Shop Now"
          kind="black"
        />
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  );
};

const Section__Advantage = () => {
  const tailwindStyles = {
    column:
      "h-screen md:h-screen-80 w-full md:w-1/2 bg-center bg-cover flex flex-col items-center justify-start pt-10",
    heading: "w-2/3 md:w-full text-center",
  };

  return (
    <section>
      <div className="w-full flex flex-col md:flex-row justify-between items-center">
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${IMAGES.airPurifierSub_3in1})`,
          }}
        >
          <Text
            kind="h4"
            className={
              tailwindStyles.heading +
              " " +
              "font-bold leading-none text-white mb-2 md:mb-0"
            }
          >
            {ID["airPurifier.section.3in1.title"]}
          </Text>
          <Text className="leading-none text-white text-center w-2/3 md:w-full md:max-w-sm">
            {ID["airPurifier.section.3in1.subtitle"]}
          </Text>
        </div>
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${IMAGES.airPurifierSub_removesOdor})`,
          }}
        >
          <Text
            kind="h4"
            className={
              tailwindStyles.heading + " font-bold leading-none text-black"
            }
          >
            {ID["airPurifier.section.removeOdor.title"]}
          </Text>
          <Text className="leading-none text-black w-2/3 md:w-full text-center max-w-sm mb-8">
            {ID["airPurifier.section.removeOdor.subtitle"]}
          </Text>
        </div>
      </div>
    </section>
  );
};

const Section__BreatheEasy = () => {
  const tailwindStyles = {
    container:
      "h-screen bg-cover bg-center flex flex-col w-full items-center justify-between py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  };

  return (
    <section
      className={tailwindStyles.container}
      style={{ backgroundImage: `url(${IMAGES.airPurifierBreatheEasy})` }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none w-2/3 md:w-full text-center">
          {ID["airPurifier.section.breatheEasy.title"]}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-black text-center"
        >
          {ID["airPurifier.section.breatheEasy.subtitle"]}
        </Text>
      </div>
    </section>
  );
};

interface ProductCardProps {
  image: string;
}

const Section__Products = (props: { 
    productImages: string[],
    id: number
   }) => {
  const tailwindStyles = {
    container: "flex flex-col w-full items-center justify-between py-16",
  };
  const ProductCard: React.FC<ProductCardProps> = ({ image }) => {
    return (
      <div className="flex flex-col w-72 h-72 justify-center items-center p-2 rounded-lg shadow-sm bg-white hover:shadow-lg mx-4 my-4">
        <img src={image} alt={image} className="h-full" />
      </div>
    );
  };

  return (
    <section className={tailwindStyles.container}>
      <div className="flex flex-col w-full justify-between items-start md:items-center overflow-auto">
        <div className="flex justify-start items-center mb-10 ">
          {props.productImages.map((p) => (
            <ProductCard key={p} image={p} />
          ))}
        </div>
      </div>

      <div className="flex flex-col md:flex-row items-center justify-between mb-10">
        <ButtonLink
          className="mx-2 my-2"
          to={"/product/"+props.id}
          label="Shop Now"
          kind="black"
        />
      </div>
    </section>
  );
};

const SanwooAirPurifier: React.FC = () => {
  const [banner, setBanner] = React.useState<IBanner[]>([]);
  const [product, setProduct] = React.useState<IProductGroup[]>([]);

  const { data: bannerData } = useBanner();
  const { data: productData } = userProducts();

  React.useEffect(() => {
    if (bannerData) {
      setBanner(bannerData.data);
    }
  }, [bannerData]);

  React.useEffect(() => {
    if (productData) {
      setProduct(productData.data);
    }
  }, [productData]);
  return (
    <Layout title="Sanwoo Air Purifier">
      <Section__Intro
        title={banner[10]?.judul ?? ""}
        image={banner[10]?.gambar ?? ""}
        desc={banner[10]?.keterangan ?? ""}
        id={banner[10]?.id_produk ?? ""}
      />
      {/* <Section__Advantage /> */}
      {/* <Section__BreatheEasy /> */}
      <Section__Products
        productImages={
          // @ts-ignore
          product[0]?.["Air Purifier"]?.[0].gambar ?? []
        }
        id={banner[10]?.id_produk ?? ""}
      />
    </Layout>
  );
};

export default SanwooAirPurifier;

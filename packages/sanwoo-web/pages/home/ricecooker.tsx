import React from "react";
import { Text } from "../../components/foundations/Text";
import Layout from "../../components/layout";
import IMAGES from "@/images";
import CssUtils from "@/utils/Css";
import { ButtonLink } from "../../components/Button";
import ROUTES from "../../routes";
import { IBanner } from "@/integrations/types/IBanner";
import useBanner from "@/integrations/hooks/useBanner";

const Section__Intro = (props: {
  image: string;
  title: string;
  desc: string;
  id: number;
}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex w-full items-center justify-center py-16",
    spacer: "w-1/12",
  };

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${props.image})` }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-center md:items-start">
        <div className="mb-10">
          <Text kind="h3" className="font-bold leading-none mb-4 text-white">
            {props.title}
          </Text>
          <Text className="leading-none max-w-sm text-white">
            {props.desc}
          </Text>
        </div>

        <ButtonLink
          to={"/product/"+props.id}
          label="Shop Now"
          kind="white"
        />
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  );
};

const Section__TwoColumns = (props: {
  image1: string;
  title1: string;
  desc1: string;
  image2: string;
  title2: string;
  desc2: string;
}) => {
  const tailwindStyles = {
    column:
      "h-screen md:h-screen-80 w-full md:w-1/2 bg-center bg-contain flex flex-col items-center justify-start pt-10",
    heading: "w-2/3 md:w-full text-center",
  };

  return (
    <section>
      <div className="w-full flex flex-col md:flex-row justify-between items-center">
        <div
          className={tailwindStyles.column}
          style={{
            backgroundColor: "#5F666C",
            backgroundImage: `url(${props.image1})`,
            backgroundRepeat: "no-repeat",
          }}
        ></div>
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${props.image2})`,
            backgroundColor: "#111",
            backgroundRepeat: "no-repeat",
          }}
        ></div>
      </div>
    </section>
  );
};

const Section__Multifunction = (props: {
  image: string;
  title: string;
  desc: string;
}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen flex flex-col w-full items-center justify-between py-16",
  };

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${props.image})` }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none w-2/3 md:w-full text-center">
          {props.desc}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-black text-center"
        >
          {props.title}
        </Text>
      </div>
    </section>
  );
};

interface ProductCardProps {
  image: string;
}

const Section__Products = (props: {
  id: number;
}) => {
  const tailwindStyles = {
    container: "flex flex-col w-full items-center justify-between py-16",
  };
  const ProductCard: React.FC<ProductCardProps> = ({ image }) => {
    return (
      <div className="flex flex-col w-72 h-72 justify-center items-center p-2 rounded-lg shadow-sm bg-white hover:shadow-lg mx-4 my-4">
        <img src={image} alt={image} className="h-full" />
      </div>
    );
  };

  return (
    <section className={tailwindStyles.container}>
      <div className="flex flex-col w-full justify-between items-start md:items-center overflow-auto">
        <div className="flex justify-start items-center mb-10 ">
          <ProductCard
            image={IMAGES["Sanwoo Multifunction Low Sugar Rice Cooker"][0]}
          />
          <ProductCard
            image={IMAGES["Sanwoo Multifunction Low Sugar Rice Cooker"][2]}
          />
          <ProductCard
            image={IMAGES["Sanwoo Multifunction Low Sugar Rice Cooker"][1]}
          />
        </div>
      </div>

      <div className="flex flex-col md:flex-row items-center justify-between mb-10">
        <ButtonLink
          className="mx-2 my-2"
          to={"/product/"+props.id}
          label="Shop Now"
          kind="black"
        />
      </div>
    </section>
  );
};

const SanwoHomeRiceCooker: React.FC = () => {

  const [banner, setBanner] = React.useState<IBanner[]>([]);

  const { data: bannerData } = useBanner();

  React.useEffect(() => {
    if (bannerData) {
      setBanner(bannerData.data);
    }
  }, [bannerData]);

  return (
    <Layout title="Sanwoo Home">
      <Section__Intro
        image={banner[26]?.gambar ?? ""}
        title={banner[26]?.judul ?? ""}
        id={banner[26]?.id_produk ?? "#"}
       />
      <Section__TwoColumns
        image1={banner[27]?.gambar ?? ""}
        title1={banner[27]?.judul ?? ""}
        desc1={banner[27]?.keterangan ?? ""}
        image2={banner[28]?.gambar ?? ""}
        title2={banner[28]?.judul ?? ""}
        desc2={banner[28]?.keterangan ?? ""}
      />
      <Section__Multifunction 
        image={banner[29]?.gambar ?? ""}
        title={banner[29]?.judul ?? ""}
        desc={banner[29]?.keterangan ?? "#"}
      />
      <Section__Products 
        id={banner[26]?.id_produk ?? "#"}
      />
    </Layout>
  );
};

export default SanwoHomeRiceCooker;

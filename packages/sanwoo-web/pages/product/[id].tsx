import React, { ReactElement } from "react";
import { Text } from "@/components/foundations/Text";
import Layout from "@/components/layout";
import CssUtils from "@/utils/Css";
// import Breadcrumb from "@/components/Breadcrumb";
import BuyModal from "@/components/BuyModal";
import { Button } from "@/components/Button";
// import Breadcrumb from "@/components/Breadcrumb";
import axios, { AxiosRequestConfig } from "axios";
import { IProduk } from "@/types/product";
import { IEcommerce } from "@/integrations/types/IEcommerce";
import useEcommerce from "@/integrations/hooks/useEcommerce";

const ChevronBottom = () => {
  return (
    <div>
      <svg
        className="fill-current h-6 w-6"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
      >
        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
      </svg>
    </div>
  );
};

type SpecType = {
  label: string;
  value: string;
};

const Accordion = ({
  children,
  title,
}: {
  children: ReactElement;
  title: string;
}) => {
  const [showAccordion, toggleShowAccordion] = React.useState(true);

  return (
    <div className="accordion mb-10">
      <div className="w-full border-b py-3 flex justify-between items-center">
        <Text kind="h5" className="font-bold uppercase">
          {title}
        </Text>
        <div
          className={CssUtils.merge([
            showAccordion
              ? "transform hover:opacity-75 transition ease-in duration-200"
              : "transform -rotate-90 hover:rotate-0 hover:opacity-50 transition ease-in duration-200",
            "cursor-pointer",
          ])}
          onClick={() => toggleShowAccordion(!showAccordion)}
        >
          <ChevronBottom />
        </div>
      </div>
      {showAccordion && children}
    </div>
  );
};

const ButtonImage = ({
  image,
  link,
  disabled,
}: {
  image: string;
  link: string;
  disabled: boolean;
}) => {
  let calculatedLink = disabled ? "" : link;
  let calculatedStyles = disabled ? "opacity-75 cursor-not-allowed" : "";
  let calculatedTarget = disabled ? "" : "_blank";

  if (disabled) {
    return (
      <div
        className={"flex items-center justify-center" + " " + calculatedStyles}
      >
        <div className="rounded h-full rounded-full w-56 shadow px-16 py-3 text-center bg-white">
          <img src={image} className=" h-full w-full object-contain " />
        </div>
      </div>
    );
  } else {
    return (
      <a
        href={calculatedLink}
        target={calculatedTarget}
        className={"flex items-center justify-center" + " " + calculatedStyles}
      >
        <div className="rounded h-full rounded-full w-56 shadow px-16 py-3 text-center bg-white">
          <img src={image} className=" h-full w-full object-contain " />
        </div>
      </a>
    );
  }
};

const Product = (props: any) => {
  const [showBuyModal, setShowBuyModal] = React.useState(false)
  const [ecommerce, setEcommerce] = React.useState<IEcommerce[]>([]);

  const { data: ecommerceData } = useEcommerce();
  let btnTemplate;  

  React.useEffect(() => {
    if (ecommerceData) {
      setEcommerce(ecommerceData.data);
    }
  }, [
    ecommerceData,

    btnTemplate = ecommerce.map(ecom =>
        <ButtonImage
        disabled={false}
        image={ecom.gambar}
        link={ecom.link}
      />
      )
  ]);


  React.useEffect(() => {
    const fetchProduct = () => {
      let data = new FormData();
      data.append("id_produk", props.id);
      const config: AxiosRequestConfig = {
        method: "post",
        url: "http://sanwoo.powerboardcms.com/api/client/product/detail",
        headers: {
          Authorization: "Basic c2Fud29vOnNhbndvbzEyMzQ1Ng==",
        },
        data: data,
      };

      axios(config).then((res) => {
        setProduct(res.data.data);
        setActiveImage(res.data.data.produk.gambar[0]);
      });
    };

    fetchProduct();
  }, []);

  let [activeImage, setActiveImage] = React.useState("");

  let renderImage: React.FC<Array<string>> = (images) => {
    return (
      <div className="w-full">
        <div className="flex flex-col justify-center items-center p-2 rounded-lg shadow-md bg-white hover:shadow-lg mb-5 h-screen-50">
          <img
            src={activeImage}
            alt={"active-image"}
            className="h-full w-full object-contain"
          />
        </div>
        <div className="grid grid-cols-3 gap-2 md:gap-6 h-32 md:h56">
          {images &&
            images.map((image) => {
              return (
                <div
                  className="relative flex flex-col items-center justify-center rounded-lg shadow-md bg-white hover:shadow-lg cursor-pointer"
                  onClick={() => setActiveImage(image)}
                >
                  <img
                    src={image}
                    alt={"active-image"}
                    className="absolute md:p-2 h-full w-full object-contain"
                  />
                </div>
              );
            })}
        </div>
      </div>
    );
  };

  const [product, setProduct] = React.useState<IProduk | undefined>();
  const buyModalProps = () => {   
  }
  return (
    <>
      {showBuyModal && (
        <div
        className="fixed w-full h-screen z-50 flex justify-center items-center"
        style={{ background: "rgba(0,0,0,0.5)" }}
      >
        <div
          className="relative mx-4 px-6 py-6 md:py-0 md:px-0 h-screen-80 md:h-screen-50 md:w-2/3  rounded-xl flex flex-col justify-center items-center"
          style={{
            background:
              "linear-gradient(180deg, #FEFEFF 20%, rgb(245, 245, 255) 55%,  #E9E9FF 100%)",
          }}
        >
          <div
            className="absolute my-10 mr-10 top-0 right-0 cursor-pointer"
            onClick={() => setShowBuyModal(false)}
          >
            x
          </div>
          <Text kind="h3" className="text-h5 md:text-h3 text-primary mb-10">
            Buy Now
          </Text>
          <div className="w-full md:w-2/3 grid grid-rows-4 md:grid-rows-none md:grid-cols-2 gap-10  overflow-y-auto">
            {btnTemplate}
          </div>
        </div>
      </div>
      )}
      
      <Layout title={product?.produk.judul}>
        <div className="w-full flex flex-col items-center">
          <section className="py-5 w-full md:w-10/12 lg:max-w-7xl mb-10 flex flex-col md:grid md:grid-cols-2 md:gap-6">
            <div className="block md:hidden">
              <Text kind="h3" className="font-bold px-5">
                {product?.produk.judul ?? ""}
              </Text>
            </div>
            <div className="px-8 md:pt-8">
              {/* <div className="id-breadcrumb mb-5"> */}
              {/*   <Breadcrumb url={internalUrl} title={name} /> */}
              {/* </div> */}
              {renderImage(product?.produk.gambar ?? [])}

              <div className="mt-16 mb-20 w-full flex justify-center items-center">
                <Button
                  label="Buy Now"
                  kind="primary"
                  className="w-full"
                  onClick={() => setShowBuyModal(true)}
                />
              </div>
            </div>
            <div className="px-4 md:px-0 md:pl-8 md:pt-8">
              <div className="hidden md:block">
                <Text kind="h3" className="font-bold">
                  {product?.produk.judul ?? "-"}
                </Text>
              </div>

              <Accordion title="Product Overview">
                <Text kind="body" className="my-2 uppercase">
                  {product?.produk.overview ?? "-"}
                </Text>
              </Accordion>

              <Accordion title="Product Specifications">
                <div className="grid grid-cols-2 gap-2 gap-y-2 my-3">
                  {product?.spesifikasi.map(({ judul, deskripsi }) => (
                    <>
                      <Text kind="body" className="font-bold uppercase">
                        {judul}
                      </Text>
                      <Text
                        kind="body"
                        className="uppercase whitespace-pre-line"
                      >
                        {deskripsi}
                      </Text>
                    </>
                  ))}
                </div>
              </Accordion>
            </div>
          </section>
        </div>
      </Layout>
    </>
  );
};

export async function getServerSideProps(context: {
  params: { id: string };
  req: any;
}) {
  return { props: { id: context.params.id } };
}

export default Product;

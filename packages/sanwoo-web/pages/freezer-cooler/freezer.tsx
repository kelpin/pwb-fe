import React from "react";
import Layout from "../../components/layout";
import { Text } from "../../components/foundations/Text";
import { ListItem } from "../../components/ListItem";
import { ButtonLink } from "../../components/Button";
import CssUtils from "@/utils/Css";
import ID from "../../translations/id";
import { freezers, ProductType } from "../../constants/contents";
import useBanner from "@/integrations/hooks/useBanner";
import { IBanner } from "@/integrations/types/IBanner";
import { IProduct, IProductGroup } from "@/integrations/types/IProduct";
import userProducts from "@/integrations/hooks/useProduct";

const renderFreezerList = (arrOfCooler: Array<IProduct>) => {
  return arrOfCooler.map((p) => (
    <ListItem
      image={p.gambar[0] ?? ""}
      title={p.judul ?? ""}
      desc={p.judul ?? ""}
      to={p.id ?? ""}
    />
  ));
};

const Section__Intro = (props: {
  title: string;
  desc: string;
  image: string;
}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    spacer: "w-1/12",
  };

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${props.image})`,
      }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-start ">
        <div className="mb-10">
          <Text kind="h3" className="font-bold leading-none text-primary mb-4">
            {props.title}
          </Text>
          <Text className="leading-none text-black max-w-sm">{props.desc}</Text>
        </div>

        <ButtonLink to="#items-list" label="Shop Now" kind="primary" />
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  );
};

const Section__Advantage = (props: {
  leftImage: string;
  leftDesc: string;
  leftTitle: string;
  rightImage: string;
  rightDesc: string;
  rightTitle: string;
}) => {
  const tailwindStyles = {
    column:
      "h-screen-80 h-full w-full md:w-1/2 md:bg-bottom bg-contain bg-no-repeat bg-bottom flex flex-col items-center justify-start pt-10 text-center px-4",
  };

  return (
    <section>
      <div className="w-full flex flex-col md:flex-row justify-between items-center">
        <div
          className={CssUtils.merge([tailwindStyles.column])}
          style={{
            backgroundColor: "#363636",
            backgroundImage: `url(${props.leftImage})`,
          }}
        >
          <Text
            kind="h4"
            className="font-bold leading-none text-white text-center"
          >
            {props.leftTitle}
          </Text>
          <Text className="leading-none text-white text-center max-w-sm">
            {props.leftDesc}
          </Text>
        </div>
        <div
          className={tailwindStyles.column}
          style={{
            backgroundColor: "#fff",
            backgroundImage: `url(${props.rightImage})`,
          }}
        >
          <Text kind="h4" className="font-bold leading-none text-primary">
            {props.rightTitle}
          </Text>
          <Text className="leading-none text-black text-center max-w-sm mb-8">
            {props.rightDesc}
          </Text>
        </div>
      </div>
    </section>
  );
};

const FreezerHome: React.FC = () => {
  const [banner, setBanner] = React.useState<IBanner[]>([]);
  const [product, setProduct] = React.useState<IProductGroup[]>([]);

  const { data: bannerData } = useBanner();
  const { data: productData } = userProducts();

  React.useEffect(() => {
    if (bannerData) {
      setBanner(bannerData.data);
    }
  }, [bannerData]);

  React.useEffect(() => {
    if (productData) {
      setProduct(productData.data);
    }
  }, [productData]);

  return (
    <Layout title="Freezer Home">
      <Section__Intro
        title={banner[5]?.judul ?? ""}
        image={banner[5]?.gambar ?? ""}
        desc={banner[5]?.keterangan ?? ""}
      />
      <Section__Advantage
        leftTitle={banner[6]?.judul ?? ""}
        leftImage={banner[6]?.gambar ?? ""}
        leftDesc={banner[6]?.keterangan ?? ""}
        rightTitle={banner[7]?.judul ?? ""}
        rightImage={banner[7]?.gambar ?? ""}
        rightDesc={banner[7]?.keterangan ?? ""}
      />
      <div
        id="items-list"
        className="w-full flex flex-col items-center justify-center pt-10 pb-32"
      >
        <Text kind="h5" className="leading-none font-light">
          {ID["freezer.itemsSubtitle"]}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-primary mb-10 text-center"
        >
          {ID["freezer.itemsTitle"]}
        </Text>
        <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 md:gap-8 w-full md:w-10/12 lg:max-w-6xl px-4">
          {
            // @ts-ignore
            renderFreezerList(product[0]?.["Freezer"] ?? [])
          }
        </div>
      </div>
    </Layout>
  );
};

export default FreezerHome;

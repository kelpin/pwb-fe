import React from "react";
import Layout from "../../components/layout";
import { Text } from "../../components/foundations/Text";
import { ListItem } from "../../components/ListItem";
import { ButtonTab } from "../../components/ButtonTab";
import { ButtonLink } from "../../components/Button";
import CssUtils from "@/utils/Css";
import ID from "../../translations/id";
import useBanner from "@/integrations/hooks/useBanner";
import { IBanner } from "@/integrations/types/IBanner";
import { IProduct, IProductGroup } from "@/integrations/types/IProduct";
import userProducts from "@/integrations/hooks/useProduct";
import { ICategory } from "@/integrations/types/ICategory";
import useCategori from "@/integrations/hooks/useCategory";

const renderProducts = (arrOfCooler: Array<IProduct>) => {
  return arrOfCooler.map((p) => (
    <ListItem
      image={p.gambar[0] ?? ""}
      title={p.judul ?? ""}
      desc={p.judul ?? ""}
      to={p.id ?? ""}
    />
  ));
};

const SectionIntro = (props: {
  title: string;
  desc: string;
  image: string;
}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    spacer: "w-1/12",
  };

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${props.image})`,
      }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-start ">
        <div className="mb-10">
          <Text kind="h3" className="font-bold leading-none text-primary mb-4">
            {props.title}
          </Text>
          <Text className="leading-none text-black max-w-sm">{props.desc}</Text>
        </div>

        <ButtonLink to={"#items-list"} label="Shop Now" kind="primary" />
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  );
};

const SectionAdvantage = (props: {
  leftTitle: string;
  leftDesc: string;
  leftImage: string;
  rightTitle: string;
  rightDesc: string;
  rightImage: string;
}) => {
  const tailwindStyles = {
    column:
      "h-screen-80 h-full w-full md:w-1/2 md:bg-bottom  bg-no-repeat bg-bottom flex flex-col items-center justify-start pt-10",
  };

  return (
    <section>
      <div className="w-full flex flex-col md:flex-row justify-between items-center">
        <div
          className={tailwindStyles.column}
          style={{
            backgroundColor: "#363636",
            backgroundImage: `url(${props.leftImage})`,
            backgroundSize: "cover",
          }}
        >
          <Text kind="h4" className="font-bold leading-none text-white">
            {props.leftTitle}
          </Text>
          <Text className="leading-none text-white text-center max-w-sm px-4">
            {props.leftDesc}
          </Text>
        </div>
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${props.rightImage})`,
            backgroundSize: "cover",
          }}
        >
          <Text kind="h4" className="font-bold leading-none text-primary">
            {props.rightTitle}
          </Text>
          <Text className="leading-none text-black text-center max-w-sm mb-8 px-4">
            {props.rightDesc}
          </Text>
        </div>
      </div>
    </section>
  );
};

const CoolerHome = () => {
  const [coolerType, setCoolerType] = React.useState<string>("all");
  const [banner, setBanner] = React.useState<IBanner[]>([]);
  const [category, setCategory] = React.useState<ICategory[]>([]);

  const { data: bannerData } = useBanner();
  const { data: productData } = userProducts();
  const { data: categoryData } = useCategori();

  const [product, setProduct] = React.useState<IProductGroup[]>([]);

  let dataKategori;

  React.useEffect(() => {
    if (bannerData) {
      setBanner(bannerData.data);
    }
  }, [bannerData]);

  React.useEffect(() => {
    if (categoryData) {
      setCategory(categoryData.data);
    }
  }, [
    categoryData,
    dataKategori = category.map(cate =>
      cate.page != "Home"?
      <ButtonTab
        label={cate.judul? cate.judul : "" }
        onClick={() => setCoolerType(cate.judul? cate.judul : "")}
        active={coolerType === cate.judul}
      />:""
    ),
  ]);

  React.useEffect(() => {
    if (productData) {
      setProduct(productData.data);
    }
  }, [productData]);

  const renderTabContainer = (): React.ReactElement => {
    return (
      <div className="flex flex-row md:w-12/12 w-full px-4 lg:max-w-6xl md:grid md:grid-cols-5 mb-10 overflow-x-auto">
        <ButtonTab
          label="All"
          onClick={() => setCoolerType("all")}
          active={coolerType === "all"}
        />
        {dataKategori}
      </div>
    );
  };

  const renderActiveProduct = () => {
    switch (coolerType) {
      case "Cooler":
        // @ts-ignore
        return renderProducts(product[0]?.["Cooler"] ?? []);

      case "Mini Cooler":
        // @ts-ignore
        return renderProducts(product[0]?.["Mini Cooler"] ?? []);

      case "Hybrid Cooler":
        // @ts-ignore
        return renderProducts(product[0]?.["Hybrid Cooler"] ?? []);

      case "2 in 1 cooler with freezer on top":
        return renderProducts(
          // @ts-ignore
          product[0]?.["2 in 1 cooler with freezer on top"] ?? []
        );

      case "all":
      default:
        // @ts-ignore
        return renderProducts(product[1]?.["all"] ?? []);
    }
  };

  return (
    <Layout title="Cooler Home">
      <SectionIntro
        title={banner[2]?.judul ?? ""}
        image={banner[2]?.gambar ?? ""}
        desc={banner[2]?.keterangan ?? ""}
      />
      <SectionAdvantage
        leftImage={banner[3]?.gambar ?? ""}
        leftTitle={banner[3]?.judul ?? ""}
        leftDesc={banner[3]?.keterangan ?? ""}
        rightImage={banner[4]?.gambar ?? ""}
        rightTitle={banner[4]?.judul ?? ""}
        rightDesc={banner[4]?.keterangan ?? ""}
      />

      <div
        id="items-list"
        className="w-full flex flex-col items-center justify-center py-10 pb-32"
      >
        <Text kind="h5" className="leading-none font-light text-center">
          {ID["cooler.itemsSubtitle"]}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-primary mb-10 text-center"
        >
          {ID["cooler.itemsTitle"]}
        </Text>
        {renderTabContainer()}
        <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4  md:w-10/12 gap-4 md:gap-8  w-full px-4 lg:max-w-6xl">
          {renderActiveProduct()}
        </div>
      </div>
    </Layout>
  );
};

export default CoolerHome;

import React from "react";
import { SectionTwoColumns } from "../../components/layouts/SectionTwoColumns";
import { Text } from "../../components/foundations/Text";
import Layout from "../../components/layout";
import CssUtils from "@/utils/Css";
import { IService } from "@/integrations/types/IService";
import useService from "@/integrations/hooks/useService";
import { IBanner } from "@/integrations/types/IBanner";
import useBanner from "@/integrations/hooks/useBanner";

const SectionIntro = (props: {
  title: string;
  desc: string;
  image: string;
}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    spacer: "w-1/12",
  };

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${props.image})` }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-center md:items-start">
        <Text
          kind="h3"
          className="leading-4 md:leading-normal font-bold text-primary mb-4 px-3 md:px-0"
        >
          {props.title}
        </Text>
        <Text
          kind="h5"
          className="md:leading-relaxed max-w-sm md:max-w-2xl lg:max-w-4xl text-primary w-screen md:w-full font-light px-3 md:px-0"
        >
          {props.desc}
        </Text>
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  );
};

const CoolerHome: React.FC<any> = () => {
  const [service, setService] = React.useState<IService[]>([]);
  const [banner, setBanner] = React.useState<IBanner[]>([]);

  const { data: bannerData } = useBanner();
  const { data: serviceData } = useService();

  React.useEffect(() => {
    if (serviceData) {
      setService(serviceData.data);
    }
  }, [serviceData]);

  React.useEffect(() => {
    if (bannerData) {
      setBanner(bannerData.data);
    }
  }, [bannerData]);

  return (
    <Layout title="Service">
      <SectionIntro
        image={banner[8]?.gambar ?? ""}
        title={banner[8]?.judul ?? ""}
        desc={banner[8]?.keterangan ?? ""}
      />

      <SectionTwoColumns image={service[0]?.gambar ?? ""} imagePosition="left">
        <Text kind="h3" className="text-primary mb-4">
          {service[0]?.judul ?? ""}
        </Text>
        <Text kind="body">{service[0]?.deskripsi ?? ""}</Text>
      </SectionTwoColumns>

      <SectionTwoColumns image={service[1]?.gambar ?? ""} imagePosition="right">
        <Text kind="h3" className="text-primary mb-4">
          {service[1]?.judul ?? ""}
        </Text>

        <Text kind="body">{service[1]?.deskripsi ?? ""}</Text>
      </SectionTwoColumns>

      <SectionTwoColumns image={service[2]?.gambar ?? ""} imagePosition="left">
        <Text kind="h3" className="text-primary mb-4">
          {service[2]?.judul ?? ""}
        </Text>
        <Text kind="body">{service[2]?.deskripsi ?? ""}</Text>
      </SectionTwoColumns>
    </Layout>
  );
};

export default CoolerHome;

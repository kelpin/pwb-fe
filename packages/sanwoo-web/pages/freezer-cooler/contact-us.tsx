import React from "react";
import { Text } from "../../components/foundations/Text";
import Layout from "../../components/layout";
import ID from "../../translations/id";
import { Button } from "../../components/Button";
import * as Form from "../../components/Form";
import { useMutation } from "react-query";
import { IMessage } from "@/integrations/types/IMessage";
import axios, { AxiosRequestConfig } from "axios";
import postMessage from "@/integrations/hooks/useMessage";
import { useRouter } from 'next/router';

const ContactUs: React.FC<any> = () => {
  let selectOptions: Form.SelectOption[] = [
    { value: "General Inquiry", label: "General Inquiry" },
    {
      value: "Maintenance & Refurbishment",
      label: "Maintenance & Refurbishment",
    },
    { value: "Stickering", label: "Stickering" },
    { value: "Placement", label: "Placement" },
  ];
  const mutation = useMutation((message: IMessage) => postMessage(message), {
    // onSuccess: () => alert("Berhasil mengirim pesan!"),
    onSuccess: (message) => {
      alert("Berhasil mengirim pesan!");
      window.location.reload();
    }
  });

  const [formTitle, setFormTitle] = React.useState("");
  const [formPhoneNumber, setFormPhoneNumber] = React.useState("");
  const [formEmail, setFormEmail] = React.useState("");
  const [formTopic, setFormTopic] = React.useState("");
  const [formMessage, setFormMessage] = React.useState("");

  const handleSubmit = () => {
    console.log({
      formTitle,
      formPhoneNumber,
      formEmail,
      formTopic,
      formMessage,
    });

    mutation.mutate({
      nama: formTitle,
      email: formEmail,
      pesan: formMessage,
      telp: formPhoneNumber,
      topik: formTopic,
    });
  };

  return (
    <Layout title="Contact Us">
      <section id="form-section" className="w-full flex flex-col items-center">
        <div className="flex flex-col py-5 border-t items-center justify-start w-full md:w-10/12 lg:max-w-7xl">
          <div className="w-full flex flex-col items-center pt-10">
            <Text
              kind="h3"
              className="leading-none text-primary text-center mb-4"
            >
              {ID["contactUs.title"]}
            </Text>

            <Text
              kind="body"
              className="leading-relax text-primary text-center w-2/3"
            >
              {ID["contactUs.desc"]}
            </Text>
          </div>

          <form className="w-full my-10">
            <div className="w-full grid grid-rows-2 md:grid-rows-none md:grid-cols-2 px-4 md:px-0 gap-6">
              <Form.TextField
                id={"fullname"}
                label={ID["contactUs.form.fullname.label"]}
                placeholder={ID["contactUs.form.fullname.label"]}
                type="text"
                onChange={(value) => setFormTitle(value)}
              />
              <Form.PhoneField
                id={"phoneNumber"}
                label={ID["contactUs.form.phoneNumber.label"]}
                placeholder={ID["contactUs.form.phoneNumber.label"]}
                onChange={(value) => setFormPhoneNumber(value)}
              />
              <Form.TextField
                id={"email"}
                label={ID["contactUs.form.emailAddress.label"]}
                placeholder={ID["contactUs.form.emailAddress.label"]}
                type="email"
                onChange={(value) => setFormEmail(value)}
              />
              <Form.Select
                id={"topic"}
                label={ID["contactUs.form.selectTopic.label"]}
                placeholder={ID["contactUs.form.selectTopic.label"]}
                options={selectOptions}
                onChange={(value) => setFormTopic(value)}
              />
              <Form.TextArea
                id={"select-message"}
                label={ID["contactUs.form.selectMessages.label"]}
                placeholder={ID["contactUs.form.selectMessages.label"]}
                type="email"
                onChange={(value) => setFormMessage(value)}
              />
            </div>

            <div className="w-full flex justify-center items-center mt-10">
              <Button
                kind="primary"
                label="Submit"
                className="w-64"
                onClick={() => handleSubmit()}
              />
            </div>
          </form>
        </div>
      </section>

      <hr className="mx-4 md:mx-0 my-10" />

      <section id="form-section" className="w-full flex flex-col items-center">
        <div className="flex flex-col py-10 mb-10  items-center justify-start w-full md:w-10/12 lg:max-w-7xl">
          <div className="w-full grid grid-rows-2 md:grid-rows-none md:grid-cols-2 gap-6 px-4 md:px-0">
            <div className="bg-white p-5 shadow-lg rounded-lg">
              <Text kind="h5" className="flex-1 font-bold text-primary">
                {ID["contactUs.card.phoneEmail.title"]}
              </Text>

              <Text kind="body2" className="flex-1 font-light my-2">
                {ID["contactUs.card.phoneEmail.number"]}
              </Text>

              <Text kind="body2" className="flex-1 font-light my-2">
                {ID["contactUs.card.phoneEmail.emailCS"]}
              </Text>

              <Text kind="body2" className="flex-1 font-light my-2">
                {ID["contactUs.card.phoneEmail.emailAdmin"]}
              </Text>
            </div>

            <div className="bg-white p-5 shadow-lg rounded-lg">
              <Text kind="h5" className="flex-1 font-bold text-primary">
                {ID["contactUs.card.headOffice.title"]}
              </Text>

              <Text kind="body2" className="flex-1 font-light my-2">
                {ID["contactUs.card.headOffice.officeName"]}
              </Text>

              <Text kind="body2" className="flex-1 font-light my-2">
                {ID["contactUs.card.headOffice.officeAddress"]}
              </Text>

              <Text kind="body2" className="flex-1 font-light my-2">
                {ID["contactUs.card.headOffice.officePostalCode"]}
              </Text>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default ContactUs;
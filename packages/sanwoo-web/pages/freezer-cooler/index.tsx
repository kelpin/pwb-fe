import React, { useEffect } from "react";
import { SectionTwoColumns } from "../../components/layouts/SectionTwoColumns";
import { Text } from "../../components/foundations/Text";
import Layout from "../../components/layout";
import List from "@/components/List";
import CssUtils from "@/utils/Css";
import { ButtonLink } from "../../components/Button";
// import BuyModal from "@/components/BuyModal";
import dynamic from "next/dynamic";
import useSlideshow from "@/integrations/hooks/useSlideshow";
import { ISlideShow } from "@/integrations/types/ISlideShow";
import SectionSlideShow from "@/components/SectionSlideShow";
import SectionSlideShowVideo from "@/components/SectionSlideShowVideo";
import useAboutUs from "@/integrations/hooks/useAbout";
import { IAbout } from "@/integrations/types/IAbout";
import { IBanner } from "@/integrations/types/IBanner";
import useBanner from "@/integrations/hooks/useBanner";
import userProducts from "@/integrations/hooks/useProduct";
import { IProductGroup } from "@/integrations/types/IProduct";

const CarouselShowcase = dynamic(
  () => import("../../components/carousel/CarouselShowcase"),
  {
    ssr: false,
  }
);

const Carousel = dynamic(
  () => import("../../components/carousel/Carousel.js"),
  {
    ssr: false,
  }
);

let CarouselShowcaseImage: React.FC<{ src: string; alt: string }> = ({
  src,
  alt,
}) => {
  return (
    <div className="p-10 flex justify-center items-center h-screen-50">
      <img
        className="self-center"
        src={src}
        alt={alt}
        style={{ maxHeight: "100%" }}
      />
    </div>
  );
};

const SectionCoolerOneColumn = (props: {
  title: string;
  subtitle: string;
  image: string;
}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex flex-col w-full items-center justify-between py-16",
  };

  let [isMd, setIsMd] = React.useState(false);

  React.useEffect(() => {
    let isMd = window.matchMedia("(min-width: 768px)");
    setIsMd(isMd.matches);
  }, []);

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${props.image})`,
      }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none w-2/3 md:w-full text-center">
          {props.subtitle}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-primary text-center"
        >
          {props.title}
        </Text>
      </div>
    </section>
  );
};

const SectionFreezerOneColumn = (props: {
  title: string;
  subtitle: string;
  image: string;
}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex flex-col w-full items-center justify-between py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  };

  let [isMd, setIsMd] = React.useState(false);

  React.useEffect(() => {
    let isMd = window.matchMedia("(min-width: 768px)");
    setIsMd(isMd.matches);
  }, []);

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${props.image})`,
      }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none w-2/3 md:w-full text-center">
          {props.subtitle}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-primary text-center"
        >
          {props.title}
        </Text>
      </div>
    </section>
  );
};

const CoolerHome = () => {
  const [slideShow, setSlideShow] = React.useState<ISlideShow[]>([]);
  const [aboutUs, setAboutUs] = React.useState<IAbout[]>([]);
  const [banner, setBanner] = React.useState<IBanner[]>([]);
  const [products, setProducts] = React.useState<IProductGroup[]>([]);

  const { data: slideShowData } = useSlideshow();
  const { data: aboutUsData } = useAboutUs();
  const { data: bannerData } = useBanner();
  const { data: productsData } = userProducts();
  let carouselTemplate;

  useEffect(() => {
    if (slideShowData) {
      setSlideShow(slideShowData.data);
    }
  }, [
    slideShowData,

    carouselTemplate = slideShow.map(slide =>
      slide.kategori == "Cooler & Freezer" ?
        slide.embed == null ? <SectionSlideShow
        heading={slide.judul}
        desc={slide.keterangan}
        link={slide.link == null ? "":slide.link   }
        image={slide.gambar}
        />
        :<SectionSlideShowVideo videoUrl={slide.embed} />
        :""        
      )
  ]);

  useEffect(() => {
    if (aboutUsData) {
      setAboutUs(aboutUsData.data);
    }
  }, [aboutUsData]);

  useEffect(() => {
    if (bannerData) {
      setBanner(bannerData.data);
    }
  }, [bannerData]);

  useEffect(() => {
    if (productsData) {
      setProducts(productsData.data);
    }
  }, [productsData]);

  return (
    <Layout title="Sanwoo Cooler & Freezer">
      <Carousel arrowStyle="purple">

        {carouselTemplate}
        
      </Carousel>

      <SectionTwoColumns image={aboutUs[0]?.gambar ?? ""} imagePosition="left">
        <Text kind="h3" className="text-primary mb-2">
          {aboutUs[0]?.judul ?? ""}
        </Text>
        <Text kind="body" className="leading-relaxed">
          {aboutUs[0]?.deskripsi ?? ""}
        </Text>
      </SectionTwoColumns>

      <SectionCoolerOneColumn
        title={banner[0]?.judul ?? ""}
        subtitle={banner[0]?.keterangan ?? ""}
        image={banner[0]?.gambar ?? ""}
      />

      <CarouselShowcase to="/freezer-cooler/cooler">
        {
          // @ts-ignore
          products &&
            products[0] &&
            // @ts-ignore
            products[0]["Cooler"].map((p) => (
              <CarouselShowcaseImage
                key={p?.id}
                src={p?.gambar[0] ?? ""}
                alt="cooler-image"
              />
            ))
        }
        {
          // @ts-ignore
          products &&
            products[0] &&
            // @ts-ignore
            products[0]["Cooler"].map((p) => (
              <CarouselShowcaseImage
                key={p?.id}
                src={p?.gambar[0] ?? ""}
                alt="cooler-image"
              />
            ))
        }
      </CarouselShowcase>

      <SectionFreezerOneColumn
        title={banner[1]?.judul ?? ""}
        subtitle={banner[1]?.keterangan ?? ""}
        image={banner[1]?.gambar ?? ""}
      />

      <CarouselShowcase to="/freezer-cooler/freezer">
        {
          // @ts-ignore
          products &&
            products[0] &&
            // @ts-ignore
            products[0]["Freezer"].map((p) => (
              <CarouselShowcaseImage
                key={p?.id}
                src={p?.gambar[0] ?? ""}
                alt="image-SNW-200F"
              />
            ))
        }
        {
          // @ts-ignore
          products &&
            products[0] &&
            // @ts-ignore
            products[0]["Freezer"].map((p) => (
              <CarouselShowcaseImage
                key={p?.id}
                src={p?.gambar[0] ?? ""}
                alt="image-SNW-200F"
              />
            ))
        }
      </CarouselShowcase>

      <SectionTwoColumns image={aboutUs[1]?.gambar ?? ""} imagePosition="left">
        <Text kind="h3" className="text-primary">
          {aboutUs[1]?.judul ?? ""}
        </Text>
        <List>{aboutUs[1]?.deskripsi}</List>
      </SectionTwoColumns>

      <SectionTwoColumns image={aboutUs[2]?.gambar ?? ""} imagePosition="right">
        <Text kind="h3" className="text-primary">
          {aboutUs[2]?.judul ?? ""}
        </Text>
        <Text kind="body" className="leading-relaxed my-8">
          {aboutUs[2]?.deskripsi ?? ""}
        </Text>
        <ButtonLink
          to="/freezer-cooler/service"
          label="Read more"
          kind="primary"
        />
      </SectionTwoColumns>
    </Layout>
  );
};

export default CoolerHome;

import React, { useEffect } from "react";
import { Text } from "../components/foundations/Text";
import { ButtonLink } from "../components/Button";
import CssUtils from "../utils/Css";
import { ISplit } from "@/types/splitScreen";
import useSplitScreen from "@/integrations/hooks/useSplitScreen";

const tailwindStyles = {
  column:
    "w-full md:w-1/2 h-screen bg-center bg-contain bg-black bg-no-repeat flex flex-col items-center justify-center",
  logo: " w-2/3 md:w-1/2",
  textDescription:
    "text-white w-2/3 md:w-1/2 leading-relax md:leading-none text-center",
};

function Split() {
  const [pageData, setPageData] = React.useState<ISplit[]>([]);

  const { data } = useSplitScreen();

  useEffect(() => {
    if (data) {
      setPageData(data.data);
    }
  }, [data]);

  return (
    <div>
      <div className="w-full flex flex-col md:flex-row justify-between items-center">
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url("${pageData[0]?.gambar_bg ?? ""}")`,
            height: "100vh",
          }}
        >
          <img
            className={tailwindStyles.logo}
            src={pageData[0]?.gambar_logo ?? "-"}
            alt="logo-freezer-cooler-white"
            width={100}
            height={100}
          />
          <Text className={tailwindStyles.textDescription}>
            {pageData[0]?.gambar_keterangan ??
              "Wide range of coolers & freezers that suit your businesses"}
          </Text>
          <ButtonLink
            kind="secondary-white"
            label="Learn More"
            to="/freezer-cooler"
            className="mt-5 rounded-full hover:opacity-75"
          />
        </div>
        <div
          className={CssUtils.merge([tailwindStyles.column])}
          style={{
            backgroundImage: `url("${pageData[1]?.gambar_bg ?? ""}")`,
            height: "100vh",
          }}
        >
          <img
            className={tailwindStyles.logo ?? ""}
            src={pageData[1]?.gambar_logo ?? "-"}
            alt="logo-home"
            width={100}
            height={100}
          />
          <Text className={tailwindStyles.textDescription}>
            {pageData[1]?.gambar_keterangan ??
              "Ideas that bring comfort wherever you are. Get what your house needs"}
          </Text>

          <ButtonLink
            kind="secondary-white"
            label="Learn More"
            to="/home"
            className="mt-5 rounded-full hover:opacity-75"
          />
        </div>
      </div>
    </div>
  );
}

export default Split;

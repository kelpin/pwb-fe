import React from "react";
import { Text } from "./foundations/Text";
import { ButtonLink } from "./Button";
import Link from "next/link";

export interface ListItemProps {
  image: string;
  title: string;
  desc: string;
  to: string | number;
}

export const ListItem: React.FC<ListItemProps> = ({
  image,
  title,
  desc,
  to,
}): React.ReactElement => {
  return (
    <Link href={"/product/" + to}>
      <div className="flex flex-col justify-between p-2 rounded-lg shadow-md bg-white hover:shadow-lg">
        <div className="h-24 md:h-64 w-full flex flex-col items-center justify-center">
          <img src={image} alt={image} className="h-56" />
        </div>
        <Text
          kind="h5"
          className="text-center md:text-left font-bold py-3 px-2"
        >
          {title}
        </Text>
        <Text kind="body2" className="px-2 pb-4">
          {desc}
        </Text>
        <ButtonLink
          kind="primary"
          className="w-full rounded-md"
          customLabelStyle="text-subtitle2 md:text-button"
          label="View More"
          rounded={false}
          to={"/product/" + to}
        />
      </div>
    </Link>
  );
};

export default ListItem;

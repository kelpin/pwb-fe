import React from "react";
import Link from "next/link";
import { ButtonLink } from "./Button";
import IMAGES from "@/images";
import routes from "../routes";
import { getActiveRootPath } from "../utils/Location";
import BurgerMenu from "../assets/icons/burgerMenu.svg";
import CloseMenu from "../assets/icons/close.svg";
import { useRouter } from "next/router";

type LogoMenuProps = {
  to: string;
  image: string;
};

const LogoMenu: React.FC<LogoMenuProps> = ({ to, image }) => {
  return (
    <Link href={to}>
      <img src={image} alt="" className="mx-4 w-32" />
    </Link>
  );
};

let FreezerCoolerMenu = () => {
  let [isMd, setIsMd] = React.useState(false);

  React.useEffect(() => {
    let isMd = window.matchMedia("(min-width: 768px)");
    setIsMd(isMd.matches);
  }, []);

  return (
    <div className="flex px-2 md:px-0 flex-col md:grid md:grid-cols-3">
      <ButtonLink kind="text" to={routes.split} label="Product" />
      <ButtonLink
        kind="text"
        to={routes.freezerCooler.service.link}
        label="Service"
      />
      <ButtonLink
        kind={isMd ? "white-purple" : "text"}
        to={routes.contactUs.link}
        label={routes.contactUs.label}
      />
    </div>
  );
};

let HomeMenu = () => {
  let [isMd, setIsMd] = React.useState(false);

  React.useEffect(() => {
    let isMd = window.matchMedia("(min-width: 768px)");
    setIsMd(isMd.matches);
  }, []);

  return (
    <div className="flex flex-col md:grid md:grid-cols-3">
      <ButtonLink kind="text" to={routes.split} label="Product" />
      <ButtonLink
        kind="text"
        to={routes.home.eWarranty.link}
        label={routes.home.eWarranty.label}
      />
      <ButtonLink
        kind={isMd ? "white-purple" : "text"}
        to={routes.contactUs.link}
        label={routes.contactUs.label}
      />
    </div>
  );
};

export const Navigator = () => {
  // TODO: Use this for mobile view
  // let [isMenuMobileOpen, setIsMenuMobileOpen] = useState(false)
  //

  const router = useRouter();

  let isActiveFreezerCooler = getActiveRootPath(
    router.pathname,
    "freezer-cooler"
  );
  let isActiveHome = getActiveRootPath(router.pathname, "home");

  let logoFreezerCooler = isActiveFreezerCooler
    ? IMAGES.logoFreezerCoolerBlue
    : IMAGES.logoFreezerCoolerGray;
  let logoHome = isActiveHome ? IMAGES.logoHomeBlue : IMAGES.logoHomeGray;

  let menu = () =>
    getActiveRootPath(router.pathname, "home") ? (
      <HomeMenu />
    ) : (
      <FreezerCoolerMenu />
    );

  let [showMobileMenu, setShowMobileMenu] = React.useState(false);

  return (
    <nav className="shadow-lg w-full flex items-center justify-center">
      <div
        id="menu-desktop"
        className="hidden w-full md:w-10/12 lg:max-w-7xl md:flex items-center justify-between py-4"
      >
        <div
          id="placeholder for logo"
          className="flex justify-between items-center"
        >
          <LogoMenu to="/freezer-cooler" image={logoFreezerCooler} />

          <LogoMenu to="/home" image={logoHome} />
        </div>
        {menu()}
      </div>
      <div
        id="menu-mobile"
        className="w-full md:hidden flex items-center justify-between py-4"
      >
        {isActiveHome ? (
          <LogoMenu to="/home" image={logoHome} />
        ) : (
          <LogoMenu to="/freezer-cooler" image={logoFreezerCooler} />
        )}
        <div
          className="px-4 focus:outline-none"
          onClick={() => setShowMobileMenu(true)}
        >
          {/* TODO: Update SVG */}
          {/* <BurgerMenu /> */}
        </div>
      </div>
      {showMobileMenu && (
        <div className="h-screen md:hidden fixed bg-white z-50 top-0 bottom-0 w-full">
          <div
            id="menu-mobile-open"
            className="w-full md:hidden flex items-start justify-between py-4"
          >
            <div>
              <LogoMenu to="/home" image={logoHome} />
              <div className="h-4" />
              <LogoMenu to="/freezer-cooler" image={logoFreezerCooler} />
            </div>
            <div
              className="px-4 focus:outline-none"
              onClick={() => setShowMobileMenu(false)}
            >
              <CloseMenu />
            </div>
          </div>

          <hr className="md:hidden mx-4 md:mx-0 my-4" />

          <div>{menu()}</div>
        </div>
      )}
    </nav>
  );
};

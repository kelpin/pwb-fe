import React from "react";
import { Text } from "../components/foundations/Text";
import axios, { AxiosRequestConfig } from "axios";
import { IEcommerce } from "@/integrations/types/IEcommerce";
import useEcommerce from "@/integrations/hooks/useEcommerce";


const BuyModal = ({
  onClose = () => {},
}) => {   
  return (
    <div
      className="fixed w-full h-screen z-50 flex justify-center items-center"
      style={{ background: "rgba(0,0,0,0.5)" }}
    >
      <div
        className="relative mx-4 px-6 py-6 md:py-0 md:px-0 h-screen-80 md:h-screen-50 md:w-2/3  rounded-xl flex flex-col justify-center items-center"
        style={{
          background:
            "linear-gradient(180deg, #FEFEFF 20%, rgb(245, 245, 255) 55%,  #E9E9FF 100%)",
        }}
      >
        <div
          className="absolute my-10 mr-10 top-0 right-0 cursor-pointer"
          onClick={onClose}
        >
          x
        </div>
        <Text kind="h3" className="text-h5 md:text-h3 text-primary mb-10">
          Buy Now
        </Text>
        <div className="w-full md:w-2/3 grid grid-rows-4 md:grid-rows-none md:grid-cols-2 gap-10  overflow-y-auto">

        </div>
      </div>
    </div>
  );
};

export default BuyModal;

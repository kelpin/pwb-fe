import React from "react";
import { Navigator } from "./menus";
import Footer from "../components/Footer";
import WhatsAppButton from "../components/WhatsappButton";

const Layout = ({ title, children }) => {
  return (
    <div
      className="global-wrapper"
      style={{
        background:
          "linear-gradient(180deg, #FEFEFF 50.44%, rgba(245, 245, 255, 0.4) 70.61%, rgba(245, 245, 255, 0.65) 73.76%, #F5F5FF 100%)",
      }}
    >
      <Navigator />
      <main>{children}</main>

      <WhatsAppButton />
      <Footer />
    </div>
  );
};

export default Layout;

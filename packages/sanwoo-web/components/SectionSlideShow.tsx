import React from "react";
import { ButtonLink } from "./Button";
import { Text } from "./foundations/Text";
import CssUtils from "utils/Css";

type SectionCarouselTypes = {
  heading: string;
  desc: string;
  link: string;
  image: string;
};

const SectionSlideShow: React.FC<SectionCarouselTypes> = ({
  heading,
  desc,
  link,
  image,
}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex w-full items-center justify-center md:py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    spacer: "hidden md:block w-1/12",
  };

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${image})`,
      }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="w-screen md:flex-1 flex flex-col items-start md:px-0">
        <div className="mb-10">
          <Text
            kind="h3"
            className="text-h4 md:text-h3 leading-4 md:leading-none font-bold leading-none text-primary mb-4 px-3 md:px-0"
          >
            {heading}
          </Text>
          <Text
            kind="h5"
            className="text-black text-body2 md:text-h5 md:leading-relaxed max-w-sm md:max-w-2xl lg:max-w-4xl w-screen md:w-full font-light px-3 md:px-0"
          >
            {desc}
          </Text>
        </div>
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  );
};

export default SectionSlideShow;

import React from "react";
import ROUTES from "@/routes";
import Link from "next/link";

type BreadcrumbTypes = {
  url: string;
  title: string;
};

const getHomeAndCategoryFromUrl = (url: string) => {
  let urlSplit = url.split("/");

  let home = urlSplit[1];
  let productCategory = urlSplit[2];

  return { home, productCategory };
};

const Breadcrumb: React.FC<BreadcrumbTypes> = ({ url = "", title = "" }) => {
  let { home, productCategory } = getHomeAndCategoryFromUrl(url);

  let createBaseLink = (home: string) => {
    switch (home) {
      case "freezer-cooler":
        return ROUTES.freezerCooler.index.link;

      case "home":
      default:
        return ROUTES.home.index.link;
    }
  };
  let createBaseLabel = (home: string) => {
    switch (home) {
      case "freezer-cooler":
        return "Home";
      case "home":
      default:
        return "Home";
    }
  };

  let createCategoryLink = (productCategory: string) => {
    switch (productCategory) {
      case "cooler":
        return ROUTES.freezerCooler.cooler.link;
      case "freezer":
        return ROUTES.freezerCooler.freezer.link;
      case "airfyer":
        return ROUTES.home.airFryer.link;
      case "airpurifier":
        return ROUTES.home.airPurifier.link;

      case "ricecooker":
        return ROUTES.home.ricecooker.link;

      case "multicooker":
        return ROUTES.home.multicooker.link;

      default:
        return ROUTES.home.airFryer52.link;
    }
  };

  let createCategoryLabel = (productCategory: string) => {
    switch (productCategory) {
      case "cooler":
        return "Sanwoo Cooler";

      case "freezer":
        return "Sanwoo Freezer";

      case "airfryer":
        return "Sanwoo Air Fryer";

      case "airpurifier":
        return "Sanwo Air Purifier";

      case "ricecooker":
        return "Sanwoo Rice Cooker";

      case "multicooker":
        return "Sanwoo Multi Cooker";

      default:
        return "Sanwoo Air Fryer 52";
    }
  };

  return (
    <div className="flex items-center">
      <a className="text-gray-400 truncate hover:text-primary">
        <Link href={createBaseLink(home)}>{createBaseLabel(home)}</Link>
      </a>
      <p className="mx-2 text-gray-400">/</p>
      <a className="text-gray-400 truncate hover:text-primary">
        <Link href={createCategoryLink(productCategory)}>
          {createCategoryLabel(productCategory)}
        </Link>
      </a>
      <p className="mx-2 text-gray-400">/</p>
      <p className="text-primary truncate font-bold">{title}</p>
    </div>
  );
};

export default Breadcrumb;

const ButtonImage = ({
  image,
  link,
  disabled,
}: {
  image: string;
  link: string;
  disabled: boolean;
}) => {
  let calculatedLink = disabled ? "" : link;
  let calculatedStyles = disabled ? "opacity-75 cursor-not-allowed" : "";
  let calculatedTarget = disabled ? "" : "_blank";

    return (
      <a
        href={calculatedLink}
        target={calculatedTarget}
        className={"flex items-center justify-center" + " " + calculatedStyles}
      >
        <div className="rounded h-full rounded-full w-56 shadow px-16 py-3 text-center bg-white">
          <img src={image} className=" h-full w-full object-contain " />
        </div>
      </a>
    );

};

export default ButtonImage;

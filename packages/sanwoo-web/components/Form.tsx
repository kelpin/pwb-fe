import React from "react";
import CssUtils from "@/utils/Css";

type TextFieldProps = {
  id: string;
  label: string;
  placeholder: string;
  type: string;
  onChange: (e: string) => void;
};

type PhoneFieldProps = {
  id: string;
  label: string;
  placeholder: string;
  onChange: (e: string) => void;
};

export type SelectOption = {
  value: string;
  label: string;
};
type SelectProps = {
  id: string;
  label: string;
  placeholder: string;
  options: Array<{ value: string; label: string }>;
  onChange: (e: string) => void;
};

export const TextField: React.FC<TextFieldProps> = ({
  id,
  label,
  placeholder,
  type,
  onChange,
}) => {
  const styles = {
    label: "block text-gray-700 text-sm font-bold mb-2",
    input:
      "appearance-none border rounded w-full py-5 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline",
  };
  return (
    <div>
      <label className={styles.label}>{label}</label>
      <input
        className={styles.input}
        id={id}
        type={type}
        placeholder={placeholder}
        onChange={(e) => onChange(e.target.value)}
      />
    </div>
  );
};

export const TextArea: React.FC<TextFieldProps> = ({
  id,
  label,
  placeholder,
  type,
  onChange,
}) => {
  const styles = {
    label: "block text-gray-700 text-sm font-bold mb-2",
    input:
      "appearance-none border rounded w-full py-5 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline",
  };
  return (
    <div>
      <label className={styles.label}>{label}</label>
      <textarea
        className={styles.input}
        id={id}
        placeholder={placeholder}
        rows={4}
        onChange={(e) => onChange(e.target.value)}
      />
    </div>
  );
};

export const PhoneField: React.FC<PhoneFieldProps> = ({
  id,
  label,
  placeholder,
  onChange,
}) => {
  const styles = {
    label: "block text-gray-700 text-sm font-bold mb-2",
    input:
      "appearance-none border rounded w-full py-5 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline",
  };
  return (
    <div>
      <label className={styles.label}>{label}</label>
      <input
        className={CssUtils.merge(["form-number", styles.input])}
        id={id}
        type="number"
        placeholder={placeholder}
        onChange={(e) => onChange(e.target.value)}
      />
    </div>
  );
};

export const Select: React.FC<SelectProps> = ({
  id,
  label,
  placeholder,
  options,
  onChange,
}) => {
  const styles = {
    label: "block text-gray-700 text-sm font-bold mb-2",
    input:
      "appearance-none border rounded w-full py-5 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline",
  };
  return (
    <div>
      <label className={styles.label}>{label}</label>

      <div className="relative">
        <select
          className={CssUtils.merge(["block form-number", styles.input])}
          id={id}
          placeholder={placeholder}
          onChange={(e) => onChange(e.target.value)}
        >
          <option disabled selected>
            Select Topic *
          </option>
          {options.map((o) => (
            <option value={o.value}>{o.label}</option>
          ))}
        </select>
        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
          <svg
            className="fill-current h-4 w-4"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
          >
            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
          </svg>
        </div>
      </div>
    </div>
  );
};

import React from "react";
// import ListCheckIcon from "@/assets/icons/circleCheckPurple.svg";
import { Text } from "./foundations/Text";

interface ListProps {
  children: string;
}

const List: React.FC<ListProps> = ({ children }) => {
  return (
    <li className="flex justify-start items-start mb-4">
      {/* <ListCheckIcon className="mr-4 h-6 w-6" /> */}

      <Text kind="body" className="flex-1">
        {children}
      </Text>
    </li>
  );
};

export default List;

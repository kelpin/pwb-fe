import React from "react";
import { Text } from "./foundations/Text";
import { getActiveRootPath } from "../utils/Location";
import routes from "../routes";
import Link from "next/link";
import { useRouter } from "next/router";
import Image from "next/image";

type TextLinkProps = {
  label: string;
  to: string;
};
const TextLink: React.FC<TextLinkProps> = ({ label, to }) => {
  return (
    <Text kind="subtitle" className="hover:opacity-50 my-1">
      <Link href={to}>{label}</Link>
    </Text>
  );
};

const IconButton = ({ link, image }: { link: string; image: string }) => {
  return (
    <a href={link} target="_blank">
      <Image height={20} width={20} className="hover:opacity-50" src={image} />
    </a>
  );
};

const FREEZER_COOLER = {
  PRODUCT: [
    {
      label: routes.freezerCooler.cooler.label,
      to: routes.freezerCooler.cooler.link,
    },
    {
      label: routes.freezerCooler.freezer.label,
      to: routes.freezerCooler.freezer.link,
    },
    {
      label: routes.freezerCooler.freezer.children.hybrid.label,
      to: routes.freezerCooler.freezer.children.hybrid.link,
    },
    {
      label: routes.freezerCooler.freezer.children.twoInOne.label,
      to: routes.freezerCooler.freezer.children.twoInOne.link,
    },
  ],
  SERVICES: [
    {
      label: routes.freezerCooler.service.label,
      to: routes.freezerCooler.service.link,
    },
  ],
};

const HOME = {
  PRODUCT: [
    {
      label: routes.home.airFryer.label,
      to: routes.home.airFryer.link,
    },
    {
      label: routes.home.airPurifier.label,
      to: routes.home.airPurifier.link,
    },
    {
      label: "Air Fryer 5.2 L",
      to: routes.home.airFryer52.link,
    },
    {
      label: "Multi Cooker",
      to: routes.home.multicooker.link,
    },
    {
      label: "Rice Cooker",
      to: routes.home.ricecooker.link,
    },
  ],
  SERVICES: [
    {
      label: routes.home.eWarranty.label,
      to: routes.home.eWarranty.link,
    },
  ],
};
const renderLinks = (links: Array<TextLinkProps>) => {
  return links.map(({ label, to }) => <TextLink label={label} to={to} />);
};

const styles = {
  colWrapper: "px-5 md:px-0 flex flex-col items-stretch",
  colContentContainer: "mb-8 md:mb-0 flex flex-col items-start w-2/3",
};
const FooterTop = () => {
  const { pathname } = useRouter();
  let isActiveHome = getActiveRootPath(pathname, "home");
  let productLinks = isActiveHome ? HOME.PRODUCT : FREEZER_COOLER.PRODUCT;
  let serviceLinks = isActiveHome ? HOME.SERVICES : FREEZER_COOLER.SERVICES;

  return (
    <div className="flex flex-col md:grid md:grid-cols-3 md:pb-24 w-full md:w-10/12 lg:max-w-7xl">
      <div className={styles.colWrapper}>
        <div className={styles.colContentContainer}>
          <Text kind="h6" className="font-semibold">
            Product
          </Text>
          {renderLinks(productLinks)}
        </div>
      </div>
      <div className={styles.colWrapper}>
        <div className={styles.colContentContainer}>
          <Text kind="h6" className="font-semibold">
            Service
          </Text>
          {renderLinks(serviceLinks)}
        </div>
      </div>

      <div className={styles.colWrapper}>
        <div className={styles.colContentContainer}>
          <Text kind="h6" className="font-semibold">
            Contact Us
          </Text>
          <Text kind="subtitle" className="my-1">
            cs@sanwooelectronics.com
          </Text>
          <Text kind="subtitle" className="my-1">
            Call Center : +62 811 1353 510
          </Text>
          <Text kind="subtitle" className="my-1">
            Jln. Jembatan 3 Blok F No. 8 Jakarta Utara - 14440
          </Text>
        </div>
      </div>
    </div>
  );
};

const FooterBottom = () => {
  return (
    <div className="flex flex-col md:flex-row py-5 border-t items-start md:items-center md:justify-between w-full md:w-10/12 lg:max-w-7xl pl-6 md:pl-0">
      <div className="flex flex-col md:flex-row items-start md:items-center mb-4 md:mb-0">
        <Text kind="subtitle2" className="mr-3 mb-2 md:mb-0">
          Get in touch with us on social media
        </Text>
        <div className="grid grid-cols-3 gap-3">
          <IconButton
            key="instagram-1"
            link="https://instagram.com/sanwoo.id?igshid=17b4r94la5qnn "
            image="/static/assets/logo-instagram.png"
          />
          <IconButton
            key="facebook"
            link="https://www.facebook.com/sanwoo.airfryer "
            image="/static/assets/logo-facebook.png"
          />
          <IconButton
            key="instagram-2"
            link="https://instagram.com/sanwoo.home?igshid=8lvdohgr221a"
            image="/static/assets/logo-instagram.png"
          />
        </div>
      </div>
      <Text kind="subtitle2"> Copyright © 2020 Sanwoo Electronics</Text>
    </div>
  );
};

const Footer: React.FC = () => {
  return (
    <footer className="w-full flex flex-col items-center">
      <FooterTop />
      <FooterBottom />
    </footer>
  );
};

export default Footer;

import React from "react";
import CssUtils from "../../utils/Css";

export interface TextProps {
  children: React.ReactText | string | React.ReactElement | Element;
  /**
   * [FONTS VARIANT]: Poppins - [Regular | Medium | Semibold]
   * h1         : Poppins - Medium - 93
   * h2         : Poppins - Medium - 58
   * h3         : Poppins - Regular - 46
   * h4         : Poppins - Regular - 33
   * h5         : Poppins - Semibold - 23
   * h6         : Poppins - Medium - 19
   * subtitle   : Poppins - Regular - 15
   * subtitle2  : Poppins - Regular - 13
   * body       : Poppins - Regular - 18
   * body2      : Poppins - Medium - 16
   * button     : Poppins - Medium - 16
   * button2    : Poppins - Medium - 13
   *  caption   : Poppins - Medium - 16
   * */
  kind?:
    | "h1"
    | "h2"
    | "h3"
    | "h4"
    | "h5"
    | "h6"
    | "body"
    | "body2"
    | "subtitle"
    | "subtitle2"
    | "button"
    | "button2"
    | "caption";
  /**
   * Additional Tailwind or CSS class
   * */
  className?: string;
  styles?: any;
}

const tailwindStyles = {
  h1: "font-poppins text-h2 md:text-h1 leading-normal",
  h2: "font-poppins text-h3 md:text-h2 leading-normal",
  h3: "font-poppins text-h4 md:text-h3 leading-normal",
  h4: "font-poppins text-h5 md:text-h4 leading-normal",
  h5: "font-poppins text-h6 md:text-h5 leading-normal",
  h6: "font-poppins text-body md:text-h6 leading-normal",
  body: "font-poppins text-body2 md:text-body leading-normal",
  body2: "font-poppins text-subtitle md:text-body2 leading-normal",
  subtitle: "font-poppins text-subtitle2 md:text-subtitle leading-normal",
  subtitle2: "font-poppins text-subtitle3 md:text-subtitle2 leading-normal",
  button: "font-poppins text-subtitle md:text-button leading-normal",
  button2: "font-poppins text-subtitle3 md:text-button2 leading-normal",
  caption: "font-poppins text-caption leading-normal",
};

export const Text: React.FC<TextProps> = ({
  children,
  kind,
  className = "",
  styles,
}) => {
  switch (kind) {
    case "h1":
      return (
        <h1 className={CssUtils.merge([tailwindStyles.h1, className])}>
          {children}
        </h1>
      );

    case "h2":
      return (
        <h2 className={CssUtils.merge([tailwindStyles.h2, className])}>
          {children}
        </h2>
      );

    case "h3":
      return (
        <h3 className={CssUtils.merge([tailwindStyles.h3, className])}>
          {children}
        </h3>
      );

    case "h4":
      return (
        <h4 className={CssUtils.merge([tailwindStyles.h4, className])}>
          {children}
        </h4>
      );

    case "h5":
      return (
        <h5 className={CssUtils.merge([tailwindStyles.h5, className])}>
          {children}
        </h5>
      );

    case "h6":
      return (
        <h6 className={CssUtils.merge([tailwindStyles.h6, className])}>
          {children}
        </h6>
      );

    case "subtitle":
      return (
        <p
          className={CssUtils.merge([tailwindStyles.subtitle, className])}
          style={styles}
        >
          {children}
        </p>
      );

    case "subtitle2":
      return (
        <p className={CssUtils.merge([tailwindStyles.subtitle2, className])}>
          {children}
        </p>
      );

    case "button":
      return (
        <span className={CssUtils.merge([tailwindStyles.button, className])}>
          {children}
        </span>
      );

    case "button2":
      return (
        <span className={CssUtils.merge([tailwindStyles.button2, className])}>
          {children}
        </span>
      );

    case "caption":
      return (
        <p className={CssUtils.merge([tailwindStyles.caption, className])}>
          {children}
        </p>
      );

    default:
    case "body2":
      return (
        <p className={CssUtils.merge([tailwindStyles.body2, className])}>
          {children}
        </p>
      );

    case "body":
      return (
        <p className={CssUtils.merge([tailwindStyles.body, className])}>
          {children}
        </p>
      );
  }
};

export default Text;

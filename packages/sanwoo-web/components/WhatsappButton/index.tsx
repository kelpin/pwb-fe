import React from "react";

const WhatsAppButton = () => {
  return (
    <>
      <a
        href="https://api.whatsapp.com/send?phone=6281389200035"
        className="float"
        target="_blank"
      >
        <img src={"/static/assets/whatsapp.png"} />{" "}
      </a>
    </>
  );
};

export default WhatsAppButton;

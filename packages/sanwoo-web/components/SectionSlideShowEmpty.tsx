import React from "react";
import CssUtils from "@/utils/Css";

type SectionCarouselTypes = {
  image: string;
};

const SectionSlideShowEmpty: React.FC<SectionCarouselTypes> = ({ image }) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex w-full items-center justify-center md:py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    spacer: "hidden md:block w-1/12",
  };

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${image})`,
      }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="w-screen md:flex-1 flex flex-col items-start md:px-0"></div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  );
};

export default SectionSlideShowEmpty;

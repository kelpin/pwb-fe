import React, { useState } from "react";
// @ts-ignore
import Slider from "react-slick";
import Image from "next/image";

const tailwindStyles = {
  container: "md:h-screen-80 w-full ",
  imageContainer: "flex-col justify-center items-center",
  textWrapper: "flex-row items-center",
  textContainer: "flex flex-col justify-start items-start w-2/3",
  image: "",
  title: "",
  subtitle: "",
};

const sliderSettings = {
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoPlay: true,
  autoplaySpeed: 200,
  dots: true,
};

const Carousel = ({ containerStyle, children, arrowStyle }) => {
  const [slider, setSlider] = useState(null);
  // @ts-ignore
  const onNext = () => slider.slickNext();
  // @ts-ignore
  const onPrevious = () => slider.slickPrev();

  return (
    <section
      className={
        (containerStyle ? containerStyle : "h-screen ") +
        " " +
        tailwindStyles.container
      }
    >
      <div
        aria-label="previous-button"
        className="hidden md:absolute top-0 bottom-0 w-1/12 md:flex items-center justify-center z-50"
      >
        <button onClick={() => onPrevious()}>
          {arrowStyle !== "white" ? (
            <Image
              src="/static/assets/icons/carouselButtonLeft.svg"
              width={40}
              height={40}
            />
          ) : (
            <Image
              src="/static/assets/icons/carouselButtonLeftWhite.svg"
              width={40}
              height={40}
            />
          )}
        </button>
      </div>
      <Slider
        ref={
          // @ts-ignore
          (slider) => setSlider(slider)
        }
        {...sliderSettings}
        arrows={false}
      >
        {children}
      </Slider>
      <div
        aria-label="next-button"
        className="hidden md:absolute right-0 top-0 bottom-0 w-1/12 md:flex items-center justify-center transform rotate-180 z-50"
      >
        <button onClick={() => onNext()}>
          {arrowStyle !== "white" ? (
            <Image
              src="/static/assets/icons/carouselButtonLeft.svg"
              width={40}
              height={40}
            />
          ) : (
            <Image
              src="/static/assets/icons/carouselButtonLeftWhite.svg"
              width={40}
              height={40}
            />
          )}
        </button>
      </div>
    </section>
  );
};

export default Carousel;

import React, { useState } from "react";
import CarouselButtonLeft from "../../assets/icons/carouselButtonLeft.svg";
import CarouselButtonLeftWhite from "../../assets/icons/carouselButtonLeftWhite.svg";
// @ts-ignore
import Slider from "react-slick";

export interface Carousel {
  containerStyle?: string;
  children: React.ReactElement[];
  arrowStyle: "white" | "purple";
}

const tailwindStyles = {
  container: "md:h-screen-80 w-full ",
  imageContainer: "flex-col justify-center items-center",
  textWrapper: "flex-row items-center",
  textContainer: "flex flex-col justify-start items-start w-2/3",
  image: "",
  title: "",
  subtitle: "",
};

const sliderSettings = {
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoPlay: true,
  autoplaySpeed: 200,
  dots: true,
};

const Carousel: React.FC<Carousel> = ({
  containerStyle,
  children,
  arrowStyle,
}) => {
  const [slider, setSlider] = useState(null);
  // @ts-ignore
  const onNext = () => slider.slickNext();
  // @ts-ignore
  const onPrevious = () => slider.slickPrev();

  return (
    <section
      className={
        (containerStyle ? containerStyle : "h-screen ") +
        " " +
        tailwindStyles.container
      }
    >
      <Slider
        ref={
          // @ts-ignore
          (slider) => setSlider(slider)
        }
        {...sliderSettings}
        arrows={false}
      >
        {children}
      </Slider>
    </section>
  );
};

export default Carousel;

import React, { useState } from "react";
// @ts-ignore
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { ButtonLink } from "../Button";

const tailwindStyles = {
  container: "w-full py-10",
  imageContainer: "flex-col justify-center items-center",
  textWrapper: "flex-row items-center",
  textContainer: "flex flex-col justify-start items-start w-2/3",
  image: "",
  title: "",
  subtitle: "",
};

const sliderSettings = {
  infinite: true,
  speed: 500,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
};

export const CarouselShowcase = ({ children, to }) => {
  let [isMd, setIsMd] = React.useState(false);

  React.useEffect(() => {
    let isMd = window.matchMedia("(min-width: 768px)");
    setIsMd(isMd.matches);
  }, []);

  let slidesToShow = isMd ? 5 : 1;

  return (
    <section className={tailwindStyles.container}>
      <Slider {...sliderSettings} slidesToShow={slidesToShow} arrows={false}>
        {children}
      </Slider>

      <div className="w-full flex justify-center pt-10">
        <ButtonLink kind="primary" to={to} label="View All Product" />
      </div>
    </section>
  );
};

export default CarouselShowcase;

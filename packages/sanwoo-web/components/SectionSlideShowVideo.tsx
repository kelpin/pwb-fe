import React from "react";
import CssUtils from "utils/Css";

interface ISectionSlideShowVideProps {
  videoUrl: string;
}

const SectionSlideShowVideo = (props: ISectionSlideShowVideProps) => {
  const tailwindStyles = {
    container:
      "bg-center h-screen-80 flex flex-col w-full items-center justify-center",
  };

  return (
    <section className={CssUtils.merge([tailwindStyles.container, ""])}>
      <iframe
        style={{ width: "100vw", height: "100vh" }}
        width="560"
        height="315"
        src={props.videoUrl}
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      ></iframe>
    </section>
  );
};

export default SectionSlideShowVideo;

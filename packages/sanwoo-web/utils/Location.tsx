export const getActiveRootPath = (
  path: string,
  kind: "freezer-cooler" | "home"
): boolean => {
  let freezerCoolerReg = /freezer-cooler/g
  let homeReg = /home/g

  switch (kind) {
    case "freezer-cooler":
      return freezerCoolerReg.test(path)

    case "home":
      return homeReg.test(path)
  }
}

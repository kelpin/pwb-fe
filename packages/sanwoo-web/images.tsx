export default {
  logoFreezerCoolerBlue: "/static/assets/sanwoo-freezer-cooler-blue.png",
  logoFreezerCoolerGray: "/static/assets/sanwoo-freezer-cooler-gray.png",
  logoHomeBlue: "/static/assets/sanwoo-home-blue.png",
  logoHomeGray: "/static/assets/sanwoo-home-gray.png",

  freezerCoolerJumbotronCooler:
    "/static/assets/freezer-cooler-jumbotron-cooler.jpg",
  freezerCoolerJumbotronCoolerSection:
    "/static/assets/freezer-cooler-jumbotron-cooler-section.jpg",
  freezerCoolerJumbotronCoolerMobile: "/static/assets/coolerMobile.jpg",
  freezerCoolerJumbotronCoolerLeft:
    "/static/assets/freezer-cooler-jumbotron-cooler-left.jpg",
  freezerCoolerJumbotronFreezer:
    "/static/assets/freezer-cooler-jumbotron-freezer.jpg",
  freezerCoolerJumbotronFreezerMobile: "/static/assets/freezerMobile.jpg",
  freezerCoolerJumbotronFeelMaximum:
    "/static/assets/freezer-cooler-jumbotron-feel-maximum.jpg",
  freezerCoolerJumbotronFreezerLeft:
    "/static/assets/freezer-cooler-jumbotron-freezer-left.jpg",
  freezerCoolerAboutUs: "/static/assets/freezer-cooler/img-about-us.jpg",
  freezerCoolerOneStopSolutiokn:
    "/static/assets/freezer-cooler/img-one-stop-solution.jpg",
  freezerCoolerOurAdvantage:
    "/static/assets/freezer-cooler/img-our-advantage.jpg",

  freezerCooler_coolerLanding1:
    "/static/assets/sanwoo-coolers-landing/cooler-landing-1.png",
  freezerCooler_coolerLanding2:
    "/static/assets/sanwoo-coolers-landing/cooler-landing-2.png",
  freezerCooler_coolerLanding3:
    "/static/assets/sanwoo-coolers-landing/cooler-landing-3.png",
  freezerCooler_coolerLanding4:
    "/static/assets/sanwoo-coolers-landing/cooler-landing-4.png",
  freezerCooler_coolerLanding5:
    "/static/assets/sanwoo-coolers-landing/cooler-landing-5.png",
  freezerCooler_coolerLanding6:
    "/static/assets/sanwoo-coolers-landing/cooler-landing-6.png",
  freezerCooler_coolerLanding7:
    "/static/assets/sanwoo-coolers-landing/cooler-landing-7.png",
  freezerCooler_coolerLanding8:
    "/static/assets/sanwoo-coolers-landing/cooler-landing-8.png",

  freezerCooler_freezerLanding1:
    "/static/assets/sanwoo-freezer-landing/freezer-landing-1.png",
  freezerCooler_freezerLanding2:
    "/static/assets/sanwoo-freezer-landing/freezer-landing-2.png",
  freezerCooler_freezerLanding3:
    "/static/assets/sanwoo-freezer-landing/freezer-landing-3.png",
  freezerCooler_freezerLanding4:
    "/static/assets/sanwoo-freezer-landing/freezer-landing-4.png",
  freezerCooler_freezerLanding5:
    "/static/assets/sanwoo-freezer-landing/freezer-landing-5.png",

  coolerBigger: "/static/assets/sanwoo-coolers-landing/cooler-bigger.jpg",
  coolerEco: "/static/assets/sanwoo-coolers-landing/cooler-eco.jpg",

  freezerBest:
    "/static/assets/sanwoo-freezer-landing/sanwoo-freezer-landing-best.jpg",
  freezerEasy:
    "/static/assets/sanwoo-freezer-landing/sanwoo-freezer-landing-easy.jpg",

  serviceJumbotron: "/static/assets/service/img-jumbotron.jpg",
  servicePlacement: "/static/assets/service/img-placement.jpg",
  serviceRebranding: "/static/assets/service/img-rebranding.png",
  serviceWorkshop: "/static/assets/service/img-workshop-around.jpg",

  homeJumbotronSimplicity:
    "/static/assets/home-jumbotron-simplicity-matters.jpg",
  homeAirFyer: "/static/assets/home-air-fryer.jpg",
  homeAirPurifier: "/static/assets/home-air-purifier.jpg",
  homeAirFryer55: "/static/assets/home-airfryer-55.jpg",
  homeMultiCooker: "/static/assets/home-multicooker.jpg",
  homeRiceCooker: "/static/assets/home-ricecooker.png",
  homeEWarranty: "/static/assets/home-ewarranty.jpg",
  homePromo: "/static/assets/home-promo.jpg",
  homePromoBig: "/static/assets/home-promo-new.jpg",
  homePromoSomething: "/static/assets/home-banner-something-big.jpg",
  homeBannerNew: "/static/assets/home-banner-new.jpg",

  airFryerIntro: "/static/assets/airfyer-intro.jpg",
  airFryerSub_advance: "/static/assets/airfyer-sub-advance.jpg",
  airFryerSub_advance_item: "/static/assets/airfryer-sub-advance-items.png",
  airFryerSub_limitless: "/static/assets/airfyer-sub-limitless.jpg",
  airFryerWithoutFat: "/static/assets/airfyer-without-fat.jpg",
  airFryerShowcase__Front__Open: "/static/assets/airfryer-front-open.jpg",
  airFryerShowcase__Front__Closed: "/static/assets/airfryer-front-closed.jpg",
  airFryerShowcase__Side__Open: "/static/assets/airfryer-side-open.jpg",
  itemHomeFryer__FRONT: "/static/assets/contents/airfryer-FRONT__CLOSED.jpg",
  itemHomeFryer__FRONT__OPEN: "/static/assets/contents/airfryer-SIDE__OPEN.jpg",
  itemHomeFryer__SIDE__OPEN: "/static/assets/contents/airfryer-FRONT__OPEN.jpg",
  airPurifierIntro: "/static/assets/airpurifier-intro.jpg",
  airPurifierSub_3in1: "/static/assets/airpurifier-3in1.jpg",
  airPurifierSub_removesOdor: "/static/assets/airpurifier-removes-odor.jpg",
  airPurifierBreatheEasy: "/static/assets/airpurifier-breathe-easy.jpg",
  airPurifierShowcase__Front: "/static/assets/airpurifier-front.jpg",
  airPurifierShowcase__Side: "/static/assets/airpurifier-side.jpg",
  airPurifierShowcase__Back: "/static/assets/airpurifier-back.jpg",

  airFryer52Intro: "/static/assets/airfyer-52-intro.jpg",
  airFryer52ComingSoon: "/static/assets/airfryer-52-coming-soon.jpg",
  airFryer52Bigger: "/static/assets/airfryer-52-bigger-better-faster.jpg",
  airFryer52Something:
    "/static/assets/airfryer-52-something-bigger-is-coming.jpg",

  multiCookerIntro: "/static/assets/multi-cooker-intro.jpg",
  multiCookerLeft: "/static/assets/multi-cooker-left.jpg",
  multiCookerRight: "/static/assets/multi-cooker-right.jpg",
  multiCooker14in1: "/static/assets/multi-cooker-14-in-1.jpg",

  riceCookerIntro: "/static/assets/rice-cooker-intro.jpg",
  riceCookerLeft: "/static/assets/rice-cooker-left.jpg",
  riceCookerRight: "/static/assets/rice-cooker-right.jpg",
  riceCookerLowSugar: "/static/assets/rice-cooker-low-sugar.jpg",

  itemHomePurifier__FRONT: "/static/assets/sanwoo-home/AIRPURIFIER_FRONT.png",
  itemHomePurifier__SIDE: "/static/assets/sanwoo-home/AIRPURIFIER_SIDE.png",
  itemHomePurifier__BACK: "/static/assets/sanwoo-home/AIRPURIFIER_BACK.png",

  riceCooker__FRONT: "/static/assets/rice-cooker-front.png",
  bgRiceCooker__FRONT: "/static/assets/rice-cooker-front-bg.jpg",

  riceCooker__TOP: "/static/assets/rice-cooker-top.png",
  bgRiceCooker__TOP: "/static/assets/rice-cooker-top-bg.jpg",

  buy: {
    shopee: "/static/assets/buy-shopee.png",
    tokopedia: "/static/assets/buy-tokopedia.png",
    jdId: "/static/assets/buy-jd-id.png",
    blibli: "/static/assets/buy-blibli.png",
  },

  "SNW 46": [
    "/static/assets/contents/cooler-SNW-46-SIDE__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-46-FRONT__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-46-FRONT__CLOSED.jpg",
  ],

  "SNW 60": [
    "/static/assets/contents/cooler-SNW-60-SIDE__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-60-FRONT__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-60-FRONT__CLOSED.jpg",
  ],

  "SNW 106": [
    "/static/assets/contents/cooler-SNW-106-FRONT__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-106-FRONT__CLOSED.jpg",
    "/static/assets/contents/cooler-SNW-106-SIDE__CLOSED.jpg",
  ],

  "SNW 160 Slim": [
    "/static/assets/contents/cooler-SNW-160-SIDE__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-160-FRONT__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-160-FRONT__CLOSED.jpg",
  ],

  "SNW 246": [
    "/static/assets/contents/cooler-SNW-246-SIDE__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-246-FRONT__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-246-FRONT__CLOSED.jpg",
  ],

  "SNW 270SCD": [
    "/static/assets/contents/cooler-SNW-270SCD-SIDE__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-270SCD-FRONT__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-270SCD-FRONT__CLOSED.jpg",
  ],

  "SNW 279": [
    "/static/assets/contents/cooler-SNW-279-SIDE__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-279-FRONT__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-279-FRONT__CLOSED.jpg",
  ],

  "SNW 286": [
    "/static/assets/contents/cooler-SNW-286-SIDE__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-286-FRONT__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-286-FRONT__CLOSED.jpg",
  ],

  "SNW 320SCD": [
    "/static/assets/contents/cooler-SNW-320SCD-SIDE__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-320SCD-FRONT__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-320SCD-FRONT__CLOSED.jpg",
  ],

  "SNW 336": [
    "/static/assets/contents/cooler-SNW-336-SIDE__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-336-FRONT__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-336-FRONT__CLOSED.jpg",
  ],

  "SNW 1006": [
    "/static/assets/contents/cooler-SNW-1006-SIDE__CLOSED.jpg",
    "/static/assets/contents/cooler-SNW-1006-FRONT__OPEN.jpg",
    "/static/assets/contents/cooler-SNW-1006-FRONT__CLOSED.jpg",
  ],

  "SNW 100F": [
    "/static/assets/contents/freezer-SNW-100F-FRONT__CLOSED.jpg",
    "/static/assets/contents/freezer-SNW-100F-TOP_CLOSED.jpg",
    "/static/assets/contents/freezer-SNW-100F-SIDE_CLOSED.jpg",
  ],

  "SNW 152": [
    "/static/assets/contents/freezer-SNW-152-FRONT__CLOSED.jpg",
    "/static/assets/contents/freezer-SNW-152-SIDE__CLOSED.jpg",
    "/static/assets/contents/freezer-SNW-152-TOP__CLOSED.jpg",
  ],

  "SNW 200F": [
    "/static/assets/contents/freezer-SNW-200F-FRONT__CLOSED.jpg",
    "/static/assets/contents/freezer-SNW-200F-TOP__CLOSED.jpg",
    "/static/assets/contents/freezer-SNW-200F-SIDE__CLOSED.jpg",
  ],

  "SNW 300F": [
    "/static/assets/contents/freezer-SNW-300F-FRONT__CLOSED.jpg",
    "/static/assets/contents/freezer-SNW-300F-TOP__CLOSED.jpg",
    "/static/assets/contents/freezer-SNW-300F-SIDE__CLOSED.jpg",
  ],

  "SNW 330 Curved": [
    "/static/assets/contents/freezer-SNW-330CURVED-FRONT__CLOSED.jpg",
    "/static/assets/contents/freezer-SNW-330CURVED-SIDE__CLOSED.jpg",
    "/static/assets/contents/freezer-SNW-330CURVED-FRONT__CLOSED.jpg",
  ],

  "Sanwoo Air Fryer": [
    "/static/assets/contents/airfryer-SIDE__OPEN.jpg",
    "/static/assets/contents/airfryer-FRONT__OPEN.jpg",
    "/static/assets/contents/airfryer-FRONT__CLOSED.jpg",
  ],

  "Sanwoo Air Purifier": [
    "/static/assets/sanwoo-home/AIRPURIFIER_SIDE.png",
    "/static/assets/sanwoo-home/AIRPURIFIER_FRONT.png",
    "/static/assets/sanwoo-home/AIRPURIFIER_BACK.png",
  ],

  "Sanwoo Air Fryer 52": [
    "/static/assets/contents/airfryer-55-FRONT__CLOSED.png",
    "/static/assets/contents/airfryer-55-TOP__OPEN.png",
    "/static/assets/contents/airfryer-55-SIDE__OPEN.png",
    "/static/assets/contents/airfryer-55-SIDE__CLOSED.png",
  ],

  "Sanwoo 14-in-1 Multi Cooker & Air Fryer": [
    "/static/assets/contents/multicooker__FRONT.png",
    "/static/assets/contents/multicooker__TOP.png",
    "/static/assets/contents/multicooker__TOP__TRANSPARENT.png",
  ],

  "Sanwoo Multifunction Low Sugar Rice Cooker": [
    "/static/assets/contents/ricecooker__FRONT.png",
    "/static/assets/contents/ricecooker__INNER.png",
    "/static/assets/contents/ricecooker__INNER_DOTTED.png",
    "/static/assets/contents/ricecooker__SIDE__CLOSED.png",
  ],
};

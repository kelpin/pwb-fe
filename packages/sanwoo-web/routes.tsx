export default {
  split: "/",
  freezerCooler: {
    index: {
      label: "",
      link: "/freezer-cooler",
    },
    cooler: {
      label: "Cooler",
      link: "/freezer-cooler/cooler",
    },
    freezer: {
      label: "Freezer",
      link: "/freezer-cooler/freezer",
      children: {
        hybrid: {
          label: "Hybrid Cooler",
          link: "/freezer-cooler/cooler?type=hybrid",
        },
        twoInOne: {
          label: "2 in 1 Cooler",
          link: "/freezer-cooler/cooler?type=2in1",
        },
      },
    },
    service: {
      label: "One Stop Solution",
      link: "/freezer-cooler/service",
    },
  },
  home: {
    index: {
      label: "",
      link: "/home",
    },
    airFryer: {
      label: "Air Fryer",
      link: "/home/airfryer",

      detail: {
        label: "Air Fryer Detail",
        link: "/home/airfryer/detail",
      },
    },
    airPurifier: {
      label: "Air Purifier",
      link: "/home/airpurifier",

      detail: {
        label: "Air Purifier Detail",
        link: "/home/airpurifier/detail",
      },
    },
    eWarranty: {
      label: "E-Warranty",
      link: "/home/e-warranty",
    },
    airFryer52: {
      label: "AirFryer 5.2L",
      link: "/home/airfryer-52",
    },
    multicooker: {
      label: "Rice Cooker",
      link: "/home/multicooker",
    },
    ricecooker: {
      label: "Rice Cooker",
      link: "/home/ricecooker",
    },
  },
  contactUs: {
    label: "Contact Us",
    link: "/freezer-cooler/contact-us",
  },
}

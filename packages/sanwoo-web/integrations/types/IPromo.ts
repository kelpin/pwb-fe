// {
//   "id": 1,
//   "gambar": "http://sanwoo.powerboardcms.com/images/slideshow/freezer-cooler-jumbotron-cooler.jpg",
//   "embed": "",
//   "judul": "Innovative cold merchandising solutions",
//   "keterangan": null,
//   "link": "#",
//   "kategori": "Cooler & Freezer"
// }
export interface IPromo {
  id: number;
  judul: string;
  image: string;
  link: string;
}

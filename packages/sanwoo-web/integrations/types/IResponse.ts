export interface IResponse<T> {
  status: {
    response: number;
    message: string;
  };
  data: T;
}

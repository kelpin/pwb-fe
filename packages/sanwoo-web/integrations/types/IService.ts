export interface IService {
  id: number;
  gambar: string;
  judul: string;
  deskripsi: string;
}

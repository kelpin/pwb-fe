// {
//   "id": 1,
//   "gambar": [
//   "http://sanwoo.powerboardcms.com/images/product/cooler-SNW-106-1.jpg",
//   "http://sanwoo.powerboardcms.com/images/product/cooler-SNW-106-2.jpg",
//   "http://sanwoo.powerboardcms.com/images/product/cooler-SNW-106-3.jpg"
//   ],
//   "judul": "SNW 106",
//   "overview": "ANTI RAT PROTECTION. LED LIGHTING. HIGH DENSITY SHELVES. DOUBLE TEMPERED GLASS. REMOVABLE BRANDING CANOPY & LOWER COVER"
// },
export interface IProduct {
  id: number;
  gambar: string[];
  judul: string;
  overview: string;
}

export type TKeys =
  | "Cooler"
  | "Mini Cooler"
  | "Hybrid Cooler"
  | "Freezer"
  | "Air Fryer"
  | "Air Purifier"
  | "2 in 1 cooler with freezer on top";

export type IProductGroupGrouped = {
  [key: string]: IProduct[];
};

export type IProductGroup = IProductGroupGrouped[];

// {
//   "id": 1,
//   "gambar": "http://sanwoo.powerboardcms.com/images/aboutus/img-about-us.jpg",
//   "judul": "About Us",
//   "deskripsi": "Founded in 2009, Sanwoo Electronics is a growing company that provides cooling solutions for leading FMCGs and retail businesses in the country and a wide range of home appliances products for households. We understand Indonesia is comprised of many islands hence we commit to make our products accessible for delivery and services nationwide."
// },
export interface IAbout {
  id: number;
  gambar: string;
  judul: string;
  deskripsi: string;
}

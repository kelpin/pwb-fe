
export interface IEcommerce {
  id: string,
  gambar: string,
  nama: string,
  link: string,
}

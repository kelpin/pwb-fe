export interface IWaranty {
  nama: string,
  telpon: string,
  alamat1: string,
  alamat2: string,
  email: string,
  kodepos: string,
  provinsi: string,
  model: string,
  modelLain: string,
  serialNumber: string,
  tglPurchase: string,
  tipeUnit: string,
}

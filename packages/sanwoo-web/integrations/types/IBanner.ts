// {
//   "id": 1,
//   "id_produk": null,
//   "gambar": "http://sanwoo.powerboardcms.com/images/banner/freezer-cooler-jumbotron-cooler-section.jpg",
//   "judul": "Ice Cold Merchandiser",
//   "keterangan": null
// },
export interface IBanner {
  id: number;
  id_produk?: number;
  gambar?: string;
  judul?: string;
  keterangan?: string;
}

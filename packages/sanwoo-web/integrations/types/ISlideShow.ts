// {
//   "id": 1,
//   "gambar": "http://sanwoo.powerboardcms.com/images/slideshow/freezer-cooler-jumbotron-cooler.jpg",
//   "embed": "",
//   "judul": "Innovative cold merchandising solutions",
//   "keterangan": null,
//   "link": "#",
//   "kategori": "Cooler & Freezer"
// }
export interface ISlideShow {
  id: number;
  gambar: string;
  embed: string;
  judul: string;
  keterangan: string;
  link: string;
  kategori: "Cooler & Freezer" | "Home";
}

export interface IMessage {
  nama: string;
  email: string;
  pesan: string;
  telp: string;
  topik: string;
}

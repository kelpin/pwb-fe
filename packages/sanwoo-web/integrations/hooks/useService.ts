import { Axios } from "@/integrations/helpers";
import { AxiosError } from "axios";
import { useQuery } from "react-query";
import { IResponse } from "../types/IResponse";
import { IService } from "../types/IService";

async function getService() {
  const { data } = await Axios.get("/service");
  return data;
}

export default function useService() {
  return useQuery<IResponse<IService[]>, AxiosError<Error>>(
    "service",
    getService,
    {
      enabled: true,
    }
  );
}

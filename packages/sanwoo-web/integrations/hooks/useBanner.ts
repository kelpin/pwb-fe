import { Axios } from "@/integrations/helpers";
import { IBanner } from "@/integrations/types/IBanner";
import { AxiosError } from "axios";
import { useQuery } from "react-query";
import { IResponse } from "../types/IResponse";

async function getBanner() {
  const { data } = await Axios.get("/banner");
  return data;
}

export default function useBanner() {
  return useQuery<IResponse<IBanner[]>, AxiosError<Error>>(
    "banner",
    getBanner,
    {
      enabled: true,
    }
  );
}

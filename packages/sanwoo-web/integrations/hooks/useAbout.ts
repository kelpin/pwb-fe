import { Axios } from "@/integrations/helpers";
import { IAbout } from "@/integrations/types/IAbout";
import { AxiosError } from "axios";
import { useQuery } from "react-query";
import { IResponse } from "../types/IResponse";

async function getAboutUs() {
  const { data } = await Axios.get("/about");
  return data;
}

export default function useAboutUs() {
  return useQuery<IResponse<IAbout[]>, AxiosError<Error>>("about", getAboutUs, {
    enabled: true,
  });
}

import { Axios } from "@/integrations/helpers";
import { AxiosError } from "axios";
import { useQuery } from "react-query";
import { ICategory } from "../types/ICategory";
import { IResponse } from "../types/IResponse";

async function getBanner() {
  const { data } = await Axios.get("/kategori");
  return data;
}

export default function useCategori() {
  return useQuery<IResponse<ICategory[]>, AxiosError<Error>>(
    "kategori",
    getBanner,
    {
      enabled: true,
    }
  );
}

import { Axios } from "@/integrations/helpers";
import { ISplit } from "@/types/splitScreen";
import { AxiosError } from "axios";
import { useQuery } from "react-query";
import { IResponse } from "../types/IResponse";

async function getSplitScreen() {
  const { data } = await Axios.get("/split");
  return data;
}

export default function useSplitScreen() {
  return useQuery<IResponse<ISplit[]>, AxiosError<Error>>(
    "split",
    getSplitScreen,
    {
      enabled: true,
    }
  );
}

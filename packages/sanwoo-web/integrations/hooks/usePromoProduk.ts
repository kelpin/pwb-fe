import { Axios } from "@/integrations/helpers";
import { ISlideShow } from "@/integrations/types/ISlideShow";
import { AxiosError } from "axios";
import { useQuery } from "react-query";
import { IPromo } from "../types/IPromo";
import { IPromoProduk } from "../types/IPromoProduk";
import { IResponse } from "../types/IResponse";

async function getPromoProduk() {
  const { data } = await Axios.get("/promo/produk");
  return data;
}

export default function usePromoProduk() {
  return useQuery<IResponse<IPromoProduk[]>, AxiosError<Error>>(
    "promo produk",
    getPromoProduk,
    {
      enabled: true,
    }
  );
}

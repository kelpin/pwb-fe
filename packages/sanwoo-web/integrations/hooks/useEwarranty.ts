import { Axios } from "../helpers";
import { IWaranty } from "../types/IEwaranty";

export default async function postEwaranty(params: IWaranty) {
  var data = new FormData();
  data.append("nama", params.nama);
  data.append("telpon", params.telpon);
  data.append("alamat1", params.alamat1);
  data.append("alamat2", params.alamat2);
  data.append("email ", params.email);
  data.append("kodepos", params.kodepos);
  data.append("provinsi", params.provinsi);
  data.append("model", params.model);
  data.append("modelLain", params.modelLain);
  data.append("serialNumber    ", params.serialNumber);
  data.append("tglPurchase", params.tglPurchase);
  data.append("tipeUnit", params.tipeUnit);


  const response = await Axios.post("/ewarranty", data);

  return response;
}

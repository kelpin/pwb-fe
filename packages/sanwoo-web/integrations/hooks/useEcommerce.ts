import { Axios } from "@/integrations/helpers";
import { AxiosError } from "axios";
import { useQuery } from "react-query";
import { IEcommerce } from "../types/IEcommerce";
import { IResponse } from "../types/IResponse";

async function getEcommerce() {
  const { data } = await Axios.get("/ecommerce");
  return data;
}

export default function userEcommerce() {
  return useQuery<IResponse<IEcommerce[]>, AxiosError<Error>>(
    "ecommerce",
    getEcommerce,
    {
      enabled: true,
    }
  );
}

import { Axios } from "@/integrations/helpers";
import { IProductGroup } from "@/integrations/types/IProduct";
import { AxiosError } from "axios";
import { useQuery } from "react-query";
import { IResponse } from "../types/IResponse";

async function getProducts() {
  const { data } = await Axios.get("/product");
  return data;
}

export default function userProducts() {
  return useQuery<IResponse<IProductGroup[]>, AxiosError<Error>>(
    "products",
    getProducts,
    {
      enabled: true,
    }
  );
}

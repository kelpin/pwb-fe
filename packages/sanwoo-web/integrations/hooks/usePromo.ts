import { Axios } from "@/integrations/helpers";
import { ISlideShow } from "@/integrations/types/ISlideShow";
import { AxiosError } from "axios";
import { useQuery } from "react-query";
import { IPromo } from "../types/IPromo";
import { IResponse } from "../types/IResponse";

async function getPromo() {
  const { data } = await Axios.get("/promo");
  return data;
}

export default function usePromo() {
  return useQuery<IResponse<IPromo[]>, AxiosError<Error>>(
    "promo",
    getPromo,
    {
      enabled: true,
    }
  );
}

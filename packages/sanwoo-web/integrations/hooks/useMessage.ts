import { Axios } from "../helpers";
import { IMessage } from "../types/IMessage";

export default async function postMessage(message: IMessage) {
  var data = new FormData();
  data.append("nama", message.nama ?? "");
  data.append("email", message.email);
  data.append("pesan", message.pesan);
  data.append("telp", message.telp);
  data.append("topik", message.topik);

  const response = await Axios.post("/message", data);

  return response;
}

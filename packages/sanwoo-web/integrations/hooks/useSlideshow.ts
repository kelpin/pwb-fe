import { Axios } from "@/integrations/helpers";
import { ISlideShow } from "@/integrations/types/ISlideShow";
import { AxiosError } from "axios";
import { useQuery } from "react-query";
import { IResponse } from "../types/IResponse";

async function getSlideshow() {
  const { data } = await Axios.get("/slideshow");
  return data;
}

export default function useSlideshow() {
  return useQuery<IResponse<ISlideShow[]>, AxiosError<Error>>(
    "slideshow",
    getSlideshow,
    {
      enabled: true,
    }
  );
}

import axios, { AxiosInstance } from "axios";

const requestHeaders = {
  Authorization: "Basic c2Fud29vOnNhbndvbzEyMzQ1Ng==",
};

const axiosInstance: AxiosInstance = axios.create({
  baseURL: process.env.API_HOST + "/api/client",
  headers: requestHeaders,
});

// axiosInstance.interceptors.request.use((request) => {
//   if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
//     return request;
//   } else {
//     return request;
//   }
// });

export { axiosInstance as Axios };

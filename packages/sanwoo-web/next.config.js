module.exports = {
  env: {
    apiHost: "https://sanwoo.powerboardcms.com",
    API_HOST: "https://sanwoo.powerboardcms.com",
  },
  images: {
    domains: ["https://sanwoo.powerboardcms.com/"],
  },
};

module.exports = {
  darkMode: false, // or 'media' or 'class'
  purge: [],
  theme: {
    fontSize: {
      h1: "5.8rem",
      h2: "3.625rem",
      h3: "2.875rem",
      h4: "2rem",
      h5: "1.4rem",
      h6: "1.1875rem",
      body: "1.125rem",
      body2: "1rem",
      subtitle: "0.9375rem",
      subtitle2: "0.8125rem",
      subtitle3: "0.785rem",
      button: "1rem",
      button2: "0.8125rem",
      caption: "1rem",
    },

    extend: {
      backgroundSize: {
        "105%": "105%",
        "110%": "110%",
      },
      fontFamily: {
        poppins: ["Poppins", "sans-serif"],
      },
      colors: {
        primary: "#2E2E79",
      },
      height: {
        "screen-80": "80vh",
        "screen-50": "50vh",
        72: "20rem",
      },
      width: {
        "max-w-7xl": "80rem",
        72: "20rem",
      },
      transitionProperty: {
        height: "height",
        spacing: "margin, padding",
        width: "width",
        backgroundSize: "background-size",
      },
    },
  },
  variants: {
    width: ["responsive", "hover"],
    backgroundSize: ["responsive", "hover", "focus"],
    transitionTimingFunction: ["responsive", "hover", "focus"],
    transitionProperty: ["responsive", "hover"],
  },
  plugins: [],
};

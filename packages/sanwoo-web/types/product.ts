export interface IProductDetail {
  gambar: string[];
  judul: string;
  overview: string;
}

export interface IProductSpec {
  judul: string;
  deskripsi: string;
}

export interface IProduk {
  produk: IProductDetail;
  spesifikasi: IProductSpec[];
}

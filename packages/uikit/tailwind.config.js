module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    fontSize: {
      h1: "5.8rem",
      h2: "3.625rem",
      h3: "2.875rem",
      h4: "2rem",
      h5: "1.4rem",
      h6: "1.1875rem",
      body: "1.125rem",
      body2: "1rem",
      subtitle: "0.9375rem",
      subtitle2: "0.8125rem",
      button: "1rem",
      button2: "0.8125rem",
      caption: "1rem",
    },
    height: {
      "screen-80": "80vh",

      "screen-30": "30vh",
    },
    extend: {
      fontFamily: {
        poppins: ["Poppins", "sans-serif"],
      },
      colors: {
        primary: "#2E2E79",
      },
    },
    maxWidth: {
      "5xl": "64rem",
    },
  },
  variants: {},
  plugins: [],
};

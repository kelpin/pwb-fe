import { Meta } from "@storybook/react/types-6-0";
import React from "react";
import Text from "../components/foundations/Text";

const Square = () => {
  return (
    <div className="bg-primary w-32 h-32 flex justify-center items-center p-5 rounded">
      <Text className="text-white text-center">Primary Color</Text>
    </div>
  );
};

export default {
  title: "Foundation/Color",
  component: Square,
} as Meta;

export const All = () => <Square />;

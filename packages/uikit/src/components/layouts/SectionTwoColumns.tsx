import React from "react";
import CssUtils from "../../utils/Css";
import { Text } from "../foundations/Text";

export interface SectionTwoColumnsProps {
  image: string;
  title: React.ReactText | string;
  description: React.ReactText | string;
  className?: string;
  imagePosition: "left" | "right";
  wrapperStyles?: string;
  imageContainerStyles?: string;
  textWrapperStyles?: string;
  textContainerStyles?: string;
  imageAlt?: string;
}

const tailwindStyles = {
  wrapper: "h-screen-80 flex w-full",
  container: "flex flex-1 ",
  imageContainer: "flex-col justify-center items-center",
  textWrapper: "flex-row items-center",
  textContainer: "flex flex-col justify-start items-start w-2/3",
  image: "",
  title: "",
  subtitle: "",
};

export const SectionTwoColumns: React.FC<SectionTwoColumnsProps> = ({
  image,
  title,
  description,
  imagePosition = "left",
  wrapperStyles = "",
  imageContainerStyles = "",
  textContainerStyles = "",
  textWrapperStyles = "",
  imageAlt,
}) => {
  const layoutStyles =
    imagePosition === "left" ? "flex-row" : "flex-row-reverse";
  const containerTextLayout =
    imagePosition === "left" ? "justify-start pr-12" : "justify-end px-12";
  return (
    <section
      className={CssUtils.merge([
        tailwindStyles.wrapper,
        layoutStyles,
        wrapperStyles,
      ])}
    >
      <div
        className={CssUtils.merge([
          tailwindStyles.container,
          tailwindStyles.imageContainer,
          imageContainerStyles,
        ])}
      >
        <img src={image} alt={imageAlt ? imageAlt : image} />
      </div>
      <div
        className={CssUtils.merge([
          tailwindStyles.container,
          tailwindStyles.textWrapper,
          containerTextLayout,
          textWrapperStyles,
        ])}
      >
        <div className={(tailwindStyles.textContainer, textContainerStyles)}>
          <Text kind="h3" className="text-primary">
            {title}
          </Text>
          <Text kind="subtitle">{description}</Text>
        </div>
      </div>
    </section>
  );
};

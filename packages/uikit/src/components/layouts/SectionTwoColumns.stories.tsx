import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import { SectionTwoColumns, SectionTwoColumnsProps } from "./SectionTwoColumns";

export default {
  title: "Layout/SectionTwoColumns",
  component: SectionTwoColumns,
} as Meta;

const Template: Story<SectionTwoColumnsProps> = (args) => (
  <SectionTwoColumns {...args} />
);

export const Default = Template.bind({});
Default.args = {
  image: "https://picsum.photos/200/300",
  title: "Example Title",
  description:
    "Lorem Ipsum Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
};

export const Right = Template.bind({});
Right.args = {
  image: "https://picsum.photos/200/300",
  title: "Example Title",
  description:
    "Lorem Ipsum Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
  imagePosition: "right",
};

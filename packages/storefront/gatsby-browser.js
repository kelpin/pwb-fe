// custom typefaces
import "typeface-montserrat"
import "typeface-merriweather"
// normalize CSS across browsers
// custom CSS styles
import "./src/tailwind.css"
import "./src/index.css"

// Highlighting for code blocks
import "prismjs/themes/prism.css"

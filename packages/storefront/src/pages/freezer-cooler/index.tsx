// If you don't want to use TypeScript you can delete this file!
import React from "react"
import { PageProps } from "gatsby"
import { SectionTwoColumns } from "../../components/layouts/SectionTwoColumns"
import { Text } from "../../components/foundations/Text"
import { Carousel } from "../../components/carousel/Carousel"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import CarouselShowcase from "../../components/carousel/CarouselShowcase"
import IMAGES from "../../assets/images"
import ID from "../../translations/id"
import List from "../../components/List"
import { ButtonLink } from "../../components/Button"
import BuyModal from "../../components/BuyModal"
import ROUTES from "../../routes"

type DataProps = {
  site: {
    buildTime: string
  }
}
const CarouselItem__TopBannerAugust = () => {
  const tailwindStyles = {
    container:
      "bg-cover h-screen md:h-screen-80 flex flex-row w-full items-center justify-center py-16",
  }
  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${IMAGES.topBannerAugust})`,
        backgroundPosition: "top-left",
      }}
    ></section>
  )
}
const CarouselItem__Video__SNW279 = () => {
  const tailwindStyles = {
    container:
      "bg-center h-screen-80 flex flex-col w-full items-center justify-center",
  }

  return (
    <section className={CssUtils.merge([tailwindStyles.container, ""])}>
      <iframe
        style={{ width: "100vw", height: "100vh" }}
        width="560"
        height="315"
        src="https://www.youtube.com/embed/5EJ9b7GbK6A?controls=0&autoplay=1&mute=1&playlist=5EJ9b7GbK6A&loop=1&modestbranding=0&disablekb=1"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      ></iframe>
    </section>
  )
}
const CarouselItem__Video__SNW270 = () => {
  const tailwindStyles = {
    container:
      "bg-center h-screen-80 flex flex-col w-full items-center justify-center",
  }

  return (
    <section className={CssUtils.merge([tailwindStyles.container, ""])}>
      <iframe
        style={{ width: "100vw", height: "100vh" }}
        width="560"
        height="315"
        src="https://www.youtube.com/embed/00MGMS2rg6w?controls=0&autoplay=1&mute=1&playlist=00MGMS2rg6w&loop=1&modestbranding=0&disablekb=1"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      ></iframe>
    </section>
  )
}

let CarouselShowcaseImage: React.FC<{ src: string; alt: string }> = ({
  src,
  alt,
}) => {
  return (
    <div className="p-10 flex justify-center items-center h-screen-50">
      <img
        className="self-center"
        src={src}
        alt={alt}
        style={{ maxHeight: "100%" }}
      />
    </div>
  )
}

type SectionCarouselTypes = {
  heading: string
  desc: string
  link: string
  image: string
}

const Section__Carousel: React.FC<SectionCarouselTypes> = ({
  heading,
  desc,
  link,
  image,
}) => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex w-full items-center justify-center md:py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    spacer: "hidden md:block w-1/12",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${image})`,
      }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="w-screen md:flex-1 flex flex-col items-start md:px-0">
        <div className="mb-10">
          <Text
            kind="h3"
            className="text-h4 md:text-h3 leading-4 md:leading-none font-bold leading-none text-primary mb-4 px-3 md:px-0"
          >
            {heading}
          </Text>
          <Text
            kind="h5"
            className="text-black text-body2 md:text-h5 md:leading-relaxed max-w-sm md:max-w-2xl lg:max-w-4xl w-screen md:w-full font-light px-3 md:px-0"
          >
            {desc}
          </Text>
        </div>

        <div className="px-3 md:px-0">
          <ButtonLink to={link} label="Shop Now" kind="primary" />
        </div>
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  )
}

const Section_CoolerOneColumn = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex flex-col w-full items-center justify-between py-16",
  }

  let [isMd, setIsMd] = React.useState(false)

  React.useEffect(() => {
    let isMd = window.matchMedia("(min-width: 768px)")
    setIsMd(isMd.matches)
  }, [])

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${
          isMd
            ? IMAGES.freezerCoolerJumbotronCoolerSection
            : IMAGES.freezerCoolerJumbotronCoolerMobile
        })`,
      }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none w-2/3 md:w-full text-center">
          {ID["freezerCooler.cooler.subtitle"]}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-primary text-center"
        >
          {ID["freezerCooler.cooler.title"]}
        </Text>
      </div>
    </section>
  )
}

const Section_FreezerOneColumn = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex flex-col w-full items-center justify-between py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  }

  let [isMd, setIsMd] = React.useState(false)

  React.useEffect(() => {
    let isMd = window.matchMedia("(min-width: 768px)")
    setIsMd(isMd.matches)
  }, [])

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${IMAGES.bannerPartner})`,
        width: "100vw",
        height: "50vw",
      }}
    >
      {/* <div className="flex flex-col items-center"> */}
      {/*   <Text className="leading-none w-2/3 md:w-full text-center"> */}
      {/*     {ID["freezerCooler.freezer.subtitle"]} */}
      {/*   </Text> */}
      {/*   <Text */}
      {/*     kind="h3" */}
      {/*     className="font-bold leading-none text-primary text-center" */}
      {/*   > */}
      {/*     {ID["freezerCooler.freezer.title"]} */}
      {/*   </Text> */}
      {/* </div> */}
    </section>
  )
}

const CoolerHome: React.FC<PageProps<DataProps>> = ({
  data,
  path,
  location,
}) => {
  const [showBuyModal, setShowBuyModal] = React.useState(false)

  return (
    <Layout title="Sanwoo Cooler & Freezer" location={location}>
      <SEO title="Sanwoo Cooler & Freezer" />

      {showBuyModal && <BuyModal onClose />}
      <Carousel arrowStyle="purple">
        <CarouselItem__TopBannerAugust />
        <Section__Carousel
          heading={ID["freezerCooler.jumbotron.innovative.title"]}
          desc={ID["freezerCooler.jumbotron.innovative.desc"]}
          link={ROUTES.freezerCooler.cooler.link + "#items-list"}
          image={IMAGES.freezerCoolerJumbotronCooler}
        />
        <Section__Carousel
          heading={ID["freezerCooler.jumbotron.feelMaximum.title"]}
          desc={ID["freezerCooler.jumbotron.feelMaximum.desc"]}
          link={ROUTES.freezerCooler.cooler.link + "#items-list"}
          image={IMAGES.freezerCoolerJumbotronFeelMaximum}
        />
        <Section__Carousel
          heading={ID["freezerCooler.jumbotron.quality.title"]}
          desc={ID["freezerCooler.jumbotron.quality.desc"]}
          link={ROUTES.freezerCooler.freezer.link + "#items-list"}
          image={IMAGES.freezerCoolerJumbotronFreezerLeft}
        />
        <CarouselItem__Video__SNW270 />
        <CarouselItem__Video__SNW279 />
      </Carousel>

      <SectionTwoColumns
        image={IMAGES.freezerCoolerAboutUs}
        imagePosition="left"
      >
        <Text kind="h3" className="text-primary mb-2">
          {ID["freezerCooler.aboutUs.title"]}
        </Text>
        <Text kind="body" className="leading-relaxed">
          {ID["freezerCooler.aboutUs.description"]}
        </Text>
      </SectionTwoColumns>

      <Section_CoolerOneColumn />

      <CarouselShowcase to="/freezer-cooler/cooler">
        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_coolerLanding1}
          alt="image-cooler-landing-1"
        />
        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_coolerLanding2}
          alt="image-cooler-landing-2"
        />
        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_coolerLanding3}
          alt="image-cooler-landing-3"
        />

        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_coolerLanding4}
          alt="image-cooler-landing-4"
        />

        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_coolerLanding5}
          alt="image-cooler-landing-5"
        />

        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_coolerLanding6}
          alt="image-cooler-landing-6"
        />
        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_coolerLanding7}
          alt="image-cooler-landing-7"
        />
        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_coolerLanding8}
          alt="image-cooler-landing-8"
        />
      </CarouselShowcase>

      <Section_FreezerOneColumn />
      <CarouselShowcase to="/freezer-cooler/freezer">
        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_freezerLanding1}
          alt="image-SNW-200F"
        />

        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_freezerLanding2}
          alt="image-freezer-landing-2"
        />
        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_freezerLanding3}
          alt="image-freezer-landing-3"
        />
        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_freezerLanding4}
          alt="image-freezer-landing-4"
        />
        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_freezerLanding5}
          alt="image-freezer-landing-5"
        />
        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_freezerLanding1}
          alt="image-SNW-200F"
        />

        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_freezerLanding2}
          alt="image-freezer-landing-2"
        />
        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_freezerLanding3}
          alt="image-freezer-landing-3"
        />
        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_freezerLanding4}
          alt="image-freezer-landing-4"
        />
        <CarouselShowcaseImage
          src={IMAGES.freezerCooler_freezerLanding5}
          alt="image-freezer-landing-5"
        />
      </CarouselShowcase>

      <SectionTwoColumns
        image={IMAGES.freezerCoolerOurAdvantage}
        imagePosition="left"
      >
        <Text kind="h3" className="text-primary">
          {ID["freezerCooler.freezer.advantageTitle"]}
        </Text>
        <ul className="section-list mt-4">
          <List>{ID["freezerCooler.freezer.advantageItem1"]}</List>
          <List>{ID["freezerCooler.freezer.advantageItem2"]}</List>
        </ul>
      </SectionTwoColumns>

      <SectionTwoColumns
        image={IMAGES.freezerCoolerOneStopSolutiokn}
        imagePosition="right"
      >
        <Text kind="h3" className="text-primary">
          {ID["freezerCooler.freezer.oneStopSolutionTitle"]}
        </Text>
        <Text kind="body" className="leading-relaxed my-8">
          {ID["freezerCooler.freezer.oneStopSolutionDescription"]}
        </Text>
        <ButtonLink
          to="/freezer-cooler/service"
          label="Read more"
          kind="primary"
        />
      </SectionTwoColumns>
    </Layout>
  )
}

export default CoolerHome

// If you don't want to use TypeScript you can delete this file!
import React from "react"
import { PageProps } from "gatsby"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Text } from "../../components/foundations/Text"
import IMAGES from "../../assets/images"
import { ListItem } from "../../components/ListItem"
import { ButtonTab } from "../../components/ButtonTab"
import { ButtonLink } from "../../components/Button"
import ID from "../../translations/id"
import {
  coolers,
  miniCoolers,
  hybridCoolers,
  coolerFreezer,
  ProductType,
} from "../../constants/contents"
import ROUTES from "../../routes"

type DataProps = {
  site: {
    buildTime: string
  }
}

type CoolersType =
  | "cooler"
  | "mini-cooler"
  | "hybrid-cooler"
  | "cooler-freezer"
  | "all"

const renderCoolerList = (arrOfCooler: Array<ProductType>) => {
  return arrOfCooler.map(({ name, desc, image, to }) => (
    <ListItem image={image} title={name} desc={desc} to={to} />
  ))
}

let renderCoolers = (coolerType: CoolersType) => {
  switch (coolerType) {
    case "cooler":
      return renderCoolerList(coolers)

    case "mini-cooler":
      return renderCoolerList(miniCoolers)

    case "hybrid-cooler":
      return renderCoolerList(hybridCoolers)

    case "cooler-freezer":
      return renderCoolerList(coolerFreezer)

    case "all":
      return renderCoolerList(allCoolers)
  }
}

const allCoolers = [
  ...coolers,
  ...coolerFreezer,
  ...hybridCoolers,
  ...miniCoolers,
]

const Section__Intro = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    spacer: "w-1/12",
  }

  return (
    <section
      style={{
        backgroundImage: `url(${IMAGES.freezerCoolerJumbotronCooler})`,
      }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-start ">
        <div className="mb-10">
          <Text kind="h3" className="font-bold leading-none text-primary mb-4">
            {ID["freezerCooler.jumbotron.feelMaximum.title"]}
          </Text>
          <Text className="leading-none text-black max-w-sm">
            {ID["freezerCooler.jumbotron.feelMaximum.desc"]}
          </Text>
        </div>

        <ButtonLink to={"#items-list"} label="Shop Now" kind="primary" />
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  )
}

const Section__Advantage = () => {
  const tailwindStyles = {
    column:
      "h-screen-80 h-full w-full md:w-1/2 md:bg-bottom  bg-no-repeat bg-bottom flex flex-col items-center justify-start pt-10",
  }

  return (
    <section>
      <div className="w-full flex flex-col md:flex-row justify-between items-center">
        <div
          className={tailwindStyles.column}
          style={{
            backgroundColor: "#363636",
            backgroundImage: `url(${IMAGES.coolerBigger})`,
            backgroundSize: "cover",
          }}
        >
          <Text kind="h4" className="font-bold leading-none text-white">
            {ID["cooler.biggerSection.title"]}
          </Text>
          <Text className="leading-none text-white text-center max-w-sm px-4">
            {ID["cooler.biggerSection.description"]}
          </Text>
        </div>
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${IMAGES.coolerEco})`,
            backgroundSize: "cover",
          }}
        >
          <Text kind="h4" className="font-bold leading-none text-primary">
            {ID["cooler.ecoFriendly.title"]}
          </Text>
          <Text className="leading-none text-black text-center max-w-sm mb-8 px-4">
            {ID["cooler.ecoFriendly.description"]}
          </Text>
        </div>
      </div>
    </section>
  )
}

const Section__ItemsList = () => {
  const [coolerType, setCoolerType] = React.useState<CoolersType>("all")

  const renderTabContainer = (): React.ReactElement => {
    return (
      <div className="flex flex-row md:w-10/12 w-full px-4 lg:max-w-6xl md:grid md:grid-cols-5 mb-10 overflow-x-auto">
        <ButtonTab
          label="All"
          onClick={() => setCoolerType("all")}
          active={coolerType === "all"}
        />
        <ButtonTab
          label="Cooler"
          onClick={() => setCoolerType("cooler")}
          active={coolerType === "cooler"}
        />

        <ButtonTab
          label="Mini Cooler"
          onClick={() => setCoolerType("mini-cooler")}
          active={coolerType === "mini-cooler"}
        />
        <ButtonTab
          label="Hybrid Cooler"
          onClick={() => setCoolerType("hybrid-cooler")}
          active={coolerType === "hybrid-cooler"}
        />
        <ButtonTab
          label="2 in 1 cooler with freezer on top"
          onClick={() => setCoolerType("cooler-freezer")}
          active={coolerType === "cooler-freezer"}
        />
      </div>
    )
  }

  return (
    <div
      id="items-list"
      className="w-full flex flex-col items-center justify-center py-10 pb-32"
    >
      <Text kind="h5" className="leading-none font-light text-center">
        {ID["cooler.itemsSubtitle"]}
      </Text>
      <Text
        kind="h3"
        className="font-bold leading-none text-primary mb-10 text-center"
      >
        {ID["cooler.itemsTitle"]}
      </Text>
      {renderTabContainer()}
      <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4  md:w-10/12 gap-4 md:gap-8  w-full px-4 lg:max-w-6xl">
        {renderCoolers(coolerType)}
      </div>
    </div>
  )
}

const CoolerHome: React.FC<PageProps<DataProps>> = ({
  // data,
  // path,
  location,
}) => {
  return (
    <Layout title="Cooler Home" location={location}>
      <SEO title="Cooler" />
      <Section__Intro />
      <Section__Advantage />
      <Section__ItemsList />
    </Layout>
  )
}

export default CoolerHome

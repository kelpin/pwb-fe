// If you don't want to use TypeScript you can delete this file!
import React from "react"
import { PageProps } from "gatsby"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Text } from "../../components/foundations/Text"
import { ListItem } from "../../components/ListItem"
import IMAGES from "../../assets/images"
import { ButtonLink } from "../../components/Button"
import ID from "../../translations/id"
import { freezers, ProductType } from "../../constants/contents"

type DataProps = {
  site: {
    buildTime: string
  }
}

const renderFreezerList = (arrOfCooler: Array<ProductType>) => {
  return arrOfCooler.map(({ name, desc, image, to }) => (
    <ListItem image={image} title={name} desc={desc} to={to} />
  ))
}

const Section__Intro = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    spacer: "w-1/12",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${IMAGES.freezerCoolerJumbotronFreezerLeft})`,
      }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-start ">
        <div className="mb-10">
          <Text kind="h3" className="font-bold leading-none text-primary mb-4">
            {ID["freezerCooler.jumbotron.quality.title"]}
          </Text>
          <Text className="leading-none text-black max-w-sm">
            {ID["freezerCooler.jumbotron.quality.desc"]}
          </Text>
        </div>

        <ButtonLink to="#items-list" label="Shop Now" kind="primary" />
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  )
}

const Section__Advantage = () => {
  const tailwindStyles = {
    column:
      "h-screen-80 h-full w-full md:w-1/2 md:bg-bottom bg-contain bg-no-repeat bg-bottom flex flex-col items-center justify-start pt-10 text-center px-4",
  }

  return (
    <section>
      <div className="w-full flex flex-col md:flex-row justify-between items-center">
        <div
          className={CssUtils.merge([tailwindStyles.column])}
          style={{
            backgroundColor: "#363636",
            backgroundImage: `url(${IMAGES.freezerBest})`,
          }}
        >
          <Text
            kind="h4"
            className="font-bold leading-none text-white text-center"
          >
            {ID["freezer.bestCold.title"]}
          </Text>
          <Text className="leading-none text-white text-center max-w-sm">
            {ID["freezer.bestCold.description"]}
          </Text>
        </div>
        <div
          className={tailwindStyles.column}
          style={{
            backgroundColor: "#fff",
            backgroundImage: `url(${IMAGES.freezerEasy})`,
          }}
        >
          <Text kind="h4" className="font-bold leading-none text-primary">
            {ID["freezer.easyServicing.title"]}
          </Text>
          <Text className="leading-none text-black text-center max-w-sm mb-8">
            {ID["freezer.easyServicing.description"]}
          </Text>
        </div>
      </div>
    </section>
  )
}

const Section__ItemsList = () => {
  return (
    <div
      id="items-list"
      className="w-full flex flex-col items-center justify-center pt-10 pb-32"
    >
      <Text kind="h5" className="leading-none font-light">
        {ID["freezer.itemsSubtitle"]}
      </Text>
      <Text
        kind="h3"
        className="font-bold leading-none text-primary mb-10 text-center"
      >
        {ID["freezer.itemsTitle"]}
      </Text>
      <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 md:gap-8 w-full md:w-10/12 lg:max-w-6xl px-4">
        {renderFreezerList(freezers)}
      </div>
    </div>
  )
}

const CoolerHome: React.FC<PageProps<DataProps>> = ({ location }) => (
  <Layout title="Freezer Home" location={location}>
    <SEO title="Freezer" />

    <Section__Intro />
    <Section__Advantage />
    <Section__ItemsList />
  </Layout>
)

export default CoolerHome

// If you don't want to use TypeScript you can delete this file!
import React from "react"
import { PageProps, Link, graphql } from "gatsby"
import { SectionTwoColumns } from "../../components/layouts/SectionTwoColumns"
import { Text } from "../../components/foundations/Text"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import IMAGES from "../../assets/images"
import ID from "../../translations/id"

type DataProps = {
  site: {
    buildTime: string
  }
}

const Section__Intro = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    spacer: "w-1/12",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${IMAGES.serviceJumbotron})` }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-center md:items-start">
        <Text
          kind="h3"
          className="leading-4 md:leading-normal font-bold text-primary mb-4 px-3 md:px-0"
        >
          {ID["service.jumbotron.oneStop.title"]}
        </Text>
        <Text
          kind="h5"
          className="md:leading-relaxed max-w-sm md:max-w-2xl lg:max-w-4xl text-primary w-screen md:w-full font-light px-3 md:px-0"
        >
          {ID["service.jumbotron.oneStop.desc"]}
        </Text>
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  )
}

const CoolerHome: React.FC<PageProps<DataProps>> = ({
  data,
  path,
  location,
}) => (
  <Layout title="Service" location={location}>
    <SEO title="service" />
    <Section__Intro />

    <SectionTwoColumns image={IMAGES.serviceWorkshop} imagePosition="left">
      <Text kind="h3" className="text-primary mb-4">
        {ID["service.section.workshopAround.title"]}
      </Text>
      <Text kind="body">{ID["service.section.workshopAround.desc"]}</Text>
    </SectionTwoColumns>

    <SectionTwoColumns image={IMAGES.serviceRebranding} imagePosition="right">
      <Text kind="h3" className="text-primary mb-4">
        {ID["service.section.rebranding.title"]}
      </Text>

      <Text kind="body">{ID["service.section.rebranding.desc"]}</Text>
    </SectionTwoColumns>

    <SectionTwoColumns image={IMAGES.servicePlacement} imagePosition="left">
      <Text kind="h3" className="text-primary mb-4">
        {ID["service.section.placement.title"]}
      </Text>
      <Text kind="body">{ID["service.section.placement.desc"]}</Text>
    </SectionTwoColumns>
  </Layout>
)

export default CoolerHome

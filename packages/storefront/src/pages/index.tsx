import React from "react"
import { PageProps } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { Text } from "../components/foundations/Text"
import { ButtonLink } from "../components/Button"
import CssUtils from "css-utils"

type DataProps = {
  site: {
    buildTime: string
  }
}

let backgroundFreezerCooler = require("../assets/split/split-freezer-cooler.jpg")
let backgroundHome = require("../assets/split/split-home.jpg")
let logoFreezerCooler = require("../assets/split/split-logo-freezer-cooler.png")
let logoHome = require("../assets/split/split-logo-home.png")

const tailwindStyles = {
  column:
    "w-full md:w-1/2 h-screen bg-center bg-contain bg-black bg-no-repeat flex flex-col items-center justify-center",
  logo: " w-2/3 md:w-1/2",
  textDescription:
    "text-white w-2/3 md:w-1/2 leading-relax md:leading-none text-center",
}
const Split: React.FC<PageProps<DataProps>> = ({ data, path, location }) => {
  return (
    <div>
      <SEO title="Sanwoo Electronics" />

      <div className="w-full flex flex-col md:flex-row justify-between items-center">
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${backgroundFreezerCooler})`,
            height: "100vh",
          }}
        >
          <img
            className={tailwindStyles.logo}
            src={logoFreezerCooler}
            alt="logo-freezer-cooler-white"
          />
          <Text className={tailwindStyles.textDescription}>
            Wide range of coolers & freezers that suit your businesses
          </Text>
          <ButtonLink
            kind="secondary-white"
            label="Learn More"
            to="/freezer-cooler"
            className="mt-5 rounded-full hover:opacity-75"
          />
        </div>
        <div
          className={CssUtils.merge([tailwindStyles.column])}
          style={{ backgroundImage: `url(${backgroundHome})`, height: "100vh" }}
        >
          <img className={tailwindStyles.logo} src={logoHome} alt="logo-home" />
          <Text className={tailwindStyles.textDescription}>
            Ideas that bring comfort wherever you are. <br /> Get what your
            house needs
          </Text>

          <ButtonLink
            kind="secondary-white"
            label="Learn More"
            to="/home"
            className="mt-5 rounded-full hover:opacity-75"
          />
        </div>
      </div>
    </div>
  )
}

export default Split

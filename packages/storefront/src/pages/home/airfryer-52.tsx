import React from "react"
import { PageProps } from "gatsby"
import { Text } from "../../components/foundations/Text"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import IMAGES from "../../assets/images"
import { ButtonLink } from "../../components/Button"
import ROUTES from "../../routes"

type DataProps = {
  site: {
    buildTime: string
  }
}

const Section__Intro = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-50vw flex w-full items-center justify-center py-16",
    spacer: "w-1/12",
  }

  return (
    <section
      style={{ backgroundImage: `url(${IMAGES.airfryer52NewBigger})` }}
    >
      <div className={tailwindStyles.spacer}></div>
      {/* <div className="flex-1 flex flex-col items-center md:items-start"> */}
      {/*   <div className="mb-10"> */}
      {/*     <Text className="leading-none text-white max-w-sm"> */}
      {/*       Sanwoo Air Fryer 5.2 L */}
      {/*     </Text> */}
      {/*     <Text kind="h3" className="font-bold leading-none text-white mb-4"> */}
      {/*       Coming Soon */}
      {/*     </Text> */}
      {/*     <Text className="leading-none text-white max-w-sm"> */}
      {/*       Bigger, Better, Faster */}
      {/*     </Text> */}
      {/*   </div> */}

      {/*   <ButtonLink */}
      {/*     to={ROUTES.home.airFryer52.link + "/detail"} */}
      {/*     label="Shop Now" */}
      {/*     kind="white" */}
      {/*   /> */}
      {/* </div> */}
      <div className={tailwindStyles.spacer}></div>
    </section>
  )
}

const Section__TwoColumns = () => {
  const tailwindStyles = {
    column:
      "h-screen-100vw md:h-screen-50vw w-screen-100vw md:w-screen-50vw bg-center bg-contain flex flex-col items-center justify-start pt-10 bg-no-repeat",
    heading: "w-2/3 md:w-full text-center",
  }

  return (
    <section>
      <div className="w-full flex flex-col md:flex-row justify-between items-center">
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${IMAGES.airfryer52NewLeft})`,
            backgroundPosition: "right",
          }}
        ></div>
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${IMAGES.airfryer52NewRight})`,
            backgroundPosition: "left",
          }}
        ></div>
      </div>
    </section>
  )
}

const Section__Something = () => {
  const tailwindStyles = {
    container:
      "bg-contain w-100vw screen-80 bg-center flex flex-col items-center justify-between  bg-no-repeat",
  }

  return (
    <section
      style={{
        backgroundImage: `url(${IMAGES.airfryer52NewPromo})`,
        width: "100vw",
        height: "50vw",
      }}
    >
      {/* <div className="flex flex-col items-center"> */}
      {/*   <Text className="leading-none w-2/3 md:w-full text-center text-white"> */}
      {/*     Bigger, Better, Faster */}
      {/*   </Text> */}
      {/*   <Text */}
      {/*     kind="h3" */}
      {/*     className="font-bold leading-none text-black text-center text-white" */}
      {/*   > */}
      {/*     SOMETHING BIG IS COMING */}
      {/*   </Text> */}
      {/* </div> */}
    </section>
  )
}

interface ProductCardProps {
  image: string
}

const Section__Products = () => {
  const tailwindStyles = {
    container: "flex flex-col w-full items-center justify-between py-16",
  }
  const ProductCard: React.FC<ProductCardProps> = ({ image }) => {
    return (
      <div className="flex flex-col w-72 h-72 justify-center items-center p-2 rounded-lg shadow-sm bg-white hover:shadow-lg mx-4 my-4">
        <img src={image} alt={image} className="h-full" />
      </div>
    )
  }

  return (
    <section className={tailwindStyles.container}>
      <div className="flex flex-col w-full justify-between items-start md:items-center overflow-auto">
        <div className="flex justify-start items-center mb-10 ">
          <ProductCard image={IMAGES["Sanwoo Air Fryer 52"][3]} />
          <ProductCard image={IMAGES["Sanwoo Air Fryer 52"][1]} />
          <ProductCard image={IMAGES["Sanwoo Air Fryer 52"][2]} />
        </div>
      </div>

      <div className="flex flex-col md:flex-row items-center justify-between mb-10">
        <ButtonLink
          className="mx-2 my-2"
          to={ROUTES.home.airFryer52.link + "/detail"}
          label="Shop Now"
          kind="black"
        />
      </div>
    </section>
  )
}

const SanwoHomeAirfryer: React.FC<PageProps<DataProps>> = ({
  data,
  path,
  location,
}) => (
  <Layout title="Sanwoo Home" location={location}>
    <SEO title="Sanwoo Home" />
    <Section__Intro />
    <Section__TwoColumns />
    <Section__Something />
    <Section__Products />
  </Layout>
)

export default SanwoHomeAirfryer

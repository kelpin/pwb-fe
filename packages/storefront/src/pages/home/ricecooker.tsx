import React from "react"
import { PageProps } from "gatsby"
import { Text } from "../../components/foundations/Text"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import IMAGES from "../../assets/images"
import CssUtils from "css-utils"
import { ButtonLink } from "../../components/Button"
import ROUTES from "../../routes"

type DataProps = {
  site: {
    buildTime: string
  }
}

const Section__Intro = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex w-full items-center justify-center py-16",
    spacer: "w-1/12",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${IMAGES.riceCookerIntro})` }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-center md:items-start">
        <div className="mb-10">
          <Text className="leading-none  max-w-sm text-white">
            {"Sanwoo Rice Cooker"}
          </Text>
          <Text kind="h3" className="font-bold leading-none mb-4 text-white">
            {"Multifunction"}
          </Text>
          <Text className="leading-none max-w-sm text-white">
            {"Low Sugar Rice Cooker"}
          </Text>
        </div>

        <ButtonLink
          to={ROUTES.home.ricecooker.link + "/detail"}
          label="Shop Now"
          kind="white"
        />
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  )
}

const Section__TwoColumns = () => {
  const tailwindStyles = {
    column:
      "h-screen md:h-screen-80 w-full md:w-1/2 bg-center bg-contain flex flex-col items-center justify-start pt-10",
    heading: "w-2/3 md:w-full text-center",
  }

  return (
    <section>
      <div className="w-full flex flex-col md:flex-row justify-between items-center">
        <div
          className={tailwindStyles.column}
          style={{
            backgroundColor: "#5F666C",
            backgroundImage: `url(${IMAGES.riceCookerLeft})`,
            backgroundRepeat: "no-repeat",
          }}
        ></div>
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${IMAGES.riceCookerRight})`,
            backgroundColor: "#111",
            backgroundRepeat: "no-repeat",
          }}
        ></div>
      </div>
    </section>
  )
}

const Section__Multifunction = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen flex flex-col w-full items-center justify-between py-16",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${IMAGES.riceCookerLowSugar})` }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none w-2/3 md:w-full text-center">
          {"Sanwoo Multifunction"}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-black text-center"
        >
          {"Low Sugar Rice Cooker"}
        </Text>
      </div>
    </section>
  )
}

interface ProductCardProps {
  image: string
}

const Section__Products = () => {
  const tailwindStyles = {
    container: "flex flex-col w-full items-center justify-between py-16",
  }
  const ProductCard: React.FC<ProductCardProps> = ({ image }) => {
    return (
      <div className="flex flex-col w-72 h-72 justify-center items-center p-2 rounded-lg shadow-sm bg-white hover:shadow-lg mx-4 my-4">
        <img src={image} alt={image} className="h-full" />
      </div>
    )
  }

  return (
    <section className={tailwindStyles.container}>
      <div className="flex flex-col w-full justify-between items-start md:items-center overflow-auto">
        <div className="flex justify-start items-center mb-10 ">
          <ProductCard
            image={IMAGES["Sanwoo Multifunction Low Sugar Rice Cooker"][0]}
          />
          <ProductCard
            image={IMAGES["Sanwoo Multifunction Low Sugar Rice Cooker"][2]}
          />
          <ProductCard
            image={IMAGES["Sanwoo Multifunction Low Sugar Rice Cooker"][1]}
          />
        </div>
      </div>

      <div className="flex flex-col md:flex-row items-center justify-between mb-10">
        <ButtonLink
          className="mx-2 my-2"
          to={ROUTES.home.ricecooker.link + "/detail"}
          label="Shop Now"
          kind="black"
        />
      </div>
    </section>
  )
}

const SanwoHomeRiceCooker: React.FC<PageProps<DataProps>> = ({
  data,
  path,
  location,
}) => (
  <Layout title="Sanwoo Home" location={location}>
    <SEO title="Sanwoo Multi Cooker" />
    <Section__Intro />
    <Section__TwoColumns />
    <Section__Multifunction />
    <Section__Products />
  </Layout>
)

export default SanwoHomeRiceCooker

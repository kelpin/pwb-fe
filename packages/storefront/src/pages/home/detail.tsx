import React from "react"
import { PageProps } from "gatsby"
import { Text } from "../../components/foundations/Text"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import ID from "../../translations/id"
import { Button } from "../../components/Button"
import * as Form from "../../components/Form"
import CssUtils from "css-utils"
import IMAGES from "../../assets/images"

type DataProps = {
  site: {
    buildTime: string
  }
}

const Section__EWarranty = () => {
  // TODO: Change color
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex flex-col w-full items-center justify-between py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${IMAGES.homeEWarranty})` }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none text-white">
          {ID["home.eWarranty.subtitle"]}
        </Text>
        <Text kind="h3" className="font-bold leading-none text-white">
          {ID["home.eWarranty.title"]}
        </Text>
      </div>
    </section>
  )
}

const Accordion = ({ children, title }) => {
  const [showAccordion, toggleShowAccordion] = React.useState(false)

  return (
    <div>
      <div className="w-full border-b py-3 flex justify-between">
        {title}
        <button onClick={() => toggleShowAccordion(!showAccordion)}>
          show
        </button>
      </div>
      {showAccordion && children}
    </div>
  )
}

const EWarranty: React.FC<PageProps<DataProps>> = ({
  data,
  path,
  location,
}) => {
  let selectOptions: Form.SelectOption[] = [
    { value: "Topic 1", label: "Topic 1" },
    { value: "Topic 2", label: "Topic 2" },
  ]
  return (
    <Layout title="E-Warranty" location={location}>
      <SEO title="E-Warranty" />
      <div className="id-breadcrumb"></div>
      <section className="py-5 w-full md:w-10/12 lg:max-w-7xl mb-10 grid grid-cols-2 gap-6">
        <div></div>
        <div className="pl-8 pt-8">
          <div className="">
            <Text kind="h3">SNW 106</Text>
          </div>

          <Accordion title="Product Overview">
            <p>item</p>

            <p>item</p>
            <p>item</p>
            <p>item</p>
            <p>item</p>
          </Accordion>
        </div>
      </section>
    </Layout>
  )
}

export default EWarranty

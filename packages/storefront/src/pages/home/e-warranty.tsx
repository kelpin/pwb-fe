import React from "react"
import { PageProps } from "gatsby"
import { Text } from "../../components/foundations/Text"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import ID from "../../translations/id"
import { Button } from "../../components/Button"
import * as Form from "../../components/Form"
import CssUtils from "css-utils"
import IMAGES from "../../assets/images"

type DataProps = {
  site: {
    buildTime: string
  }
}

const Section__EWarranty = () => {
  // TODO: Change color
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex flex-col w-full items-center justify-between py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${IMAGES.homeEWarranty})` }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none text-white">
          {ID["home.eWarranty.subtitle"]}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-white text-center"
        >
          {ID["home.eWarranty.title"]}
        </Text>
      </div>
    </section>
  )
}

const EWarranty: React.FC<PageProps<DataProps>> = ({
  data,
  path,
  location,
}) => {
  let selectOptions: Form.SelectOption[] = [
    { value: "Topic 1", label: "Topic 1" },
    { value: "Topic 2", label: "Topic 2" },
  ]
  return (
    <Layout title="E-Warranty" location={location}>
      <SEO title="E-Warranty" />
      <Section__EWarranty />

      <section id="form-section" className="w-full flex flex-col items-center">
        <div className="flex flex-col py-5 border-t items-center justify-start w-full md:w-10/12 lg:max-w-7xl mb-10">
          <div className="w-full flex flex-col items-center pt-10">
            <Text className="leading-none">
              {ID["eWarranty.form.subtitle"]}
            </Text>
            <Text kind="h3" className="leading-none text-center mb-4 px-4">
              {ID["eWarranty.form.title"]}
            </Text>
          </div>

          <form className="w-full my-10">
            <div className="w-full grid grid-rows-2 px-4 md:px-0 md:grid-rows-none md:grid-cols-2 gap-6">
              <Form.TextField
                id={"fullname"}
                label={ID["eWarranty.form.name.label"]}
                placeholder={ID["eWarranty.form.name.label"]}
                type="text"
              />
              <Form.TextField
                id={"email"}
                label={ID["eWarranty.form.email.label"]}
                placeholder={ID["eWarranty.form.email.label"]}
                type="email"
              />
              <Form.TextField
                id={"phoneNumber"}
                label={ID["eWarranty.form.phoneNumber.label"]}
                placeholder={ID["eWarranty.form.phoneNumber.label"]}
                type="number"
              />
              <div></div>
              <Form.TextArea
                id={"street-address-1"}
                label={ID["eWarranty.form.streetAddress1.label"]}
                placeholder={ID["eWarranty.form.streetAddress1.label"]}
                type="text"
              />
              <Form.TextArea
                id={"street-address-2"}
                label={ID["eWarranty.form.streetAddress2.label"]}
                placeholder={ID["eWarranty.form.streetAddress2.label"]}
                type="text"
              />
              <Form.TextField
                id={"postal-code"}
                label={ID["eWarranty.form.postalCode.label"]}
                placeholder={ID["eWarranty.form.postalCode.label"]}
                type="number"
              />
              <Form.TextField
                id={"province"}
                label={ID["eWarranty.form.province.label"]}
                placeholder={ID["eWarranty.form.province.label"]}
                type="text"
              />
            </div>
            <hr className="mx-4 md:mx-0 my-10" />

            <div className="w-full grid grid-rows-2 px-4 md:px-0 md:grid-rows-none md:grid-cols-2 gap-6">
              <Form.TextField
                id={"model"}
                label={ID["eWarranty.form.model.label"]}
                placeholder={ID["eWarranty.form.model.label"]}
                type="text"
              />
              <Form.TextField
                id={"other-model"}
                label={ID["eWarranty.form.otherModel.label"]}
                placeholder={ID["eWarranty.form.otherModel.label"]}
                type="number"
              />
              <Form.TextField
                id={"serial-number"}
                label={ID["eWarranty.form.serialNumber.label"]}
                placeholder={ID["eWarranty.form.serialNumber.label"]}
                type="text"
              />
              <Form.TextField
                id={"date-of-purchase"}
                label={ID["eWarranty.form.dateOfPurchase.label"]}
                placeholder={ID["eWarranty.form.dateOfPurchase.label"]}
                type="date"
              />
              <Form.TextField
                id={"type-of-unit"}
                label={ID["eWarranty.form.typeOfUnit.label"]}
                placeholder={ID["eWarranty.form.typeOfUnit.label"]}
                type="text"
              />
            </div>
            <div className="w-full flex justify-center items-center mt-10">
              <Button
                onClick={() => console.log("onsubmit")}
                kind="primary"
                label="Submit"
                className="w-64"
              />
            </div>
          </form>
        </div>
      </section>
    </Layout>
  )
}

export default EWarranty

import React from "react"
import { PageProps } from "gatsby"
import { Text } from "../../components/foundations/Text"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import IMAGES from "../../assets/images"
import ID from "../../translations/id"
import { ButtonLink } from "../../components/Button"
import ROUTES from "../../routes"
import { Carousel } from "../../components/carousel/Carousel"

type DataProps = {
  site: {
    buildTime: string
  }
}

const Section__Intro = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex w-full items-center justify-center py-16",
    spacer: "w-1/12",
  }

  return (
    <section
      style={{ backgroundImage: `url(${IMAGES.airFryerIntro})` }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-center md:items-start">
        <div className="mb-10">
          <Text kind="h3" className="font-bold leading-none text-white mb-4">
            {ID["airFryer.section.intro.title"]}
          </Text>
          <Text className="leading-none text-white max-w-sm">
            {ID["airFryer.section.intro.subtitle"]}
          </Text>
        </div>

        <ButtonLink
          to={ROUTES.home.airFryer.detail.link}
          label="Shop Now"
          kind="white"
        />
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  )
}

const Section__Advantage = () => {
  const tailwindStyles = {
    column:
      "h-screen md:h-screen-80 w-full md:w-1/2 bg-center bg-cover flex flex-col items-center justify-start pt-10",
    heading: "w-2/3 md:w-full text-center",
  }

  return (
    <section>
      <div className="w-full flex flex-col md:flex-row justify-between items-center">
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${IMAGES.airFryerSub_limitless})`,
          }}
        >
          <Text
            kind="h4"
            className={CssUtils.merge([
              tailwindStyles.heading,
              "font-bold leading-none text-white",
            ])}
          >
            {ID["airFryer.section.limitless.title"]}
          </Text>
          <Text className="leading-none text-white text-center w-2/3 md:w-full md:max-w-sm">
            {ID["airFryer.section.limitless.subtitle"]}
          </Text>
        </div>
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${IMAGES.airFryerSub_advance})`,
          }}
        >
          <Text
            kind="h4"
            className={CssUtils.merge([
              tailwindStyles.heading,
              "font-bold leading-none text-black",
            ])}
          >
            {ID["airFryer.section.advancedTech.title"]}
          </Text>
          <Text className="leading-none text-black text-center max-w-sm mb-8">
            {ID["airFryer.section.advancedTech.subtitle"]}
          </Text>
          <img
            className="hidden md:block w-1/3"
            src={IMAGES.airFryerSub_advance_item}
            alt="airfryer-advance-items"
          />
        </div>
      </div>
    </section>
  )
}

const Section__WithoutFat = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen flex flex-col w-full items-center justify-between py-16",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${IMAGES.airFryerWithoutFat})` }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none w-2/3 md:w-full text-center">
          {ID["airFryer.section.eatWithouFat.subtitle"]}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-black text-center"
        >
          {ID["airFryer.section.eatWithouFat.title"]}
        </Text>
      </div>
    </section>
  )
}

interface ProductCardProps {
  image: string
}

const Section__Products = () => {
  const tailwindStyles = {
    container: "flex flex-col w-full items-center justify-between py-16",
  }
  const ProductCard: React.FC<ProductCardProps> = ({ image }) => {
    return (
      <div className="flex flex-col w-72 h-72 justify-center items-center p-2 rounded-lg shadow-sm bg-white hover:shadow-lg mx-4 my-4">
        <img src={image} alt={image} className="h-full" />
      </div>
    )
  }

  return (
    <section className={tailwindStyles.container}>
      <div className="flex flex-col w-full justify-between items-start md:items-center overflow-auto">
        <div className="flex justify-start items-center mb-10 ">
          <ProductCard image={IMAGES.airFryerShowcase__Front__Closed} />
          <ProductCard image={IMAGES.airFryerShowcase__Front__Open} />
          <ProductCard image={IMAGES.airFryerShowcase__Side__Open} />
        </div>
      </div>

      <div className="flex flex-col md:flex-row items-center justify-between mb-10">
        <ButtonLink
          className="mx-2 my-2"
          to={ROUTES.home.airFryer.detail.link}
          label="Shop Now"
          kind="black"
        />
      </div>
    </section>
  )
}

const SanwoHomeAirfryer: React.FC<PageProps<DataProps>> = ({
  data,
  path,
  location,
}) => (
  <Layout title="Sanwoo Home" location={location}>
    <SEO title="Sanwoo Home" />
    <Section__Intro />
    <Section__Advantage />
    <Section__WithoutFat />
    <Section__Products />
  </Layout>
)

export default SanwoHomeAirfryer

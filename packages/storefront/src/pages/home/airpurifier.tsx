import React from "react"
import { PageProps } from "gatsby"
import { Text } from "../../components/foundations/Text"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import IMAGES from "../../assets/images"
import ID from "../../translations/id"
import { ButtonLink } from "../../components/Button"
import ROUTES from "../../routes"

type DataProps = {
  site: {
    buildTime: string
  }
}

const Section__Intro = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    spacer: "w-1/12",
  }

  return (
    <section
      style={{ backgroundImage: `url(${IMAGES.airPurifierIntro})` }}
    >
      <div className={tailwindStyles.spacer}></div>

      <div className="flex-1 flex flex-col items-center md:items-start">
        <div className="mb-10">
          <Text kind="h3" className="font-bold leading-none text-black mb-4">
            {ID["airPurifier.section.intro.title"]}
          </Text>
          <Text className="leading-none text-black max-w-sm">
            {ID["airPurifier.section.intro.subtitle"]}
          </Text>
        </div>

        <ButtonLink
          to={ROUTES.home.airPurifier.detail.link}
          label="Shop Now"
          kind="black"
        />
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  )
}

const Section__Advantage = () => {
  const tailwindStyles = {
    column:
      "h-screen md:h-screen-80 w-full md:w-1/2 bg-center bg-cover flex flex-col items-center justify-start pt-10",
    heading: "w-2/3 md:w-full text-center",
  }

  return (
    <section>
      <div className="w-full flex flex-col md:flex-row justify-between items-center">
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${IMAGES.airPurifierSub_3in1})`,
          }}
        >
          <Text
            kind="h4"
            className={CssUtils.merge([
              tailwindStyles.heading,
              "font-bold leading-none text-white mb-2 md:mb-0",
            ])}
          >
            {ID["airPurifier.section.3in1.title"]}
          </Text>
          <Text className="leading-none text-white text-center w-2/3 md:w-full md:max-w-sm">
            {ID["airPurifier.section.3in1.subtitle"]}
          </Text>
        </div>
        <div
          className={tailwindStyles.column}
          style={{
            backgroundImage: `url(${IMAGES.airPurifierSub_removesOdor})`,
          }}
        >
          <Text
            kind="h4"
            className={CssUtils.merge([
              tailwindStyles.heading,
              "font-bold leading-none text-black",
            ])}
          >
            {ID["airPurifier.section.removeOdor.title"]}
          </Text>
          <Text className="leading-none text-black w-2/3 md:w-full text-center max-w-sm mb-8">
            {ID["airPurifier.section.removeOdor.subtitle"]}
          </Text>
        </div>
      </div>
    </section>
  )
}

const Section__BreatheEasy = () => {
  const tailwindStyles = {
    container:
      "h-screen bg-cover bg-center flex flex-col w-full items-center justify-between py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${IMAGES.airPurifierBreatheEasy})` }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none w-2/3 md:w-full text-center">
          {ID["airPurifier.section.breatheEasy.title"]}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-black text-center"
        >
          {ID["airPurifier.section.breatheEasy.subtitle"]}
        </Text>
      </div>
    </section>
  )
}

interface ProductCardProps {
  image: string
}

const Section__Producs = () => {
  const tailwindStyles = {
    container: "flex flex-col w-full items-center justify-between py-16",
  }
  const ProductCard: React.FC<ProductCardProps> = ({ image }) => {
    return (
      <div className="flex flex-col w-72 h-72 justify-center items-center p-2 rounded-lg shadow-sm bg-white hover:shadow-lg mx-4 my-4">
        <img src={image} alt={image} className="h-full" />
      </div>
    )
  }

  return (
    <section className={tailwindStyles.container}>
      <div className="flex flex-col w-full justify-between items-start md:items-center overflow-auto">
        <div className="flex justify-start items-center mb-10 ">
          <ProductCard image={IMAGES.airPurifierShowcase__Front} />
          <ProductCard image={IMAGES.airPurifierShowcase__Back} />
          <ProductCard image={IMAGES.airPurifierShowcase__Side} />
        </div>
      </div>

      <div className="flex flex-col md:flex-row items-center justify-between mb-10">
        <ButtonLink
          className="mx-2 my-2"
          to={ROUTES.home.airPurifier.detail.link}
          label="Shop Now"
          kind="black"
        />
      </div>
    </section>
  )
}

const SanwooAirPurifier: React.FC<PageProps<DataProps>> = ({
  data,
  path,
  location,
}) => (
  <Layout title="Sanwoo Air Purifier" location={location}>
    <SEO title="Sanwoo Air Purifier" />
    <Section__Intro />
    <Section__Advantage />
    <Section__BreatheEasy />
    <Section__Producs />
  </Layout>
)

export default SanwooAirPurifier

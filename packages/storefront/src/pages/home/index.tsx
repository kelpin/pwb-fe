import React from "react"
import { PageProps, Link } from "gatsby"
import { Text } from "../../components/foundations/Text"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import IMAGES from "../../assets/images"
import ID from "../../translations/id"
import CssUtils from "css-utils"
import { ButtonLink } from "../../components/Button"
import { Carousel } from "../../components/carousel/Carousel"
import ROUTES from "../../routes"

type DataProps = {
  site: {
    buildTime: string
  }
}

const CarouselItem__TopBannerAugust = () => {
  const tailwindStyles = {
    container:
      "bg-cover h-screen md:h-screen-80 flex flex-row w-full items-center justify-center py-16",
  }
  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${IMAGES.topBannerAugust})`,
        backgroundPosition: "top-left",
      }}
    ></section>
  )
}
const CarouselItem__RiceCooker__Front = () => {
  const tailwindStyles = {
    container:
      "bg-cover h-screen md:h-screen-80 flex flex-row w-full items-center justify-center py-16",
  }
  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${IMAGES.homeBannerNew})`,
        backgroundPosition: "top-left",
      }}
    >
      {/* <div className="flex justify-center items-center pt-10 pr-2"> */}
      {/*   <img className="w-full md:w-7/12" src={IMAGES.riceCooker__FRONT} /> */}
      {/* </div> */}
      {/* <div className="flex flex-col items-center mb-16"> */}
      {/*   <Text className="leading-none text-primary "> */}
      {/*     Low Sugar Rice Cooker */}
      {/*   </Text> */}
      {/*   <Text */}
      {/*     kind="h4" */}
      {/*     className="font-bold leading-none text-primary text-center mb-10" */}
      {/*   > */}
      {/*     Sanwoo Multifunction */}
      {/*   </Text> */}

      {/*   <Text className="leading-none text-primary strikethrough"> */}
      {/*     Rp1.800.000,00 */}
      {/*   </Text> */}
      {/*   <Text */}
      {/*     kind="h4" */}
      {/*     className="font-bold leading-none text-primary text-center mb-10" */}
      {/*   > */}
      {/*     Rp849.000,00 */}
      {/*   </Text> */}

      {/*   <div className={LocalTailwind.buttonContainer}> */}
      {/*     <ButtonLink */}
      {/*       className={LocalTailwind.button} */}
      {/*       to="https://tokopedia.link/qKvmAZPIMgb" */}
      {/*       label="Buy Now" */}
      {/*       kind="primary" */}
      {/*     /> */}
      {/*   </div> */}
      {/* </div> */}
    </section>
  )
}
const CarouselItem__RiceCooker__Top = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex flex-row w-full items-center justify-center py-16",
  }
  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${IMAGES.bgRiceCooker__TOP})` }}
    >
      <div className="flex justify-center items-center pt-10 pr-2">
        <img className="w-full md:w-7/12" src={IMAGES.riceCooker__TOP} />
      </div>
      <div className="flex flex-col items-center mb-16">
        <Text className="leading-none text-primary ">
          Low Sugar Rice Cooker
        </Text>
        <Text
          kind="h4"
          className="font-bold leading-none text-primary text-center mb-10"
        >
          Sanwoo Multifunction
        </Text>

        <Text className="leading-none text-primary strikethrough">
          Rp1.800.000,00
        </Text>
        <Text
          kind="h4"
          className="font-bold leading-none text-primary text-center mb-10"
        >
          Rp849.000,00
        </Text>

        <div className={LocalTailwind.buttonContainer}>
          <ButtonLink
            className={LocalTailwind.button}
            to="https://tokopedia.link/qKvmAZPIMgb"
            label="Buy Now"
            kind="primary"
          />
        </div>
      </div>
    </section>
  )
}

const CarouselItem__Video__Airpurifier = () => {
  const tailwindStyles = {
    container:
      "bg-center h-screen-80 flex flex-col w-full items-center justify-center",
  }

  return (
    <section className={CssUtils.merge([tailwindStyles.container, ""])}>
      <iframe
        style={{ width: "100vw", height: "100vh" }}
        width="560"
        height="315"
        src="https://www.youtube.com/embed/HpN4L-wKUxE?controls=0&autoplay=1&mute=1&playlist=HpN4L-wKUxE&loop=1&modestbranding=0&disablekb=1"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      ></iframe>
    </section>
  )
}

const CarouselItem__Video__Airfryer = () => {
  const tailwindStyles = {
    container:
      "bg-center h-screen-80 flex flex-col w-full items-center justify-center",
  }

  return (
    <section className={CssUtils.merge([tailwindStyles.container, ""])}>
      <iframe
        style={{ width: "100vw", height: "100vh" }}
        width="560"
        height="315"
        src="https://www.youtube.com/embed/_i6dO3qn0NQ?controls=0&autoplay=1&mute=1&playlist=_i6dO3qn0NQ&loop=1&modestbranding=0&disablekb=1"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      ></iframe>
    </section>
  )
}

const LocalTailwind = {
  buttonContainer: "flex flex-col md:flex-row items-center justify-between",
  button: "mx-2 my-2",
}

const CarouselItem__Simplicity = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen md:h-screen-80 flex flex-col w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${IMAGES.homeJumbotronSimplicity})` }}
    >
      <div className="flex flex-col items-center mb-16">
        <Text
          kind="h3"
          className="font-bold leading-none text-white text-center"
        >
          {ID["home.jumbotron.simplicity.title"]}
        </Text>
        <Text className="leading-none text-white">
          {ID["home.jumbotron.simplicity.desc"]}
        </Text>
      </div>
      <div className={LocalTailwind.buttonContainer}>
        <ButtonLink
          className={LocalTailwind.button}
          to={ROUTES.home.airFryer.detail.link}
          label="Shop Now"
          kind="white"
        />
        <ButtonLink
          className={LocalTailwind.button}
          to={ROUTES.home.airFryer.link}
          label="More Info"
          kind="text-white"
        />
      </div>
    </section>
  )
}
const Section__AirFyer = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen flex flex-col w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    image: "",
    title: "",
    subtitle: "",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${IMAGES.homeAirFyer})` }}
    >
      <div className="flex flex-col items-center mb-16">
        <Text className="leading-none text-white">
          {ID["home.promo.subtitle"]}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-white text-center mb-2"
        >
          {ID["home.airFryer.title"]}
        </Text>
        <Text
          kind="body"
          className="w-2/3 md:w-full leading-none text-white text-center"
        >
          {ID["home.airFryer.desc"]}
        </Text>
      </div>
      <div className={LocalTailwind.buttonContainer}>
        <ButtonLink
          className={LocalTailwind.button}
          to={ROUTES.home.airFryer.detail.link}
          label="Shop Now"
          kind="white"
        />
        <ButtonLink
          className={LocalTailwind.button}
          to={ROUTES.home.airFryer.link}
          label="More Info"
          kind="text-white"
        />
      </div>
    </section>
  )
}

const Section__AirPurifier = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex flex-col w-full items-center justify-between py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${IMAGES.homeAirPurifier})` }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none">{ID["home.airPurifier.subtitle"]}</Text>
        <Text kind="h3" className="font-bold leading-none text-black">
          {ID["home.airPurifier.title"]}
        </Text>
      </div>
      <div className={LocalTailwind.buttonContainer}>
        <ButtonLink
          className={LocalTailwind.button}
          to={ROUTES.home.airPurifier.detail.link}
          label="Shop Now"
          kind="black"
        />
        <ButtonLink
          className="mx-2"
          to={ROUTES.home.airPurifier.link}
          label="More Info"
          kind="text-black"
        />
      </div>
    </section>
  )
}

const Section__AirMultiCooker = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex flex-col w-full items-center justify-between py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${IMAGES.homeMultiCooker})` }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none">Sanwoo Multi Cooker</Text>
        <Text kind="h3" className="font-bold leading-none text-black">
          14 In 1 Multi Cooker
        </Text>
      </div>
      <div className={LocalTailwind.buttonContainer}>
        <ButtonLink
          className={LocalTailwind.button}
          to={ROUTES.home.multicooker.link + "/detail"}
          label="Shop Now"
          kind="white"
        />
        <ButtonLink
          className="mx-2"
          to={ROUTES.home.multicooker.link}
          label="More Info"
          kind="text-black"
        />
      </div>
    </section>
  )
}

const Section__AirFryer55 = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    spacer: "w-1/12",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${IMAGES.airfryer52NewBiggerWithGradient})`,
      }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-start ">
        <div className="mb-10">
          <Text className="leading-none  max-w-sm">{"It's coming"}</Text>
          <Text kind="h3" className="font-bold leading-none  mb-4">
            {"Sanwoo Air Fryer 5.2L"}
          </Text>
          <Text className="leading-none  max-w-sm">
            {"Bigger, Better, Faster"}
          </Text>
        </div>

        <div className={LocalTailwind.buttonContainer}>
          <ButtonLink
            className={LocalTailwind.button}
            to={ROUTES.home.airFryer52.link + "/detail"}
            label="Shop Now"
            kind="black"
          />
          <ButtonLink
            className="mx-2"
            to={ROUTES.home.airFryer52.link}
            label="More Info"
            kind="text-black"
          />
        </div>
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  )
}

const Section__RiceCooker = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex w-full items-center justify-center py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
    spacer: "w-1/12",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{
        backgroundImage: `url(${IMAGES.riceCookerIntro})`,
      }}
    >
      <div className={tailwindStyles.spacer}></div>
      <div className="flex-1 flex flex-col items-start ">
        <div className="mb-10">
          <Text className="leading-none text-white max-w-sm">
            {"Rice Cooker"}
          </Text>
          <Text kind="h3" className="font-bold leading-none text-white mb-4">
            {"Sanwoo Multifunction"}
          </Text>
          <Text className="leading-none text-white max-w-sm">
            {"Low Sugar Rice Cooker"}
          </Text>
        </div>

        <div className={LocalTailwind.buttonContainer}>
          <ButtonLink
            className={LocalTailwind.button}
            to={ROUTES.home.ricecooker.link + "/detail"}
            label="Shop Now"
            kind="white"
          />
          <ButtonLink
            className="mx-2"
            to={ROUTES.home.ricecooker.link}
            label="More Info"
            kind="text-white"
          />
        </div>
      </div>
      <div className={tailwindStyles.spacer}></div>
    </section>
  )
}

const Section__EWarranty = () => {
  const tailwindStyles = {
    container:
      "bg-cover bg-center h-screen-80 flex flex-col w-full items-center justify-between py-16",
    textWrapper: "flex-row items-center",
    textContainer: "flex flex-col justify-start items-start w-2/3",
  }

  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${IMAGES.homeEWarranty})` }}
    >
      <div className="flex flex-col items-center">
        <Text className="leading-none text-white text-center">
          {ID["home.eWarranty.subtitle"]}
        </Text>
        <Text
          kind="h3"
          className="font-bold leading-none text-white text-center"
        >
          {ID["home.eWarranty.title"]}
        </Text>
      </div>
      <div className="flex items-center justify-between">
        <ButtonLink
          className="mx-2"
          to={ROUTES.home.eWarranty.link}
          label={ID["home.eWarranty.ctaButton"]}
          kind="white"
        />
      </div>
    </section>
  )
}

interface ProductCardProps {
  image: string
  desc: string
  link: string
}

const Section__HotPromo = () => {
  const tailwindStyles = {
    container: "flex flex-col w-full items-center justify-between py-16",
  }
  const ProductCard: React.FC<ProductCardProps> = ({ image, desc, link }) => {
    return (
      <Link to={link}>
        <div className="flex flex-col justify-center p-2 rounded-lg shadow-md bg-white hover:shadow-lg mx-4 my-4 cursor-pointer">
          <div className="flex flex-col items-center justify-center">
            <img
              src={image}
              alt={image}
              className="h-72 w-72"
              style={{ width: 100, height: 100, padding: 10 }}
            />
          </div>
          <Text kind="body2" className="px-2 pb-4 text-center">
            {desc}
          </Text>
        </div>
      </Link>
    )
  }

  return (
    <section className={tailwindStyles.container}>
      <div className="flex flex-col items-center">
        <Text kind="h3" className="font-bold leading-none">
          {ID["home.promo.title"]}
        </Text>
        <Text kind="h6" className="leading-none">
          {ID["home.promo.subtitle"]}
        </Text>
      </div>
      {/* <div */}
      {/*   className="w-full px-4 md:w-2/3 my-10 flex py-4" */}
      {/*   style={{ backgroundImage: `url(${IMAGES.bgRiceCooker__TOP})` }} */}
      {/* > */}
      {/*   <div className="flex justify-center items-center pt-10 pr-2"> */}
      {/*     <img className="w-full md:w-7/12" src={IMAGES.riceCooker__TOP} /> */}
      {/*   </div> */}
      {/*   <div className="flex flex-col items-center mb-16"> */}
      {/*     <Text className="leading-none text-primary "> */}
      {/*       Low Sugar Rice Cooker */}
      {/*     </Text> */}
      {/*     <Text */}
      {/*       kind="h4" */}
      {/*       className="font-bold leading-none text-primary text-center mb-10" */}
      {/*     > */}
      {/*       Sanwoo Multifunction */}
      {/*     </Text> */}

      {/*     <Text className="leading-none text-primary strikethrough"> */}
      {/*       Rp1.800.000,00 */}
      {/*     </Text> */}
      {/*     <Text */}
      {/*       kind="h4" */}
      {/*       className="font-bold leading-none text-primary text-center mb-10" */}
      {/*     > */}
      {/*       Rp849.000,00 */}
      {/*     </Text> */}

      {/*     <div className={LocalTailwind.buttonContainer}> */}
      {/*       <ButtonLink */}
      {/*         className={LocalTailwind.button} */}
      {/*         to="https://tokopedia.link/qKvmAZPIMgb" */}
      {/*         label="Buy Now" */}
      {/*         kind="primary" */}
      {/*       /> */}
      {/*     </div> */}
      {/*   </div> */}
      {/* </div> */}
      <div
        className="w-full px-4 md:w-2/3 my-10 flex py-4"
        style={{
          backgroundImage: `url(${IMAGES.promoAugust})`,
          height: 300,
          backgroundSize: "contain",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
        }}
      ></div>
      <div className="flex flex-col md:flex-row justify-center items-center">
        <ProductCard
          image={IMAGES.airFryerShowcase__Front__Closed}
          desc="Air Fyer"
          link={ROUTES.home.airFryer.link}
        />
        <ProductCard
          image={IMAGES.airPurifierShowcase__Side}
          desc="Air Purifier"
          link={ROUTES.home.airPurifier.link}
        />
        <ProductCard
          image={IMAGES["Sanwoo Air Fryer 52"][0]}
          desc="Air Fryer 5.2 L"
          link={ROUTES.home.airFryer52.link}
        />
        <ProductCard
          image={IMAGES["Sanwoo 14-in-1 Multi Cooker & Air Fryer"][0]}
          desc="Multi cooker"
          link={ROUTES.home.multicooker.link}
        />
        <ProductCard
          image={IMAGES["Sanwoo Multifunction Low Sugar Rice Cooker"][0]}
          desc="Rice Cooker"
          link={ROUTES.home.ricecooker.link}
        />
      </div>
    </section>
  )
}

const SanwooHome: React.FC<PageProps<DataProps>> = ({
  data,
  path,
  location,
}) => (
  <Layout title="Sanwoo Home" location={location}>
    <SEO title="Sanwoo Home" />

    <Carousel arrowStyle="white">
      <CarouselItem__TopBannerAugust />
      <CarouselItem__RiceCooker__Front />
      <CarouselItem__Simplicity />
      <CarouselItem__Video__Airpurifier />
      <CarouselItem__Video__Airfryer />
    </Carousel>

    <Section__AirFyer />
    <Section__AirPurifier />
    <Section__AirFryer55 />
    <Section__AirMultiCooker />
    <Section__RiceCooker />

    <Section__EWarranty />
    <Section__HotPromo />
  </Layout>
)

export default SanwooHome

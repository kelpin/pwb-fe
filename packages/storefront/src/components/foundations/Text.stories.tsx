import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Text } from "./Text";

export default {
  title: "Foundation/Text",
  component: Text,
} as Meta;

export const All = () => (
  <div className="flex flex-col">
    <Text kind="h1">Heading 1</Text>

    <Text kind="h2">Heading 2</Text>

    <Text kind="h3">Heading 3</Text>

    <Text kind="h4">Heading 4</Text>

    <Text kind="h5">Heading 5</Text>

    <Text kind="h6">Heading 6</Text>

    <Text kind="subtitle">Subtitle</Text>

    <Text kind="subtitle2">Subtitle 2</Text>

    <Text kind="button">Button</Text>

    <Text kind="button2">Button 2</Text>

    <Text kind="caption">Caption</Text>

    <Text kind="body">Body</Text>

    <Text kind="body2">Body 2</Text>
  </div>
);

import React from "react"
import { Link } from "gatsby"
import { Text } from "./foundations/Text"
import CssUtils from "css-utils"

const Styles = {
  animation: "transition ease-in-out duration-200",
  buttonLinkPrimary: "bg-primary px-6 py-3 hover:opacity-75 text-center",
  buttonLinkSecondary: "",
  buttonLinkText: "px-3 py-3 hover:opacity-75",
  buttonLinkBlack: "bg-black px-6 py-3 hover:opacity-75 text-center",
  buttonLinkWhite: "bg-white px-6 py-3 hover:opacity-75 text-center",
  buttonLinkWhitePurple: "bg-white px-6 py-3 border-primary border",
  textPrimary: "text-primary text-center",
  textWhite: "text-white text-center",
  textBlack: "text-black",
  buttonLinkSecondaryWhite: "px-6 py-3 border border-white bg-transparent",
}

type ButtonLinkProps = {
  to: string
  label: string
  kind:
    | "primary"
    | "secondary"
    | "text"
    | "secondary-white"
    | "white"
    | "text-white"
    | "black"
    | "text-black"
    | "white-purple"
  display?: "block" | "inline" | "inline-block" | "flex"
  className?: string
  rounded?: boolean
  customLabelStyle?: string
}

type ButtonProps = {
  label: string
  kind:
    | "primary"
    | "secondary"
    | "text"
    | "secondary-white"
    | "white"
    | "text-white"
    | "black"
    | "text-black"
    | "white-purple"
  display?: "block" | "inline" | "inline-block" | "flex"
  className?: string
  rounded?: boolean
  onClick: any
}

const makeButtonStyle = (kind: ButtonLinkProps["kind"]) => {
  switch (kind) {
    case "text":
    case "text-black":
    case "text-white":
      return Styles.buttonLinkText

    case "secondary":
      return Styles.buttonLinkSecondary

    case "primary":
      return Styles.buttonLinkPrimary

    case "secondary-white":
      return Styles.buttonLinkSecondaryWhite

    case "white":
      return Styles.buttonLinkWhite

    case "black":
      return Styles.buttonLinkBlack

    case "white-purple":
      return Styles.buttonLinkWhitePurple
  }
}

const makeLabelStyle = (kind: ButtonLinkProps["kind"]) => {
  switch (kind) {
    case "text":
    case "secondary":
    case "white-purple":
      return Styles.textPrimary

    case "black":
    case "text-white":
    case "secondary-white":
    case "primary":
      return Styles.textWhite

    case "text-black":
    case "white":
      return Styles.textBlack
  }
}

export const ButtonLink: React.FC<ButtonLinkProps> = ({
  to,
  label,
  kind = "primary",
  display = "inline-block",
  className = "",
  rounded = true,
  customLabelStyle = "",
}) => {
  let buttonStyle = makeButtonStyle(kind)
  let labelStyle = makeLabelStyle(kind)
  let buttonRounded = rounded ? "rounded-full" : ""

  return (
    <Link
      to={to}
      className={CssUtils.merge([
        className,
        buttonStyle,
        Styles.animation,
        display,
        buttonRounded,
      ])}
    >
      <Text
        kind="button"
        className={CssUtils.merge([labelStyle, customLabelStyle])}
      >
        {label}
      </Text>
    </Link>
  )
}

export const Button: React.FC<ButtonProps> = ({
  label,
  kind = "primary",
  display = "inline-block",
  className = "",
  rounded = true,
  onClick,
}) => {
  let buttonStyle = makeButtonStyle(kind)
  let labelStyle = makeLabelStyle(kind)
  let buttonRounded = rounded ? "rounded-full" : ""

  return (
    <div
      onClick={onClick}
      className={CssUtils.merge([
        className,
        buttonStyle,
        Styles.animation,
        display,
        buttonRounded,
        "cursor-pointer",
      ])}
    >
      <Text kind="button" className={labelStyle}>
        {label}
      </Text>
    </div>
  )
}

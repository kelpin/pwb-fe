import React from "react"
import "./Form.css"
import CssUtils from "css-utils"

type TextFieldProps = {
  id: string
  label: string
  placeholder: string
  type: string
}

type PhoneFieldProps = {
  id: string
  label: string
  placeholder: string
}

export type SelectOption = {
  value: string
  label: string
}
type SelectProps = {
  id: string
  label: string
  placeholder: string
  options: Array<{ value: string; label: string }>
}

export const TextField: React.FC<TextFieldProps> = ({
  id,
  label,
  placeholder,
  type,
}) => {
  const styles = {
    label: "block text-gray-700 text-sm font-bold mb-2",
    input:
      "appearance-none border rounded w-full py-5 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline",
  }
  return (
    <div>
      <label className={styles.label}>{label}</label>
      <input
        className={styles.input}
        id={id}
        type={type}
        placeholder={placeholder}
      />
    </div>
  )
}

export const TextArea: React.FC<TextFieldProps> = ({
  id,
  label,
  placeholder,
  type,
}) => {
  const styles = {
    label: "block text-gray-700 text-sm font-bold mb-2",
    input:
      "appearance-none border rounded w-full py-5 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline",
  }
  return (
    <div>
      <label className={styles.label}>{label}</label>
      <textarea
        className={styles.input}
        id={id}
        placeholder={placeholder}
        rows={4}
      />
    </div>
  )
}

export const PhoneField: React.FC<PhoneFieldProps> = ({
  id,
  label,
  placeholder,
}) => {
  const styles = {
    label: "block text-gray-700 text-sm font-bold mb-2",
    input:
      "appearance-none border rounded w-full py-5 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline",
  }
  return (
    <div>
      <label className={styles.label}>{label}</label>
      <input
        className={CssUtils.merge(["form-number", styles.input])}
        id={id}
        type="number"
        placeholder={placeholder}
      />
    </div>
  )
}

export const Select: React.FC<SelectProps> = ({
  id,
  label,
  placeholder,
  options,
}) => {
  const styles = {
    label: "block text-gray-700 text-sm font-bold mb-2",
    input:
      "appearance-none border rounded w-full py-5 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline",
  }
  return (
    <div>
      <label className={styles.label}>{label}</label>

      <div className="relative">
        <select
          className={CssUtils.merge(["block form-number", styles.input])}
          id={id}
          placeholder={placeholder}
        >
          <option disabled selected>
            Select Topic *
          </option>
          {options.map(o => (
            <option value={o.value}>{o.label}</option>
          ))}
        </select>
        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
          <svg
            className="fill-current h-4 w-4"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
          >
            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
          </svg>
        </div>
      </div>
    </div>
  )
}

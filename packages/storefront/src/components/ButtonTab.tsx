import React from "react"
import CssUtils from "css-utils"

export interface ButtonTabProps {
  label: string
  onClick: any // should be function
  active: boolean
}

export const ButtonTab: React.FC<ButtonTabProps> = ({
  label,
  onClick,
  active,
}) => {
  let buttonBorder = active ? "border-primary" : "border-gray"
  return (
    <button
      className={CssUtils.merge([
        "flex-1 py-5 border-b-2 focus:outline-none",
        buttonBorder,
      ])}
      style={{ minWidth: "125px" }}
      onClick={onClick}
    >
      {label}
    </button>
  )
}

export default ButtonTab

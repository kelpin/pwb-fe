import React from "react"
import { Link } from "gatsby"
import { Navigator } from "./menus"
import Footer from "../components/Footer"
import WhatsAppButton from "../components/WhatsappButton"

const Layout = ({ location, title, children }) => {
  const rootPath = `${__PATH_PREFIX__}/`
  const isRootPath = location.pathname === rootPath

  return (
    <div
      className="global-wrapper"
      style={{
        background:
          "linear-gradient(180deg, #FEFEFF 50.44%, rgba(245, 245, 255, 0.4) 70.61%, rgba(245, 245, 255, 0.65) 73.76%, #F5F5FF 100%)",
      }}
      data-is-root-path={isRootPath}
    >
      <Navigator location={location} />
      <main>{children}</main>

      <WhatsAppButton />
      <Footer location={location} />
    </div>
  )
}

export default Layout

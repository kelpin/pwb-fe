import React, { useState } from "react"
import CarouselButtonLeft from "../../assets/icons/carouselButtonLeft.svg"
import CarouselButtonLeftWhite from "../../assets/icons/carouselButtonLeftWhite.svg"
// @ts-ignore
import Slider from "react-slick"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import "./Carousel.css"
import CssUtils from "css-utils"

export interface Carousel {
  containerStyle?: string
  children: React.ReactElement[]
  arrowStyle: "white" | "purple"
}

const tailwindStyles = {
  container: "md:h-screen-80 w-full ",
  imageContainer: "flex-col justify-center items-center",
  textWrapper: "flex-row items-center",
  textContainer: "flex flex-col justify-start items-start w-2/3",
  image: "",
  title: "",
  subtitle: "",
}

const sliderSettings = {
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoPlay: true,
  autoplaySpeed: 200,
  dots: true,
}
export const Carousel: React.FC<Carousel> = ({
  containerStyle,
  children,
  arrowStyle,
}) => {
  const [slider, setSlider] = useState(null)
  // @ts-ignore
  const onNext = () => slider.slickNext()
  // @ts-ignore
  const onPrevious = () => slider.slickPrev()

  return (
    <section
      className={CssUtils.merge([
        containerStyle ? containerStyle : "h-screen ",
        tailwindStyles.container,
      ])}
    >
      <div
        aria-label="previous-button"
        className="hidden md:absolute top-0 bottom-0 w-1/12 md:flex items-center justify-center z-50"
      >
        <button onClick={() => onPrevious()}>
          {arrowStyle === "white" ? (
            <CarouselButtonLeftWhite className="w-10 h-10 hover:opacity-50" />
          ) : (
            <CarouselButtonLeft className="w-10 h-10 hover:opacity-50" />
          )}
        </button>
      </div>
      <Slider
        ref={
          // @ts-ignore
          slider => setSlider(slider)
        }
        {...sliderSettings}
        arrows={false}
      >
        {children}
      </Slider>
      <div
        aria-label="next-button"
        className="hidden md:absolute right-0 top-0 bottom-0 w-1/12 md:flex items-center justify-center transform rotate-180 z-50"
      >
        <button onClick={() => onNext()}>
          {arrowStyle === "white" ? (
            <CarouselButtonLeftWhite className="w-10 h-10 hover:opacity-50" />
          ) : (
            <CarouselButtonLeft className="w-10 h-10 hover:opacity-50" />
          )}
        </button>
      </div>
    </section>
  )
}

import React from "react"
import CssUtils from "css-utils"
import { Text } from "../foundations/Text"
import { ButtonLink } from "../Button"

export interface CarouselItem {
  imageBackground: string
  title: React.ReactText | string
  subtitle: React.ReactText | string
  buttonLink: string
  contentAlign: "center" | "left"
}

const tailwindStyles = {
  container:
    "bg-cover bg-center h-screen-80 w-full pt-5 flex items-center justify-between",
  imageContainer: "flex-col justify-center items-center",
  textWrapper: "flex-row items-center",
  textContainer:
    "flex flex-col justify-center col-span-2 md:w-10/12 lgmax-w-7xl",
  image: "",
  title: "",
  subtitle: "",
}

let makeContentAlign = (align: CarouselItem["contentAlign"]) => {
  switch (align) {
    case "center":
      return "items-center text-center"
    case "left":
      return "items-start"
  }
}
let makeTextAlign = (align: CarouselItem["contentAlign"]) => {
  switch (align) {
    case "center":
      return "text-center"
    case "left":
      return "text-left"
  }
}

export const CarouselItem: React.FC<CarouselItem> = ({
  title,
  subtitle,
  buttonLink,
  imageBackground,
  contentAlign,
}) => {
  return (
    <div
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${imageBackground})` }}
    >
      <div className="w-1/12"></div>
      <div
        className={CssUtils.merge([
          tailwindStyles.textContainer,
          makeContentAlign(contentAlign),
        ])}
      >
        <Text
          kind="h3"
          className={CssUtils.merge([
            "font-bold leading-none text-primary max-w-2xl",
            makeTextAlign(contentAlign),
          ])}
        >
          {title}
        </Text>
        <Text
          className={CssUtils.merge([
            "leading-none my-4 max-w-2xl",
            makeTextAlign(contentAlign),
          ])}
        >
          {subtitle}
        </Text>
        {buttonLink !== "" && (
          <ButtonLink kind="primary" to={buttonLink} label="Learn More" />
        )}
      </div>
      <div className="w-1/12"></div>
    </div>
  )
}

export default CarouselItem

import React from "react"
import { Text } from "../components/foundations/Text"
import IMAGES from "../assets/images"

const ButtonImage = ({ image, link, disabled }) => {
  let calculatedLink = disabled ? "" : link
  let calculatedStyles = disabled ? "opacity-75 cursor-not-allowed" : ""
  let calculatedTarget = disabled ? "" : "_blank"

  if (disabled) {
    return (
      <div
        className={"flex items-center justify-center" + " " + calculatedStyles}
      >
        <div className="rounded h-full rounded-full w-56 shadow px-16 py-3 text-center bg-white">
          <img src={image} className=" h-full w-full object-contain " />
        </div>
      </div>
    )
  } else {
    return (
      <a
        href={calculatedLink}
        target={calculatedTarget}
        className={"flex items-center justify-center" + " " + calculatedStyles}
      >
        <div className="rounded h-full rounded-full w-56 shadow px-16 py-3 text-center bg-white">
          <img src={image} className=" h-full w-full object-contain " />
        </div>
      </a>
    )
  }
}
const BuyModal = ({
  onClose,
  disableToko = false,
  disableBlibli = false,
  disableShopee = false,
  disableJd = false,
}) => {
  return (
    <div
      className="fixed w-full h-screen z-50 flex justify-center items-center"
      style={{ background: "rgba(0,0,0,0.5)" }}
    >
      <div
        className="relative mx-4 px-6 py-6 md:py-0 md:px-0 h-screen-80 md:h-screen-50 md:w-2/3  rounded-xl flex flex-col justify-center items-center"
        style={{
          background:
            "linear-gradient(180deg, #FEFEFF 20%, rgb(245, 245, 255) 55%,  #E9E9FF 100%)",
        }}
      >
        <div
          className="absolute my-10 mr-10 top-0 right-0 cursor-pointer"
          onClick={onClose}
        >
          x
        </div>
        <Text kind="h3" className="text-h5 md:text-h3 text-primary mb-10">
          Buy Now
        </Text>
        <div className="w-full md:w-2/3 grid grid-rows-4 md:grid-rows-none md:grid-cols-2 gap-10  overflow-y-auto">
          <ButtonImage
            disabled={disableToko}
            image={IMAGES.buy.tokopedia}
            link="https://www.tokopedia.com/sanwooelectronic"
          />
          <ButtonImage
            disabled={disableBlibli}
            image={IMAGES.buy.blibli}
            link="https://www.blibli.com/merchant/sanwoo-electronics-official-store/SAE-70020"
          />
          <ButtonImage
            disabled={disableShopee}
            image={IMAGES.buy.shopee}
            link="https://shopee.co.id/sanwooelectronics"
          />
          <ButtonImage
            disabled={disableJd}
            image={IMAGES.buy.jdId}
            link="https://www.jd.id/shop/Sanwoo-Electronics_53201.html"
          />
        </div>
      </div>
    </div>
  )
}

export default BuyModal

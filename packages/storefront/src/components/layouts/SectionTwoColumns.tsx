import React from "react"
import CssUtils from "../../utils/Css"

export interface SectionTwoColumnsProps {
  image: string
  children: any | React.ReactElement
  className?: string
  imagePosition: "left" | "right"
  wrapperStyles?: string
  imageContainerStyles?: string
  textWrapperStyles?: string
  textContainerStyles?: string
  imageAlt?: string
}

const tailwindStyles = {
  wrapper: "md:h-screen-80  flex w-full justify-center  p-4 md:p-0",
  container: "flex flex-1 ",
  contentContainer:
    "w-full md:w-10/12 lg:max-w-7xl flex justify-between items-center",
  imageContainer: "flex-col justify-center items-center ",
  textWrapper: "flex-row items-center",
  textContainer: "flex flex-col justify-start items-start w-full md:w-2/3",
  image: "",
  title: "",
  subtitle: "",
}

export const SectionTwoColumns: React.FC<SectionTwoColumnsProps> = ({
  image,
  imagePosition = "left",
  wrapperStyles = "",
  imageContainerStyles = "",
  textContainerStyles = "",
  textWrapperStyles = "",
  imageAlt,
  children,
}) => {
  const layoutStyles =
    imagePosition === "left"
      ? "flex-col md:mb-0 md:flex-row"
      : "flex-col-reverse md:flex-row-reverse"
  const containerTextLayout =
    imagePosition === "left"
      ? "justify-start mt-6 md:mt-0 md:pr-12"
      : "justify-end mb-6 md:mb-0 md:px-12"
  return (
    <>
      <section
        className={CssUtils.merge([tailwindStyles.wrapper, wrapperStyles])}
      >
        <div
          className={CssUtils.merge([
            layoutStyles,
            tailwindStyles.contentContainer,
          ])}
        >
          <div
            className={CssUtils.merge([
              tailwindStyles.container,
              tailwindStyles.imageContainer,
              imageContainerStyles,
            ])}
          >
            <img
              className="w-full md:w-9/12"
              src={image}
              alt={imageAlt ? imageAlt : image}
            />
          </div>
          <div
            className={CssUtils.merge([
              tailwindStyles.container,
              tailwindStyles.textWrapper,
              containerTextLayout,
              textWrapperStyles,
            ])}
          >
            <div
              className={(tailwindStyles.textContainer, textContainerStyles)}
            >
              {children}
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

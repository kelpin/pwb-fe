import React from "react"
import CssUtils from "../../utils/Css"
import { Text } from "../foundations/Text"

export interface SectionOneColumnFullProps {
  imageBackground: string
  title: React.ReactText | string
  subtitle: React.ReactText | string
}

const tailwindStyles = {
  container:
    "bg-cover bg-center h-screen-80 flex flex-col w-full items-center justify-start pt-10",
  imageContainer: "flex-col justify-center items-center",
  textWrapper: "flex-row items-center",
  textContainer: "flex flex-col justify-start items-start w-2/3",
  image: "",
  title: "",
  subtitle: "",
}

export const SectionOneColumnFull: React.FC<SectionOneColumnFullProps> = ({
  title,
  subtitle,
  imageBackground,
}) => {
  return (
    <section
      className={CssUtils.merge([tailwindStyles.container])}
      style={{ backgroundImage: `url(${imageBackground})` }}
    >
      <Text kind="h5" className="leading-none font-light mb-2">
        {subtitle}
      </Text>
      <Text kind="h3" className="font-bold leading-none text-primary">
        {title}
      </Text>
    </section>
  )
}

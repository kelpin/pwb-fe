import { merge } from "./Css";

test("merge several css classes", () => {
  const classes = ["class1", "class2", "class3"];
  const mergeClasses = merge(classes);

  expect(mergeClasses).toEqual("class1 class2 class3");
});

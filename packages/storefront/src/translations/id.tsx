export default {
  "freezerCooler.jumbotron.innovative.title":
    "Innovative cold merchandising solutions",
  "freezerCooler.jumbotron.innovative.desc":
    "Presenting products that can enrich our life with finest quality and service, adding value through professional qualifications in our fields of activity.",
  "freezerCooler.jumbotron.feelMaximum.title": "Feel The Maximum Quality",
  "freezerCooler.jumbotron.feelMaximum.desc":
    "enjoy our fast cooling technology to reach the lowest temperature with minimum time",
  "freezerCooler.jumbotron.quality.title": "Quality Meets Practicality",
  "freezerCooler.jumbotron.quality.desc":
    "Perfect product presentation for customers, high product visibility, quick freezing, mobility, high storage capability and low energy consumption",
  "freezerCooler.aboutUs.title": "About Us",
  "freezerCooler.aboutUs.description":
    "Founded in 2009, Sanwoo Electronics is a growing company that provides cooling solutions for leading FMCGs and retail businesses in the country and a wide range of home appliances products for households. We understand Indonesia is comprised of many islands hence we commit to make our products accessible for delivery and services nationwide.",
  "freezerCooler.cooler.title": "Ice Cold Merchandiser",
  "freezerCooler.cooler.subtitle": "Sanwoo Cooler",
  "freezerCooler.cooler.buttonLabel": "View All Product",
  "freezerCooler.freezer.title": "High Quality Freezer",
  "freezerCooler.freezer.subtitle": "Sanwoo Freezer",
  "freezerCooler.freezer.buttonLabel": "View All Product",
  "freezerCooler.freezer.advantageTitle": "Our Advantage",
  "freezerCooler.freezer.advantageItem1":
    "Environment Friendly with our Hydrocarbon material and with low electricity consumption technology ",
  "freezerCooler.freezer.advantageItem2":
    "Nationwide service including one to one placement",
  "freezerCooler.freezer.oneStopSolutionTitle": "One Stop solution",
  "freezerCooler.freezer.oneStopSolutionDescription":
    "Sanwoo is committed to being your One Stop Solution. Upon every purchase, Sanwoo provides brand & advertising stickering service.",
  "freezerCooler.freezer.oneStopSolutionButtonLabel": "Read more",

  "cooler.biggerSection.title": "Bigger Capacity",
  "cooler.biggerSection.description":
    "Wide facing, more product capacity, more sales impact",
  "cooler.ecoFriendly.title": "Eco-Friendly",
  "cooler.ecoFriendly.description":
    "Low energy consumption, environment friendly refrigerant",
  "cooler.itemsTitle": "Ice Cold Merchandiser",
  "cooler.itemsSubtitle": "Sanwoo Cooler",

  "freezer.bestCold.title": "Best Cold Performance",
  "freezer.bestCold.description":
    "Utilizing the latest technology of Hydrocarbon Refrigerants",
  "freezer.easyServicing.title": "Easy Servicing",
  "freezer.easyServicing.description":
    "With our HC compressors, servicing becomes easier",
  "freezer.itemsTitle": "High Quality Freezer",
  "freezer.itemsSubtitle": "Sanwoo Freezer",

  "service.jumbotron.oneStop.title": "One Stop Solution",
  "service.jumbotron.oneStop.desc":
    "Sanwoo is committed to being your One Stop Solution. Upon every purchase, Sanwoo provides brand & advertising stickering service to make sure you can showcase your brand on our Freezers & Coolers and deliver products to any destination, nation-wide",
  "service.section.workshopAround.title": "Workshop Around Nation",
  "service.section.workshopAround.desc":
    "Support with more than 25 service points across the country, with dedicated technician and spare parts are ready to go. 2 x 24h respond time, ready to serve",
  "service.section.rebranding.title": "Rebranding",
  "service.section.rebranding.desc":
    "We are capable to do onsite or offsite rebranding, with high quality stickers",
  "service.section.placement.title": "Placement",
  "service.section.placement.desc":
    "We have done massive delivery, direct to outlet across Indonesia, from west to east, north to south, with safety installation, we are perfect partner to do your cooler and freezer deployment",

  "home.jumbotron.simplicity.title": "Simplicity Matters",
  "home.jumbotron.simplicity.desc": "Simple and Life-Changing",
  "home.airFryer.title": "Eat Without Fear",
  "home.airFryer.subtitle": "Sanwoo Air Fryer",
  "home.airFryer.desc": "Taste needs no sacrifice, so is health",
  "home.airPurifier.title": "Breathe Easy",
  "home.airPurifier.subtitle": "Sanwoo Air Purifier",
  "home.eWarranty.title": "One Year Warranty",
  "home.eWarranty.subtitle": "E-Warranty",
  "home.eWarranty.ctaButton": "Claim Your Warranty Now",
  "home.promo.title": "Hot Promo",
  "home.promo.subtitle": "Find our latest promo here",

  "airFryer.section.intro.title": "Sanwoo Air Fryer",
  "airFryer.section.intro.subtitle":
    "The revolutionary way to enjoy all type of food with ZERO OIL",
  "airFryer.section.limitless.title": "Limitless Possibilities",
  "airFryer.section.limitless.subtitle":
    "One stop solution for all your cooking needs",
  "airFryer.section.advancedTech.title": "Advanced Technology",
  "airFryer.section.advancedTech.subtitle":
    " Super Heated Rapid Air Technology ",
  "airFryer.section.eatWithouFat.title": "Eat Without Fat",
  "airFryer.section.eatWithouFat.subtitle": "Sanwoo Air Fryer",

  "airPurifier.section.intro.title": "Sanwoo Air Purifier",
  "airPurifier.section.intro.subtitle":
    "Delivering fresh mountain air to your home. Clean & alergen free, you can always count on Sanwoo Air Purifier as your ultimate sanctuary",
  "airPurifier.section.3in1.title": "3-in-1 Filter",
  "airPurifier.section.3in1.subtitle":
    "Layered with Pre-filter, HEPA, and Activated Carbon Filter, Sanwoo Air Purifier is built to ensure clean air.",
  "airPurifier.section.removeOdor.title": "Removes Odor",
  "airPurifier.section.removeOdor.subtitle":
    "Activated Carbon Filter removes pet and garbage odor, cigarette smoke, and all kinds of harmful smells",
  "airPurifier.section.breatheEasy.title": "Breathe Easy",
  "airPurifier.section.breatheEasy.subtitle": "Sanwoo Air Purifier",

  "contactUs.title": "Contact Us",
  "contactUs.desc":
    "Jika Anda memiliki pertanyaan atau masukan seputar layanan kami, mohon menghubungi kami dengan mengisi form yang tersedia.",
  "contactUs.form.fullname.label": "Fullname",
  "contactUs.form.phoneNumber.label": "Phone Number",
  "contactUs.form.emailAddress.label": "Email Address",
  "contactUs.form.selectTopic.label": "Select Topic",
  "contactUs.form.selectMessages.label": "Select Message",
  "contactUs.form.submitButton.label": "Submit",
  "contactUs.card.phoneEmail.title": "Phone & Email",
  "contactUs.card.phoneEmail.number": "+62 811-1353-510",
  "contactUs.card.phoneEmail.emailCS": "cs@sanwoelectronics.com",
  "contactUs.card.phoneEmail.emailAdmin": "admin@sanwoelectronics.com",
  "contactUs.card.headOffice.title": "Head Office",
  "contactUs.card.headOffice.officeName": "PT. Sanwoo Electronics",
  "contactUs.card.headOffice.officeAddress":
    "Jalan Jembatan Tiga Blok F No.8, Jakarta Utara",
  "contactUs.card.headOffice.officePostalCode": "14440",

  "eWarranty.form.title": "your needs are our priority",
  "eWarranty.form.subtitle": "E-Warranty",
  "eWarranty.form.name.label": "Name",
  "eWarranty.form.email.label": "Email",
  "eWarranty.form.phoneNumber.label": "Phone Number",
  "eWarranty.form.streetAddress1.label": "Street Address 1",
  "eWarranty.form.streetAddress2.label": "Street Address 2",
  "eWarranty.form.postalCode.label": "Postal Code",
  "eWarranty.form.province.label": "Province",
  "eWarranty.form.model.label": "Model",
  "eWarranty.form.otherModel.label": "Other Model",
  "eWarranty.form.serialNumber.label": "Serial Number",
  "eWarranty.form.dateOfPurchase.label": "Date of Purchase",
  "eWarranty.form.typeOfUnit.label": "Type of Unit",
}

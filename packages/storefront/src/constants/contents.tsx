import CoolerSNW46 from "../contents/cooler-snw-46.json"
import CoolerSNW60 from "../contents/cooler-snw-60.json"
import CoolerSNW106 from "../contents/cooler-snw-106.json"
import CoolerSNW160 from "../contents/cooler-snw-160.json"
import CoolerSNW246 from "../contents/cooler-snw-246.json"
import CoolerSNW270SCD from "../contents/cooler-snw-270scd.json"
import CoolerSNW279 from "../contents/cooler-snw-279.json"
import CoolerSNW286 from "../contents/cooler-snw-286.json"
import CoolerSNW320SCD from "../contents/cooler-snw-320scd.json"
import CoolerSNW336 from "../contents/cooler-snw-336.json"
import FreezerSNW100F from "../contents/freezer-snw-100F.json"
import FreezerSNW152 from "../contents/freezer-snw-152.json"
import FreezerSNW200F from "../contents/freezer-snw-200F.json"
import FreezerSNW300F from "../contents/freezer-snw-300f.json"
import FreezerSNW330Curved from "../contents/freezer-snw-330curved.json"
import CoolerSNW1006 from "../contents/cooler-snw-1006.json"
import IMAGES from "../assets/images"

export type ProductType = {
  name: string
  desc: string
  image: string
  to: string
}

export const coolers: Array<ProductType> = [
  {
    name: CoolerSNW106.name,
    desc: "",
    image: IMAGES["SNW 106"][0],
    to: CoolerSNW106.path,
  },
  {
    name: CoolerSNW160.name,
    desc: "",
    image: IMAGES["SNW 160 Slim"][0],
    to: CoolerSNW160.path,
  },
  {
    name: CoolerSNW246.name,
    desc: "",
    image: IMAGES["SNW 246"][0],
    to: CoolerSNW246.path,
  },
  {
    name: CoolerSNW286.name,
    desc: "",
    image: IMAGES["SNW 286"][0],
    to: CoolerSNW286.path,
  },
  {
    name: CoolerSNW336.name,
    desc: "",
    image: IMAGES["SNW 336"][0],
    to: CoolerSNW336.path,
  },
  {
    name: CoolerSNW1006.name,
    desc: "",
    image: IMAGES["SNW 1006"][0],
    to: CoolerSNW1006.path,
  },
]

export const miniCoolers: Array<ProductType> = [
  {
    name: CoolerSNW46.name,
    desc: "",
    image: IMAGES["SNW 46"][0],
    to: CoolerSNW46.path,
  },
  {
    name: CoolerSNW60.name,
    desc: "",
    image: IMAGES["SNW 60"][0],
    to: CoolerSNW60.path,
  },
]

export const hybridCoolers: Array<ProductType> = [
  {
    name: CoolerSNW279.name,
    desc: "",
    image: IMAGES["SNW 279"][0],
    to: CoolerSNW279.path,
  },
]

export const coolerFreezer: Array<ProductType> = [
  {
    name: CoolerSNW270SCD.name,
    desc: "",
    image: IMAGES["SNW 270SCD"][0],
    to: CoolerSNW270SCD.path,
  },
  {
    name: CoolerSNW320SCD.name,
    desc: "",
    image: IMAGES["SNW 320SCD"][0],
    to: CoolerSNW320SCD.path,
  },
]

export const freezers: Array<ProductType> = [
  {
    name: FreezerSNW100F.name,
    desc: "",
    image: IMAGES["SNW 100F"][0],
    to: FreezerSNW100F.path,
  },
  {
    name: FreezerSNW152.name,
    desc: "",
    image: IMAGES["SNW 152"][0],
    to: FreezerSNW152.path,
  },
  {
    name: FreezerSNW200F.name,
    desc: "",
    image: IMAGES["SNW 200F"][0],
    to: FreezerSNW200F.path,
  },
  {
    name: FreezerSNW300F.name,
    desc: "",
    image: IMAGES["SNW 300F"][0],
    to: FreezerSNW300F.path,
  },
  {
    name: FreezerSNW330Curved.name,
    desc: "",
    image: IMAGES["SNW 330 Curved"][0],
    to: FreezerSNW330Curved.path,
  },
]

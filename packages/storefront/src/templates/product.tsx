import React from "react"
import { PageProps } from "gatsby"
import { Text } from "../components/foundations/Text"
import Layout from "../components/layout"
import SEO from "../components/seo"
import CssUtils from "css-utils"
import QueryString from "query-string"
import BuyModal from "../components/BuyModal"
import { Button } from "../components/Button"
import IMAGES from "../assets/images"
import Breadcrumb from "../components/Breadcrumb"

const ChevronBottom = () => {
  return (
    <div>
      <svg
        className="fill-current h-6 w-6"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
      >
        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
      </svg>
    </div>
  )
}

type SpecType = {
  label: string
  value: string
}

type ProductType = {
  name: string
  overview: Array<string>
  specifications: Array<SpecType>
  images: Array<string>
}

const Accordion = ({ children, title }) => {
  const [showAccordion, toggleShowAccordion] = React.useState(true)

  return (
    <div className="accordion mb-10">
      <div className="w-full border-b py-3 flex justify-between items-center">
        <Text kind="h5" className="font-bold uppercase">
          {title}
        </Text>
        <div
          className={CssUtils.merge([
            showAccordion
              ? "transform hover:opacity-75 transition ease-in duration-200"
              : "transform -rotate-90 hover:rotate-0 hover:opacity-50 transition ease-in duration-200",
            "cursor-pointer",
          ])}
          onClick={() => toggleShowAccordion(!showAccordion)}
        >
          <ChevronBottom />
        </div>
      </div>
      {showAccordion && children}
    </div>
  )
}

let renderImage: React.FC<Array<string>> = images => {
  let [activeImage, setActiveImage] = React.useState(images[0])

  return (
    <div className="w-full">
      <div className="flex flex-col justify-center items-center p-2 rounded-lg shadow-md bg-white hover:shadow-lg mb-5 h-screen-50">
        <img
          src={activeImage}
          alt={"active-image"}
          className="h-full w-full object-contain"
        />
      </div>
      <div className="grid grid-cols-3 gap-2 md:gap-6 h-32 md:h56">
        {images &&
          images.map(image => {
            return (
              <div
                className="relative flex flex-col items-center justify-center rounded-lg shadow-md bg-white hover:shadow-lg cursor-pointer"
                onClick={() => setActiveImage(image)}
              >
                <img
                  src={image}
                  alt={"active-image"}
                  className="absolute md:p-2 h-full w-full object-contain"
                />
              </div>
            )
          })}
      </div>
    </div>
  )
}

const Product = ({ location, pageContext }) => {
  const { id, name, specifications, overview, internalUrl } = pageContext
  const images = IMAGES[name] || []

  const [showBuyModal, setShowBuyModal] = React.useState(false)

  let renderProductOverview = (overview: Array<string>) => {
    return (
      <Accordion title="Product Overview">
        {overview.map(o => (
          <Text kind="body" className="my-2 uppercase">
            {o}
          </Text>
        ))}
      </Accordion>
    )
  }
  let renderProductSpecification = (specs: Array<SpecType>) => {
    return (
      <Accordion title="Product Specifications">
        <div className="grid grid-cols-2 gap-2 gap-y-2 my-3">
          {specs.map(({ label, value }) => (
            <>
              <Text kind="body" className="font-bold uppercase">
                {label}
              </Text>
              <Text kind="body" className="uppercase whitespace-pre-line">
                {value}
              </Text>
            </>
          ))}
        </div>
      </Accordion>
    )
  }

  const buyModalProps = () => {
    if (
      name == "Sanwoo Air Fryer 52" ||
      name == "Sanwoo 14-in-1 Multi Cooker & Air Fryer" ||
      name == "Sanwoo Multifunction Low Sugar Rice Cooker"
    ) {
      return {
        disableToko: false,
        disableBlibli: true,
        disableShopee: true,
        disableJd: true,
      }
    } else {
      return {}
    }
  }
  return (
    <>
      {showBuyModal && (
        <BuyModal onClose={() => setShowBuyModal(false)} {...buyModalProps()} />
      )}
      <Layout title={name} location={location}>
        <SEO title={name} />
        <div className="w-full flex flex-col items-center">
          <section className="py-5 w-full md:w-10/12 lg:max-w-7xl mb-10 flex flex-col md:grid md:grid-cols-2 md:gap-6">
            <div className="block md:hidden">
              <Text kind="h3" className="font-bold px-5">
                {name}
              </Text>
            </div>
            <div className="px-8 md:pt-8">
              <div className="id-breadcrumb mb-5">
                <Breadcrumb url={internalUrl} title={name} />
              </div>
              {renderImage(images)}

              <div className="mt-16 mb-20 w-full flex justify-center items-center">
                <Button
                  label="Buy Now"
                  kind="primary"
                  className="w-full"
                  onClick={() => setShowBuyModal(true)}
                />
              </div>
            </div>
            <div className="px-4 md:px-0 md:pl-8 md:pt-8">
              <div className="hidden md:block">
                <Text kind="h3" className="font-bold">
                  {name}
                </Text>
              </div>
              {renderProductOverview(overview)}
              {renderProductSpecification(specifications)}
            </div>
          </section>
        </div>
      </Layout>
    </>
  )
}

export default Product

export default {
  logoFreezerCoolerBlue: require("./sanwoo-freezer-cooler-blue.png"),
  logoFreezerCoolerGray: require("./sanwoo-freezer-cooler-gray.png"),
  logoHomeBlue: require("./sanwoo-home-blue.png"),
  logoHomeGray: require("./sanwoo-home-gray.png"),

  freezerCoolerJumbotronCooler: require("./freezer-cooler-jumbotron-cooler.jpg"),
  freezerCoolerJumbotronCoolerSection: require("./freezer-cooler-jumbotron-cooler-section.jpg"),
  freezerCoolerJumbotronCoolerMobile: require("./coolerMobile.jpg"),
  freezerCoolerJumbotronCoolerLeft: require("./freezer-cooler-jumbotron-cooler-left.jpg"),
  freezerCoolerJumbotronFreezer: require("./freezer-cooler-jumbotron-freezer.jpg"),
  freezerCoolerJumbotronFreezerMobile: require("./freezerMobile.jpg"),
  freezerCoolerJumbotronFeelMaximum: require("./freezer-cooler-jumbotron-feel-maximum.jpg"),
  freezerCoolerJumbotronFreezerLeft: require("./freezer-cooler-jumbotron-freezer-left.jpg"),
  freezerCoolerAboutUs: require("./freezer-cooler/img-about-us.jpg"),
  freezerCoolerOneStopSolutiokn: require("./freezer-cooler/img-one-stop-solution.jpg"),
  freezerCoolerOurAdvantage: require("./freezer-cooler/img-our-advantage.jpg"),

  freezerCooler_coolerLanding1: require("./sanwoo-coolers-landing/cooler-landing-1.png"),
  freezerCooler_coolerLanding2: require("./sanwoo-coolers-landing/cooler-landing-2.png"),
  freezerCooler_coolerLanding3: require("./sanwoo-coolers-landing/cooler-landing-3.png"),
  freezerCooler_coolerLanding4: require("./sanwoo-coolers-landing/cooler-landing-4.png"),
  freezerCooler_coolerLanding5: require("./sanwoo-coolers-landing/cooler-landing-5.png"),
  freezerCooler_coolerLanding6: require("./sanwoo-coolers-landing/cooler-landing-6.png"),
  freezerCooler_coolerLanding7: require("./sanwoo-coolers-landing/cooler-landing-7.png"),
  freezerCooler_coolerLanding8: require("./sanwoo-coolers-landing/cooler-landing-8.png"),

  freezerCooler_freezerLanding1: require("./sanwoo-freezer-landing/freezer-landing-1.png"),
  freezerCooler_freezerLanding2: require("./sanwoo-freezer-landing/freezer-landing-2.png"),
  freezerCooler_freezerLanding3: require("./sanwoo-freezer-landing/freezer-landing-3.png"),
  freezerCooler_freezerLanding4: require("./sanwoo-freezer-landing/freezer-landing-4.png"),
  freezerCooler_freezerLanding5: require("./sanwoo-freezer-landing/freezer-landing-5.png"),

  coolerBigger: require("./sanwoo-coolers-landing/cooler-bigger.jpg"),
  coolerEco: require("./sanwoo-coolers-landing/cooler-eco.jpg"),

  freezerBest: require("./sanwoo-freezer-landing/sanwoo-freezer-landing-best.jpg"),
  freezerEasy: require("./sanwoo-freezer-landing/sanwoo-freezer-landing-easy.jpg"),

  serviceJumbotron: require("./service/img-jumbotron.jpg"),
  servicePlacement: require("./service/img-placement.jpg"),
  serviceRebranding: require("./service/img-rebranding.png"),
  serviceWorkshop: require("./service/img-workshop-around.jpg"),

  homeJumbotronSimplicity: require("./home-jumbotron-simplicity-matters.jpg"),
  homeAirFyer: require("./home-air-fryer.jpg"),
  homeAirPurifier: require("./home-air-purifier.jpg"),
  homeAirFryer55: require("./home-airfryer-55.jpg"),
  homeMultiCooker: require("./home-multicooker.jpg"),
  homeRiceCooker: require("./home-ricecooker.png"),
  homeEWarranty: require("./home-ewarranty.jpg"),
  homePromo: require("./home-promo.jpg"),
  homePromoBig: require("./home-promo-new.jpg"),
  homePromoSomething: require("./home-banner-something-big.jpg"),
  homeBannerNew: require("./home-banner-new.jpg"),

  airFryerIntro: require("./airfyer-intro.jpg"),
  airFryerSub_advance: require("./airfyer-sub-advance.jpg"),
  airFryerSub_advance_item: require("./airfryer-sub-advance-items.png"),
  airFryerSub_limitless: require("./airfyer-sub-limitless.jpg"),
  airFryerWithoutFat: require("./airfyer-without-fat.jpg"),
  airFryerShowcase__Front__Open: require("./airfryer-front-open.jpg"),
  airFryerShowcase__Front__Closed: require("./airfryer-front-closed.jpg"),
  airFryerShowcase__Side__Open: require("./airfryer-side-open.jpg"),
  itemHomeFryer__FRONT: require("./contents/airfryer-FRONT__CLOSED.jpg"),
  itemHomeFryer__FRONT__OPEN: require("./contents/airfryer-SIDE__OPEN.jpg"),
  itemHomeFryer__SIDE__OPEN: require("./contents/airfryer-FRONT__OPEN.jpg"),
  airPurifierIntro: require("./airpurifier-intro.jpg"),
  airPurifierSub_3in1: require("./airpurifier-3in1.jpg"),
  airPurifierSub_removesOdor: require("./airpurifier-removes-odor.jpg"),
  airPurifierBreatheEasy: require("./airpurifier-breathe-easy.jpg"),
  airPurifierShowcase__Front: require("./airpurifier-front.jpg"),
  airPurifierShowcase__Side: require("./airpurifier-side.jpg"),
  airPurifierShowcase__Back: require("./airpurifier-back.jpg"),

  airFryer52Intro: require("./airfyer-52-intro.jpg"),
  airFryer52ComingSoon: require("./airfryer-52-coming-soon.jpg"),
  airFryer52Bigger: require("./airfryer-52-bigger-better-faster.jpg"),
  airFryer52Something: require("./airfryer-52-something-bigger-is-coming.jpg"),

  multiCookerIntro: require("./multi-cooker-intro.jpg"),
  multiCookerLeft: require("./multi-cooker-left.jpg"),
  multiCookerRight: require("./multi-cooker-right.jpg"),
  multiCooker14in1: require("./multi-cooker-14-in-1.jpg"),

  riceCookerIntro: require("./rice-cooker-intro.jpg"),
  riceCookerLeft: require("./rice-cooker-left.jpg"),
  riceCookerRight: require("./rice-cooker-right.jpg"),
  riceCookerLowSugar: require("./rice-cooker-low-sugar.jpg"),

  itemHomePurifier__FRONT: require("./sanwoo-home/AIRPURIFIER_FRONT.png"),
  itemHomePurifier__SIDE: require("./sanwoo-home/AIRPURIFIER_SIDE.png"),
  itemHomePurifier__BACK: require("./sanwoo-home/AIRPURIFIER_BACK.png"),

  riceCooker__FRONT: require("./rice-cooker-front.png"),
  bgRiceCooker__FRONT: require("./rice-cooker-front-bg.jpg"),

  riceCooker__TOP: require("./rice-cooker-top.png"),
  bgRiceCooker__TOP: require("./rice-cooker-top-bg.jpg"),

  buy: {
    shopee: require("./buy-shopee.png"),
    tokopedia: require("./buy-tokopedia.png"),
    jdId: require("./buy-jd-id.png"),
    blibli: require("./buy-blibli.png"),
  },

  airfryer52NewBigger: require("./airfryer-52-new-bigger-better-faster.jpg"),
  airfryer52NewRight2: require("./airfryer-52-new-right-2.png"),
  airfryer52NewRight: require("./airfryer-52-new-right.png"),
  airfryer52NewLeft2: require("./airfryer-52-new-left-2.jpg"),
  airfryer52NewLeft: require("./airfryer-52-new-left.png"),
  airfryer52NewPromo: require("./airfryer-52-new-promo.jpg"),
  airfryer52NewBiggerWithGradient: require("./airfryer-52-new-bigger-with-gradient.png"),
  promoAugust: require("./promo-banner-august-2021.jpg"),

  topBannerAugust: require("./top-banner-promo-august.jpg"),

  bannerPartner: require("./banner-partners.jpg"),

  "SNW 46": [
    require("./contents/cooler-SNW-46-SIDE__OPEN.jpg"),
    require("./contents/cooler-SNW-46-FRONT__OPEN.jpg"),
    require("./contents/cooler-SNW-46-FRONT__CLOSED.jpg"),
  ],

  "SNW 60": [
    require("./contents/cooler-SNW-60-SIDE__OPEN.jpg"),
    require("./contents/cooler-SNW-60-FRONT__OPEN.jpg"),
    require("./contents/cooler-SNW-60-FRONT__CLOSED.jpg"),
  ],

  "SNW 106": [
    require("./contents/cooler-SNW-106-FRONT__OPEN.jpg"),
    require("./contents/cooler-SNW-106-FRONT__CLOSED.jpg"),
    require("./contents/cooler-SNW-106-SIDE__CLOSED.jpg"),
  ],

  "SNW 160 Slim": [
    require("./contents/cooler-SNW-160-SIDE__OPEN.jpg"),
    require("./contents/cooler-SNW-160-FRONT__OPEN.jpg"),
    require("./contents/cooler-SNW-160-FRONT__CLOSED.jpg"),
  ],

  "SNW 246": [
    require("./contents/cooler-SNW-246-SIDE__OPEN.jpg"),
    require("./contents/cooler-SNW-246-FRONT__OPEN.jpg"),
    require("./contents/cooler-SNW-246-FRONT__CLOSED.jpg"),
  ],

  "SNW 270SCD": [
    require("./contents/cooler-SNW-270SCD-SIDE__OPEN.jpg"),
    require("./contents/cooler-SNW-270SCD-FRONT__OPEN.jpg"),
    require("./contents/cooler-SNW-270SCD-FRONT__CLOSED.jpg"),
  ],

  "SNW 279": [
    require("./contents/cooler-SNW-279-SIDE__OPEN.jpg"),
    require("./contents/cooler-SNW-279-FRONT__OPEN.jpg"),
    require("./contents/cooler-SNW-279-FRONT__CLOSED.jpg"),
  ],

  "SNW 286": [
    require("./contents/cooler-SNW-286-SIDE__OPEN.jpg"),
    require("./contents/cooler-SNW-286-FRONT__OPEN.jpg"),
    require("./contents/cooler-SNW-286-FRONT__CLOSED.jpg"),
  ],

  "SNW 320SCD": [
    require("./contents/cooler-SNW-320SCD-SIDE__OPEN.jpg"),
    require("./contents/cooler-SNW-320SCD-FRONT__OPEN.jpg"),
    require("./contents/cooler-SNW-320SCD-FRONT__CLOSED.jpg"),
  ],

  "SNW 336": [
    require("./contents/cooler-SNW-336-SIDE__OPEN.jpg"),
    require("./contents/cooler-SNW-336-FRONT__OPEN.jpg"),
    require("./contents/cooler-SNW-336-FRONT__CLOSED.jpg"),
  ],

  "SNW 1006": [
    require("./contents/cooler-SNW-1006-SIDE__CLOSED.jpg"),
    require("./contents/cooler-SNW-1006-FRONT__OPEN.jpg"),
    require("./contents/cooler-SNW-1006-FRONT__CLOSED.jpg"),
  ],

  "SNW 100F": [
    require("./contents/freezer-SNW-100F-FRONT__CLOSED.jpg"),
    require("./contents/freezer-SNW-100F-TOP_CLOSED.jpg"),
    require("./contents/freezer-SNW-100F-SIDE_CLOSED.jpg"),
  ],

  "SNW 152": [
    require("./contents/freezer-SNW-152-FRONT__CLOSED.jpg"),
    require("./contents/freezer-SNW-152-SIDE__CLOSED.jpg"),
    require("./contents/freezer-SNW-152-TOP__CLOSED.jpg"),
  ],

  "SNW 200F": [
    require("./contents/freezer-SNW-200F-FRONT__CLOSED.jpg"),
    require("./contents/freezer-SNW-200F-TOP__CLOSED.jpg"),
    require("./contents/freezer-SNW-200F-SIDE__CLOSED.jpg"),
  ],

  "SNW 300F": [
    require("./contents/freezer-SNW-300F-FRONT__CLOSED.jpg"),
    require("./contents/freezer-SNW-300F-TOP__CLOSED.jpg"),
    require("./contents/freezer-SNW-300F-SIDE__CLOSED.jpg"),
  ],

  "SNW 330 Curved": [
    require("./contents/freezer-SNW-330CURVED-FRONT__CLOSED.jpg"),
    require("./contents/freezer-SNW-330CURVED-SIDE__CLOSED.jpg"),
    require("./contents/freezer-SNW-330CURVED-FRONT__CLOSED.jpg"),
  ],

  "Sanwoo Air Fryer": [
    require("./contents/airfryer-SIDE__OPEN.jpg"),
    require("./contents/airfryer-FRONT__OPEN.jpg"),
    require("./contents/airfryer-FRONT__CLOSED.jpg"),
  ],

  "Sanwoo Air Purifier": [
    require("./sanwoo-home/AIRPURIFIER_SIDE.png"),
    require("./sanwoo-home/AIRPURIFIER_FRONT.png"),
    require("./sanwoo-home/AIRPURIFIER_BACK.png"),
  ],

  "Sanwoo Air Fryer 52": [
    require("./contents/airfryer-55-FRONT__CLOSED.png"),
    require("./contents/airfryer-55-TOP__OPEN.png"),
    require("./contents/airfryer-55-SIDE__OPEN.png"),
    require("./contents/airfryer-55-SIDE__CLOSED.png"),
  ],

  "Sanwoo 14-in-1 Multi Cooker & Air Fryer": [
    require("./contents/multicooker__FRONT.png"),
    require("./contents/multicooker__TOP.png"),
    require("./contents/multicooker__TOP__TRANSPARENT.png"),
  ],

  "Sanwoo Multifunction Low Sugar Rice Cooker": [
    require("./contents/ricecooker__FRONT.png"),
    require("./contents/ricecooker__INNER.png"),
    require("./contents/ricecooker__INNER_DOTTED.png"),
    require("./contents/ricecooker__SIDE__CLOSED.png"),
  ],
}

# Sanwoo Storefront

Storybook: https://project-sanwoo-storybook.dskusuma.dev/

# How to run project

Install lerna globally

```
npm install -g lerna
```

Navigate to project

```
cd packages/storefront
```

Link subpackages `css-utils` to current project
```
lerna bootstrap
```

Install dependencies

```
npm install
```

Run project

```
npm run tailwind:build && npm start
```
